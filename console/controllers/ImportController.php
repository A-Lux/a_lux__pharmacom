<?php

namespace console\controllers;

use common\models\Country;
use common\models\Filial;
use common\models\FilialProduct;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;
use common\models\Products;
use common\models\CatalogProducts;
use common\models\SopTovary;
use common\models\Analogy;
use common\models\Catalog;
use common\models\Gift;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ImportController extends Controller
{
    public function actionRun()
    {
        $botToken = "1131589058:AAFhKq9BLxrFjbc2gAeMiX0Pr8ThlFRTeRE";

        $website = "https://api.telegram.org/bot" . $botToken;
        $chats = [
            106069427, //Ардак
            415550928, //Асель
            412368804  //Oleg
        ];  //** ===>>>NOTE: this chatId MUST be the chat_id of a person, NOT another bot chatId !!!**
        foreach ($chats as $chat) {
            $params = [
                'chat_id' => $chat,
                'text' => 'Интеграция началась',
            ];
            $ch = curl_init($website . '/sendMessage');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);
        }

        $related = [];
        $analogs = [];
        $presents = [];

        if (($handle = fopen("/home/admin/web/dobraya-apteka.kz/standart_n/warebase_new.csv", "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
                if (!$count) {
                    $count++;
                    continue;
                }

                $data = array_map(function ($str) {
                    return iconv("Windows-1251", "UTF-8", $str);
                }, $data);

                if (empty($data[18])) {
                    continue;
                }

                $categories = [];

                if (!empty($data[15])) {
                    if (!($catalog = CatalogProducts::find()->where(['id' => $data[15], 'level' => 1])->one())) {
                        if (!empty($data[14])) {
                            $catalog = new CatalogProducts();
                            $catalog->attributes = [
                                'id' => $data[15],
                                'title' => $data[14],
                                'name' => $data[14],
                                'description' => $data[14],
                                'level' => 1,
                                'status' => 1,
                                // 'sort' => 0
                            ];
                            $catalog->url = $catalog->generateCyrillicToLatin();
                            $catalog->save(false);
                            // $categories[] = $catalog->id;
                        }
                    }
                }

                $country = null;

                if (!empty($data[4])) {
                    if (!($country = Country::find()->where(['name' => $data[4]])->one())) {
                        if (!empty($data[4])) {
                            $country = new Country();
                            $country->attributes = [
                                'name' => $data[4],
                            ];
                            $country->save(false);
                        }
                    }
                }

                if (!empty($data[17])) {
                    if (!($catalog_child = CatalogProducts::find()->where(['id' => $data[17], 'level' => 2])->one())) {
                        if (!empty($data[17])) {
                            $catalog_child = new CatalogProducts();
                            $catalog_child->attributes = [
                                'id' => $data[17],
                                'title' => $data[16] ? $data[16] : $data[14],
                                'name' => $data[16] ? $data[16] : $data[14],
                                'description' => $data[16] ? $data[16] : $data[14],
                                'parent_id' => $catalog->id,
                                'level' => 2,
                                'status' => 1,
                                // 'sort' => 0
                            ];
                            $catalog_child->url = $catalog_child->generateCyrillicToLatin();
                            $catalog_child->save(false);
                            // $categories[] = $catalog_child->id;
                        }
                    }
                }

                $new = false;

                if (!($model = Products::find()->where(['ware_id' => $data[18]])->one())) {
                    $model = new Products();
                    $new = true;
                }

                $model_data = [
                    'category_id' => !empty($data[17]) ? $data[17] : '',
                    'title' => !empty($data[2]) ? $data[2] : '',
                    'description' => !$new ? $model->description : '',
                    'name' => !empty($data[2]) ? $data[2] : '',
                    'status' => !((int) $data[7] <= 0),
                    'price' => !empty($data[13]) ? (int) $data[13] : 0,
                    // 'text' => !empty($data[19]) ? $data[19] : '',
                    'country' => $country ? $country->id : 0,
                    'model' => '',
                    'status_products' => !empty($data[21]) ? (int) $data[21] : 0,
                    'top_month' => !$new ? $model->top_month : 0,
                    // !empty($data[22]) ? (int) $data[22] : 0,
                    'isHit' => !$new ? $model->isHit : 0,
                    'isNew' => !$new ? $model->isNew : 0,
                    // !empty($data[24]) ? (int) $data[24] : 0,
                    'bonus' => !empty($data[25]) ? (int) $data[25] : 0,
                    'discount' => !empty($data[26]) ? (int) $data[26] : 0,
                    'ware_id' => $data[18]
                ];

                $model->attributes = $model_data;

                $model->url = $model->generateCyrillicToLatin();
                $model->url = $model->url . '_' . $model->id;
                $model->save(false);

                if ($filial = Filial::find()->where(['name' => $data[0]])->one()) {
                    if (!($remainder = FilialProduct::find()->where([
                        'filial_id' => $filial->id,
                        'product_id' => $model->id
                    ])->one())) {
                        $remainder = new FilialProduct();
                    }

                    $remainder_data = [
                        'filial_id' => $filial->id,
                        'product_id' => $model->id,
                        'amount' => (int) $data[7]
                    ];

                    $remainder->attributes = $remainder_data;
                    $remainder->save(false);
                }

                if (!empty($data[27])) {
                    array_push($related, [
                        'id' => $model['id'],
                        'related' => $data[27],
                    ]);
                }
                if (!empty($data[28])) {
                    array_push($analogs, [
                        'id' => $model['id'],
                        'analog' => $data[28],
                    ]);
                }
                if (!empty($data[29])) {
                    array_push($presents, [
                        'id' => $model['id'],
                        'presents' => $data[29],
                    ]);
                }
                //        $dir = new \DirectoryIterator(Yii::$app->basePath.'/../backend/web/images/products/');
                //        foreach ($dir as $fileinfo) {
                //            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                //                $model = Products::find()->where(['ware_id' => $fileinfo->getFilename()])->one();
                //                $files = scandir (Yii::$app->basePath.'/../backend/web/images/products/'.$fileinfo->getFilename());
                //                $firstFile = Yii::$app->basePath.'/../backend/web/images/products/'.$fileinfo->getFilename() . '/' . $files[2];// because [0] = "." [1] = ".."
                //                if(file_put_contents(Yii::$app->basePath.'/../backend/web/images/products/'.basename($firstFile), file_get_contents($firstFile))) {
                //                    $model->images = serialize([basename($firstFile)]);
                //                    $model->save(false);
                //                }
                //            }
                //        }

            }

            fclose($handle);

            // CatalogProducts::deleteAll(['not in', 'id', $categories]);
            
            foreach ($related as $relate) {
                foreach (explode(',', str_replace("'", '', $relate['related'])) as $relate_id) {
                    if ($t = Products::find()->where(['ware_id' => $relate_id])->one()) {
                        if (!($sopsModel = SopTovary::find()->where(['product1' => $relate['id'], 'product2' => $t['id'],])->one())) {
                            $sopsModel = new SopTovary();
                        }
                        $sopsModel->attributes = [
                            'product1' => $relate['id'],
                            'product2' => $t['id'],
                        ];
                        $sopsModel->save(false);
                    }
                }
            }
            foreach ($analogs as $analog) {
                foreach (explode(',', str_replace("'", '', $analog['analog'])) as $analog_id) {
                    if ($t = Products::find()->where(['ware_id' => $analog_id])->one()) {
                        if (!($analogModel = Analogy::find()->where(['product1' => $analog['id'], 'product2' => $t['id'],])->one())) {
                            $analogModel = new Analogy();
                        }
                        $analogModel->attributes = [
                            'product1' => $analog['id'],
                            'product2' => $t['id'],
                        ];
                        $analogModel->save(false);
                    }
                }
            }
            foreach ($presents as $present) {
                $arr = [];
                foreach (explode(',', str_replace("'", '', $present['presents'])) as $present_id) {
                    if ($t = Products::find()->where(['ware_id' => $present_id])->one()) {
                        $arr[] = $t['id'];
                    }
                }
                if (!($presentModel = Gift::find()->where(['product_id' => $present['id'], 'products' => implode(",", $arr)])->one())) {
                    $presentModel = new Gift();
                }

                $presentModel->attributes = [
                    'product_id' => $present['id'],
                    'products' => implode(",", $arr),
                    'type' => 0
                ];
                $presentModel->save(false);
            }
        }

        foreach ($chats as $chat) {
            $params = [
                'chat_id' => $chat,
                'text' => 'Интеграция закончилась',
            ];
            $ch = curl_init($website . '/sendMessage');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }

    public function actionPurge()
    {
        $botToken = "1131589058:AAFhKq9BLxrFjbc2gAeMiX0Pr8ThlFRTeRE";

        $website = "https://api.telegram.org/bot" . $botToken;
        $chats = [
            106069427, //Ардак
            415550928, //Асель
            412368804  //Oleg
        ];  //** ===>>>NOTE: this chatId MUST be the chat_id of a person, NOT another bot chatId !!!**
        foreach ($chats as $chat) {
            $params = [
                'chat_id' => $chat,
                'text' => 'Очистка базы началась',
            ];
            $ch = curl_init($website . '/sendMessage');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);
        }

        // $products = [];
        // if (($handle = fopen("/home/admin/web/new.dobraya-apteka.kz/public_html/standart_n/warebase_new.csv", "r")) !== FALSE) {
        //     $count = 0;
        //     while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
        //         if (!$count) {
        //             $count++;
        //             continue;
        //         }

        //         $data = array_map(function ($str) {
        //             return iconv("Windows-1251", "UTF-8", $str);
        //         }, $data);

        //         if (empty($data[18])) {
        //             continue;
        //         }

        //         if (!empty($product = Products::find()->where(['ware_id' => $data[18]])->one())) {
        //             $products[] = $product->id;
        //             if ($filial = Filial::findOne(['name' => $data[0]])) {
        //                 FilialProduct::deleteAll([
        //                     ['=', 'filial_id', $filial->id],
        //                     ['=', 'product_id', $product->id]
        //                 ]);
        //             }
        //         }
        //     }
        // }

        // FilialProduct::deleteAll(
        //     ['not in', 'product_id', $products],
        // );

        // Products::deleteAll(
        //     ['not in', 'id', $products]
        // );

        FilialProduct::deleteAll();

        $this->actionRun();

        foreach ($chats as $chat) {
            $params = [
                'chat_id' => $chat,
                'text' => 'Очистка базы закончилась',
            ];
            $ch = curl_init($website . '/sendMessage');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }

    public function actionTemp()
    {
        if (($handle = fopen("/home/admin/web/dobraya-apteka.kz/public_html/standart_n/temp.csv", "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
                if (!$count) {
                    $count++;
                    continue;
                }

                $data = array_map(function ($str) {
                    return iconv("Windows-1251", "UTF-8", $str);
                }, $data);

                if (!($model = Products::find()->where(['ware_id' => $data[0]])->one())) {
                    continue;
                } else {
                    $model_data = [
                        'text' => $data[1],
                    ];
                    $model->save(false);
                }
            }
            fclose($handle);
        }
    }

    public function actionOptimized()
    {
        if (($handle = fopen("/home/admin/web/dobraya-apteka.kz/standart_n/warebase_new2.csv", "r")) !== FALSE) {
            $botToken = "1131589058:AAFhKq9BLxrFjbc2gAeMiX0Pr8ThlFRTeRE";

            $website = "https://api.telegram.org/bot" . $botToken;
            $chats = [
                106069427, //Ардак
                415550928, //Асель
                412368804  //Oleg
            ];  //** ===>>>NOTE: this chatId MUST be the chat_id of a person, NOT another bot chatId !!!**
            foreach ($chats as $chat) {
                $params = [
                    'chat_id' => $chat,
                    'text' => 'Интеграция началась',
                ];
                $ch = curl_init($website . '/sendMessage');
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
            }
            
            $count = 0;

            $keys = [
                // 'group', //Не используется
                'name',
                'manufacturer',//Не используется
                'country',
                'Штрихкод', //Не используется
                'price',
                'category_name',
                'category_id',
                'subcategory_name',
                'subcategory_id',
                'ware_id',
                'remainders',
                'status_products', //Не используется
                'discount', 
                'soput_list',
                'analog_list',
                'present_list',
                'temp'
            ];
            
            // $products = Products::find()
            //     ->select(['id', 'ware_id'])
            //     ->asArray()
            //     ->all();            

            // $newProducts = [];
            // $updateProducts = [];

            // $categories = CatalogProducts::find()
            //     ->all();

            // $countries = Country::find()
            //     ->select(['id', 'name'])
            //     ->all();

            $newCategories = [];
            $updateCategories = [];

            $remainders = [];

            while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
                if (!$count) {
                    $count++;
                    continue;
                }
                $count++;

                $data = array_map(function ($str) {
                    return iconv("Windows-1251", "UTF-8", $str);
                }, $data);

                if(count($keys) != count($data)){
                    continue;
                }
                $data = array_combine($keys, $data);
                
                // $country = '';
                // if(!empty($data['country'])){
                //     if(!ArrayHelper::isIn($data['country'], ArrayHelper::getColumn($countries, 'name'))){
                        
                //             $country = new Country();
                //             $country->attributes = [
                //                 'name' => $data['country']
                //             ];
                //             $country->save(false);
                //     }
                // }
                // else{
                //     $country = $countries[array_search($data['country'], array_column($countries, 'country'))];
                // }

                $cyr = [
                    'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
                    'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
                    'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',
                    'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
                    ',', '.', ' ', '/', '(', ')', '|', '_'
                ];
                $lat = [
                    'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
                    'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
                    'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
                    'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
                    '-', '-', '-', '-', '-', '-', '-', '-'
                ];
                $text = str_replace($cyr, $lat, $data['name']);
                if (substr($text, -1) == '-') $text = substr($text, 0, -1);
                $url = preg_replace('/[^a-zA-Z0-9-]/', '', $text);

                // Yii::$app->db->createCommand(

                //     "INSERT 
                //         INTO filial_product (ware_id, amount, temp) 
                //         VALUES 
                //             (   
                //                 :ware_id, 
                //                 :amount, 
                //                 :temp
                //             ) 
                //         ON DUPLICATE KEY 
                //         UPDATE 
                //             amount = :amount"                        
                // )->bindValues([
                //     ':ware_id' => $data['ware_id'],
                //     ':amount' => (int) $value,
                //     ':temp' => $key,
                // ])->execute();

                Yii::$app->db->createCommand(

                    "INSERT 
                        INTO products (category_id, title, name, price, country, discount, ware_id, url) 
                        VALUES 
                            (   
                                :category_id, 
                                :title, 
                                :name,
                                :price,
                                :country,
                                :discount,
                                :ware_id,
                                :url
                            ) 
                        ON DUPLICATE KEY 
                        UPDATE 
                            category_id = :category_id, 
                            title = :title, 
                            name = :name, 
                            price = :price, 
                            country = :country, 
                            discount = :discount"
                
                )->bindValues([
                    ':category_id' => !empty($data['subcategory_id']) ?
                        $data['subcategory_id'] : 
                            ($data['category_id'] ? 
                                $data['category_id'] : null),
                    ':title' => !empty($data['name']) ? $data['name'] : '',
                    ':name' => !empty($data['name']) ? $data['name'] : '',
                    ':price' => !empty($data['price']) ? (int) $data['price'] : 0,
                    ':country' => 1,
                    // $country->id,
                    ':discount' => !empty($data['discount']) ? (int) $data['discount'] : 0,
                    ':ware_id' => $data['ware_id'],
                    ':url' => $url
                ])->execute();
                
                try {                    
                    foreach (Json::decode($data['remainders']) as $key => $value) {
                        Yii::$app->db->createCommand(

                            "INSERT 
                                INTO filial_product (ware_id, amount, temp) 
                                VALUES 
                                    (   
                                        :ware_id, 
                                        :amount, 
                                        :temp
                                    ) 
                                ON DUPLICATE KEY 
                                UPDATE 
                                    amount = :amount"                        
                        )->bindValues([
                            ':ware_id' => $data['ware_id'],
                            ':amount' => (int) $value,
                            ':temp' => $key,
                        ])->execute();
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }                

                //$this->progressBar($count, 9196);
            }
            $botToken = "1131589058:AAFhKq9BLxrFjbc2gAeMiX0Pr8ThlFRTeRE";

                $website = "https://api.telegram.org/bot" . $botToken;
                $chats = [
                    106069427, //Ардак
                    415550928, //Асель
                    412368804  //Oleg
                ];  //** ===>>>NOTE: this chatId MUST be the chat_id of a person, NOT another bot chatId !!!**
                foreach ($chats as $chat) {
                    $params = [
                        'chat_id' => $chat,
                        'text' => 'Интеграция закончилась',
                    ];
                    $ch = curl_init($website . '/sendMessage');
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $result = curl_exec($ch);
                    curl_close($ch);
                }            
        }
    }

    public function progressBar($done, $total) {
        $perc = floor(($done / $total) * 100);
        $left = 100 - $perc;
        $write = sprintf("\033[0G\033[2K[%'={$perc}s>%-{$left}s] - $perc%% - $done/$total", "", "");
        fwrite(STDERR, $write);
    }

    public function actionRelations()
    {
        if (($handle = fopen("/home/admin/web/dobraya-apteka.kz/standart_n/warebase_new2.csv", "r")) !== FALSE) {
            $botToken = "1131589058:AAFhKq9BLxrFjbc2gAeMiX0Pr8ThlFRTeRE";

            $website = "https://api.telegram.org/bot" . $botToken;
            $chats = [
                106069427, //Ардак
                415550928, //Асель
                412368804  //Oleg
            ];  //** ===>>>NOTE: this chatId MUST be the chat_id of a person, NOT another bot chatId !!!**
            foreach ($chats as $chat) {
                $params = [
                    'chat_id' => $chat,
                    'text' => 'Обновление сопутствующих товаров, похожих товаров начато',
                ];
                $ch = curl_init($website . '/sendMessage');
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
            }
            
            $count = 0;

            $keys = [
                // 'group', //Не используется
                'name',
                'manufacturer',//Не используется
                'country',
                'Штрихкод', //Не используется
                'price',
                'category_name',
                'category_id',
                'subcategory_name',
                'subcategory_id',
                'ware_id',
                'remainders',
                'status_products', //Не используется
                'discount', 
                'soput_list',
                'analog_list',
                'present_list',
                'temp'
            ];
            while (($data = fgetcsv($handle, 1000, ";", '"')) !== FALSE) {
                if (!$count) {
                    $count++;
                    continue;
                }
                $count++;

                $data = array_map(function ($str) {
                    return iconv("Windows-1251", "UTF-8", $str);
                }, $data);

                if(count($keys) != count($data)){
                    continue;
                }
                $data = array_combine($keys, $data);

                foreach (explode(',', str_replace("'", '', $data['soput_list'])) as $relate_id) {
                    if(!empty($data['ware_id'])){
                        if($relate_id){
                            Yii::$app->db->createCommand(
                                "INSERT INTO sop_tovary (ware_id_1, ware_id_2) VALUES (:ware_id_1,:ware_id_2) ON DUPLICATE KEY UPDATE id = id"
                            )->bindValues([
                                ':ware_id_1' => $data['ware_id'],
                                ':ware_id_2' => $relate_id
                            ])->execute();
                        }
                    }
                }


                // foreach ($presents as $present) {
                //     $arr = [];
                //     foreach (explode(',', str_replace("'", '', $present['presents'])) as $present_id) {
                //         if ($t = Products::find()->where(['ware_id' => $present_id])->one()) {
                //             $arr[] = $t['id'];
                //         }
                //     }
                //     if (!($presentModel = Gift::find()->where(['product_id' => $present['id'], 'products' => implode(",", $arr)])->one())) {
                //         $presentModel = new Gift();
                //     }
    
                //     $presentModel->attributes = [
                //         'product_id' => $present['id'],
                //         'products' => implode(",", $arr),
                //         'type' => 0
                //     ];
                //     $presentModel->save(false);
                // }
                foreach (explode(',', str_replace("'", '', $data['analog_list'])) as $analog_id) {
                    if(!empty($data['ware_id'])){
                        if($analog_id){
                            Yii::$app->db->createCommand(
                                "INSERT INTO analogy (ware_id_1, ware_id_2) VALUES (:ware_id_1,:ware_id_2) ON DUPLICATE KEY UPDATE id = id"
                            )->bindValues([
                                ':ware_id_1' => $data['ware_id'],
                                ':ware_id_2' => $analog_id
                            ])->execute();
                        }
                    }
                }


                // $this->progressBar($count, 9089);
            }
            foreach ($chats as $chat) {
                $params = [
                    'chat_id' => $chat,
                    'text' => 'Обновление сопутствующих товаров, похожих товаров завершено',
                ];
                $ch = curl_init($website . '/sendMessage');
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }
    }
}
