<?php

use yii\widgets\LinkPager;

?>
<div class="main">
    <div class="container">
        <?=$this->render('/partials/breadcrumbs', ['page' => 'Адреса аптек']);?>
        <div class="main-title text-left">
            <h2>Адреса аптек</h2>
        </div>
        <div class="bg-white">
            <div class="pick-city">
                <p>Ваш город:</p>
                <select class="language-button selectpicker" onchange="location = this.options[this.selectedIndex].value;">
                    <? foreach (Yii::$app->view->params['cities'] as $value): ?>
                        <option value="/city/set-city?city=<?=$value->id;?>" <?=Yii::$app->session['city_id']==$value->id ? "selected" : "";?>>
                            <?= $value->name; ?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="map">
                <div id="map" style="width: 100%; height: 480px;border:0" ></div>
                <script>
                    ymaps.ready(init);
                    var center_map = [0, 0];
                    var map = "";
                    function init() {
                        map = new ymaps.Map('map', {
                            center: center_map,
                            zoom: 11,
                            controls: ['zoomControl','fullscreenControl']
                        });
                        // map.behaviors.disable('scrollZoom');
                        var myGeocoder = ymaps.geocode("<?=Yii::$app->session["city_name"];?>");
                        myGeocoder.then(
                            function (res) {
                                var street = res.geoObjects.get(0);
                                var coords = street.geometry.getCoordinates();
                                map.setCenter(coords);

                            },
                            function (err) {

                            }
                        );

                        <? foreach($filial as $v):?>
                            map.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                                balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/images/loc-icon.png',
                                preset: 'islands#icon',
                                iconColor: '#0095b6'
                            }));
                        <? endforeach;?>

                        $('.filial_search').on("keypress",function(e){
                            if(e.which === 13){
                                e.preventDefault();
                                map.geoObjects.removeAll();
                                text = $(this).val();
                                $(this).val("");
                                $.ajax({
                                    type: "GET",
                                    data: {text: text},
                                    url: "/site/get-search-result",
                                    success: function (response) {
                                        $('#filial').html(response);
                                    },
                                    error: function () {
                                        swal('Упс!', 'Что-то пошло не так.', 'error');
                                    }
                                });

                                $.ajax({
                                    type: "GET",
                                    dataType: "json",
                                    data: {text: text},
                                    url: "/site/get-search-map-result",
                                    success: function (data) {
                                        for (i = 0; i < data.length; i++) {
                                            latitude = data[i][0];
                                            longitude = data[i][1];
                                            address = data[i][2];
                                            phone = data[i][3];

                                            map.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + phone + '</div>'
                                            }, {
                                                iconLayout: 'default#image',
                                                iconImageHref: '/images/loc-ico.png',
                                                preset: 'islands#icon',
                                                iconColor: '#0095b6'
                                            }));
                                        }
                                    },
                                    error: function () {
                                        swal('Упс!', 'Что-то пошло не так.', 'error');
                                    }
                                });
                            }
                        });
                    }

                </script>
            </div>
            <div class="row">
                <? foreach ($filial as $value): ?>
                <? try {
                    $from_time = new DateTime($v->from_time);
                    $to_time = new DateTime($v->to_time);
                } catch (Exception $e) {
                } ?>
                <div class="col-sm-4">
                    <div class="drugstore-item">
                        <p class="green-text"><?= $value->address; ?></p>
                        <p><a href="tel:<?= $value->telephone; ?>"><?= $value->telephone; ?></a></p>
                        <p><?= $from_time->format('H:i'); ?> - <?= $to_time->format('H:i'); ?> ч.</p>
                    </div>
                </div>
                <? endforeach; ?>

            </div>
        </div>
    </div>
</div>
</div>
