<div class="container">
    <div class="row">
        <div class="offset-lg-4 col-lg-4 col-sm-12">
            <div class="authorization text-center">
                <h3>Авторизация</h3>
                <form class="">
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                           value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                    <div class="authorization-input">
                        <img src="/images/user.png" alt="">
                        <input type="text" placeholder="8(___)__-__" name="LoginForm[username]">
                        <script>
                            $('input[name="LoginForm[username]"]').inputmask("8(999) 999-9999");
                        </script>
                    </div>
                    <div class="authorization-input">

                        <img src="/images/lock-icon.png" alt="">
                        <input type="password" placeholder="Пароль" id="password-type" name="LoginForm[password]">
                        <img src="/images/show-icon.png" id="sea-pass" alt="" data-password="1">

                    </div>
                    <div class="authorization-button">
                        <button type="button" class="login-button btn btn-success">Войти</button>
                    </div>
                </form>
                <div class="authorization-button">
                    <a href="/site/sign-up">
                        <button class="btn btn-light">Авторизация</button>
                    </a>
                </div>
                <div class="authorization-button">
                    <a href="#" data-toggle="modal" data-target="#updatePassword">Забыли пароль?</a>
                </div>
            </div>
        </div>
    </div>


</div>
</div>
