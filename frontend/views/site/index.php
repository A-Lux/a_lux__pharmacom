<div class="desktop-version">
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-img">
                    </div>
                    <div class="category-hover-block">
                        <? foreach ($catalog as $value): ?>
                            <div class="category-hover-item">
                                <a href="<?= $value->getUrl(); ?>">
                                    <div class="category-hover-img">
                                        <img src="<?= $value->getDesktopImage(); ?>" alt="">
                                    </div>
                                    <p><?= $value->name; ?></p>
                                </a>
                            </div>
                        <? endforeach; ?>

                    </div>
                    <div class="main-title">
                        <div class="sep"></div>
                        <h2>КАТАЛОГ</h2>
                    </div>
                </div>
                <div class="offset-lg-2 col-lg-8 col-sm-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                               aria-controls="home" aria-selected="true">Популярные</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                               aria-controls="profile" aria-selected="false">Новинки</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                               aria-controls="contact" aria-selected="false">Акции и скидки</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12">
                    <div class="tab-content" id="myTabContent">
                        <!-- POPULAR START -->
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div id="carousel1" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <div class="row">
                                                <? $m = 0; ?>
                                                <?php $block1 = false;
                                                $block2 = false;
                                                $block3 = false; ?>
                                                <? foreach ($productHit as $value): ?>
                                                    <? $m++; ?>
                                                    <? if ($m < 9): ?>
                                                        <? $block1 = true; ?>
                                                        <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="row">
                                                <? $m = 0; ?>
                                                <? foreach ($productHit as $value): ?>
                                                    <? $m++; ?>
                                                    <? if ($m > 8 && $m < 17): ?>
                                                        <? $block2 = true; ?>
                                                        <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="row">
                                                <? $m = 0; ?>
                                                <? foreach ($productHit as $value): ?>
                                                    <? $m++; ?>
                                                    <? if ($m > 16 && $m < 25): ?>
                                                        <? $block3 = true; ?>
                                                        <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <ol class="carousel-indicators">
                                        <? if ($block1): ?>
                                            <li data-target="#carousel1" data-slide-to="0" class="active"></li>
                                        <? endif; ?>
                                        <? if ($block2): ?>
                                            <li data-target="#carousel1" data-slide-to="1"></li>
                                        <? endif; ?>
                                        <? if ($block3): ?>
                                            <li data-target="#carousel1" data-slide-to="2"></li>
                                        <? endif; ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- POPULAR END -->

                        <!-- NEWS START -->
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div id="carouse2" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <? $m = 0; ?>
                                            <? foreach ($productNew as $value): ?>
                                                <? $m++; ?>
                                                <? if ($m < 9): ?>
                                                    <? $block1 = true; ?>
                                                    <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <? $m = 0; ?>
                                            <? foreach ($productNew as $value): ?>
                                                <? $m++; ?>
                                                <? if ($m > 8 && $m < 17): ?>
                                                    <? $block2 = true; ?>
                                                    <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <? $m = 0; ?>
                                            <?php $block1 = false;
                                            $block2 = false;
                                            $block3 = false; ?>
                                            <? foreach ($productNew as $value): ?>
                                                <? $m++; ?>
                                                <? if ($m > 16 && $m < 25): ?>
                                                    <? $block3 = true; ?>
                                                    <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                <? endif; ?>

                                            <? endforeach; ?>

                                        </div>
                                    </div>
                                </div>
                                <ol class="carousel-indicators">
                                    <? if ($block1): ?>
                                        <li data-target="#carouse2" data-slide-to="0" class="active"></li>
                                    <? endif; ?>
                                    <? if ($block2): ?>
                                        <li data-target="#carouse2" data-slide-to="1"></li>
                                    <? endif; ?>
                                    <? if ($block3): ?>
                                        <li data-target="#carouse2" data-slide-to="2"></li>
                                    <? endif; ?>

                                </ol>
                            </div>
                        </div>
                        <!-- NEWS END -->

                        <!-- DISCOUNT START -->
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div id="carouse3" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <? $m = 0; ?>
                                            <?php $block1 = false;
                                            $block2 = false;
                                            $block3 = false; ?>
                                            <? foreach ($productDiscount as $value): ?>
                                                <? $m++; ?>
                                                <? if ($m < 9): ?>
                                                    <? $block1 = true; ?>
                                                    <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                <? endif; ?>

                                            <? endforeach; ?>

                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <? $m = 0; ?>
                                            <? foreach ($productDiscount as $value): ?>
                                                <? $m++; ?>
                                                <? if ($m > 8 && $m < 17): ?>
                                                    <? $block2 = true; ?>
                                                    <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                <? endif; ?>

                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <? $m = 0; ?>
                                            <? foreach ($productDiscount as $value): ?>
                                                <? $m++; ?>
                                                <? if ($m > 16 && $m < 25): ?>
                                                    <? $block3 = true; ?>
                                                    <?=$this->render('/partials/desktop_product_list', compact('value'))?>
                                                <? endif; ?>
                                            <? endforeach; ?>

                                        </div>
                                    </div>
                                </div>
                                <ol class="carousel-indicators">
                                    <? if ($block1): ?>
                                        <li data-target="#carouse3" data-slide-to="0" class="active"></li>
                                    <? endif; ?>
                                    <? if ($block2): ?>
                                        <li data-target="#carouse3" data-slide-to="1"></li>
                                    <? endif; ?>
                                    <? if ($block3): ?>
                                        <li data-target="#carouse3" data-slide-to="2"></li>
                                    <? endif; ?>
                                </ol>
                            </div>
                        </div>
                        <!-- DISCOUNT END -->
                    </div>
<!--                    <div class="show-all">-->
<!--                        <a href="">посмотреть все</a>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" id="relatedProductsForDiscount"></div>
    </div>

    <div class="pills-bg">
        <div class="container">
            <form action="/site/subscribe">
                <h2>Подпишитесь на рассылку и получайте скидки!</h2>
                <input type="email" name="Subscribe[email]" placeholder="Ваш E-mail">
                <div class="send">
                    <button type="button" class="btn-subscribe">Отправить</button>
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-title">
                    <div class="sep"></div>
                    <h2>Наши аптеки</h2>
                </div>
                <div class="map">
                    <div id="map" style="width: 100%; height: 450px;border:0"></div>

                    <script>
                        ymaps.ready(init);
                        var center_map = [0, 0];
                        var map = "";

                        function init() {
                            map = new ymaps.Map('map', {
                                center: center_map,
                                zoom: 11.4,
                                controls: ['zoomControl','fullscreenControl']
                            });
                            // map.behaviors.disable('scrollZoom');
                            var myGeocoder = ymaps.geocode("<?=Yii::$app->session["city_name"];?>");
                            myGeocoder.then(
                                function (res) {
                                    var street = res.geoObjects.get(0);
                                    var coords = street.geometry.getCoordinates();
                                    map.setCenter(coords);
                                },
                                function (err) {

                                }
                            );

                            <?foreach($address as $v):?>
                            map.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                                balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/images/loc-icon.png',
                                preset: 'islands#icon',
                                iconColor: '#0095b6'
                            }));
                            <?endforeach;?>
                        }
                    </script>
                </div>

                <div class="main-title">
                    <div class="sep"></div>
                    <h2>О КОМПАНИИ</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="about-text">
                    <p><?= \frontend\controllers\HelpController::cutStr($aboutUs->content, 598) ?></p>
                    <div class="more-btn">
                        <a href="<?= \yii\helpers\Url::to(['about/']); ?>">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <img src="/images/about-img.jpg" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</div>

<div class="mobile-version">
    <img src="images/banner.jpg" class="mobile-main" alt="">
    <div class="owl-carousel owl-theme">
        <? foreach ($catalog as $value): ?>
            <div class="item">
                <div class="category-hover-item">
                    <a href="<?= $value->getUrl(); ?>">
                        <div class="category-hover-img">
                            <img src="<?= $value->getMobileImage(); ?>" alt="<?= $value->name; ?>">
                        </div>
                        <p><?= $value->name; ?></p>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
    </div>


    <!-- POPULAR START -->
    <div class="main-title">
        <h2>ПОПУЛЯРНЫЕ</h2>
    </div>
    <div class="square-flex">
        <? $m = 0;?>
        <? foreach ($productHit as $value): ?>
            <? $m++;?>
            <?=$this->render('/partials/first_mobile_product_list', compact('value'))?>
            <? if($m > 5) break;?>
        <? endforeach; ?>
    </div>
<!--    <div class="see-all">-->
<!--        <a href="">Посмотреть все</a>-->
<!--    </div>-->
    <!-- POPULAR END -->


    <!-- NEWS START -->
    <div class="main-title">
        <h2>НОВИНКИ</h2>
    </div>
    <? $m = 0;?>
    <? foreach ($productNew as $value): ?>
        <? $m++;?>
        <?=$this->render('/partials/second_mobile_product_list', compact('value'))?>
        <? if($m > 5) break;?>
    <? endforeach; ?>
<!--    <div class="see-all">-->
<!--        <a href="">Посмотреть все</a>-->
<!--    </div>-->
    <!-- NEWS END -->

    <!-- DISCOUNT START -->
    <div class="main-title">
        <h2>АКЦИИ И СКИДКИ</h2>
    </div>
    <? $m = 0;?>
    <? foreach ($productDiscount as $value): ?>
        <? $m++;?>
        <?=$this->render('/partials/second_mobile_product_list', compact('value'))?>
        <? if($m > 5) break;?>
    <? endforeach; ?>
    <!--    <div class="see-all">-->
    <!--        <a href="">Посмотреть все</a>-->
    <!--    </div>-->
    <!-- DISCOUNT END -->

    <div class="about-text">
        <p><?= \frontend\controllers\HelpController::cutStrMob($aboutUs->content, 440) ?></p>
        <div id="demo" class="collapse"><?=$aboutContent[0]->content?></div>
        <div class="show-more">
            <img data-toggle="collapse" data-target="#demo" src="/images/show-more.png" alt="">
        </div>
    </div>
    <div class="collapse-dark-block" data-toggle="collapse" data-target="#demo2">
        <p>Каталог</p>
        <div class="arrow">
            <img src="/images/light-arrow.png" alt="">
        </div>
    </div>
    <? foreach ($catalog as $value): ?>
        <div id="demo2" class="collapse">

            <div class="collapse-block-light">
                <a href="/catalog/<?= $value->getUrl(); ?>" data-toggle="collapse" data-target="#demo<?=$value->id;?>">
                    <p><?= $value->name; ?></p>
                    <div class="arrow"><img src="/images/dark-arrow.png" alt=""></div>
                </a>
            </div>

            <? if($value->childs):?>
            <div id="demo<?=$value->id;?>" class="sublist collapse">
                <ul>
                    <? foreach ($value->childs as $child):?>
                        <li>
                            <a href="<?=$child->getUrl();?>">
                                <?= $child->name; ?>
                            </a>
                        </li>
                    <? endforeach;?>
                </ul>
            </div>
            <? endif;?>

        </div>
    <? endforeach; ?>
    <div class="main-title">
        <h2>НАШИ АПТЕКИ</h2>
    </div>
    <div class="mobile-map">
        <div id="mapMob" style="width: 100%; height: 450px;border:0"></div>
        <script>
            //document.getElementById("office-city").innerHTML="<?//=$city->name?>//";
            ymaps.ready(init);
            var center_map = [0, 0];
            var mapMob = "";


            function init() {
                mapMob = new ymaps.Map('mapMob', {
                    center: center_map,
                    zoom: 10.7,
                    controls: ['zoomControl','fullscreenControl']
                });
                mapMob.behaviors.disable('scrollZoom');
                var myGeocoder = ymaps.geocode("<?=Yii::$app->session["city_name"];?>");
                myGeocoder.then(
                    function (res) {
                        var street = res.geoObjects.get(0);
                        var coords = street.geometry.getCoordinates();
                        mapMob.setCenter(coords);
                    },
                    function (err) {

                    }
                );

                <?foreach($address as $v):?>
                mapMob.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                    balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/images/loc-icon.png',
                    preset: 'islands#icon',
                    iconColor: '#0095b6'
                }));
                <?endforeach;?>
            }
        </script>
    </div>
</div>
</div>

