<div class="main">
    <div class="container">
        <?=$this->render('/partials/breadcrumbs', ['page' => $page->text]);?>
        <div class="main-title text-left">
            <h2><?=$page->text;?></h2>
        </div>
        <div class="row bg-white">
            <?= $page->content; ?>
        </div>
    </div>
</div>