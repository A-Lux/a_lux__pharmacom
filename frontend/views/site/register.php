<?php

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>

<div class="container">
    <div class="authorization">
        <div class="text-center">
            <h3>Регистрация</h3>
        </div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">

            <div class="offset-lg-2 col-lg-4 col-sm-12">

                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="authorization-input">
                    <img src="/images/user.png" alt="">
                    <input type="text" placeholder="Фамилия" name="UserProfile[surname]">
                </div>
                <div class="authorization-input">
                    <img src="/images/user.png" alt="">
                    <input type="text" placeholder="Имя" name="UserProfile[name]">
                </div>
                <div class="authorization-input">
                    <img src="/images/user.png" alt="">
                    <input type="text" placeholder="Отчество" name="UserProfile[father]">
                </div>
                <div class="authorization-input">
                    <img src="/images/msg-icon.png" alt="">
                    <input type="text" placeholder="Почта" name="SignupForm[email]">
                </div>


                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="capcha-img">{image}<br />
                                            <div id="refresh-captcha" class="btn btn-ticked add_sl_per">
                                                Обновить
                                            </div>
                                        </div>',
                        'imageOptions' => [
                            'id' => 'my-captcha-image'
                        ]
                    ])->label(false)?>


                    <?php $this->registerJs("
                        $('body').on('click', '#refresh-captcha', function(e){
                        
                            e.preventDefault();
                            $('#my-captcha-image').yiiCaptcha('refresh');
                        });
                    "); ?>

                <div class="authorization-input">
                    <img src="/images/lock-icon.png" alt="">
                    <input type="text" placeholder="Введите код с картинки" name="SignupForm[verifyCode]">
                </div>


            </div>
            <div class="col-lg-4 col-sm-12 col-lg-offset-right-2">
                <div class="authorization-input">

                    <img src="/images/user.png" alt="">
                    <input type="text" placeholder="8(___)" class="phone-mask" name="SignupForm[username]">
                    <script>
                        $('input[name="SignupForm[username]"]').inputmask("8(999) 999-9999");
                    </script>
                </div>
                <div class="authorization-input">

                    <img src="/images/loc-icon.png">
                    <input type="date" placeholder="Дата рождения" name="UserProfile[date_of_birth]">

                </div>
                <div class="authorization-input">

                    <img src="/images/loc-icon.png">
                    <input type="text" placeholder="Адрес" name="UserAddress[address][]">

                </div>
                <div class="new-adres">
                    <p>+ Добавить адрес</p>
                </div>
                <div class="authorization-input">
                    <div class="authorization-loc">

                    </div>
                </div>
                <div class="authorization-input">

                    <img src="/images/lock-icon.png" alt="">
                    <input type="password" placeholder="Пароль" id="password-type2" name="SignupForm[password]">
                    <img src="/images/show-icon.png" id="sea-pass-1" alt="" data-password="2">

                </div>
                <div class="authorization-input">

                    <img src="/images/lock-icon.png" alt="">
                    <input type="password" placeholder="Повторите пароль" id="password-type3" name="SignupForm[password_verify]">
                    <img src="/images/show-icon.png" id="sea-pass-2" alt="" data-password="3">

                </div>

            </div>

        </div>
        <div class="regist-btn text-center">
            <button type="button" class="register-button btn">Регистрация</button>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>