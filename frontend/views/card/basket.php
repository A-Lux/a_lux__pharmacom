<?php
use yii\helpers\Url;
?>
    <div class="desktop-version">
        <div class="main">
            <div class="container">
                <?=$this->render('/partials/breadcrumbs', ['page' => 'Корзина']);?>
                <div class="main-title text-left">
                    <h2>корзина</h2>
                </div>
                
                <?php if(count($_SESSION['basket']) != 0 || count($_SESSION['gift']) != 0):?>


                <div class="table-responsive">
                    <table class="table">

                        <thead>
                            <tr>
                                <th>

                                </th>
                                <th>Количество</th>
                                <th>Стоимость</th>
                                <th>Итог</th>
                                <th></th>
                            </tr>
                        </thead>
                        
                    <? foreach ($_SESSION['gift'] as $v):?>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="product-item">

                                        <div class="image">
                                            <img src="<?= $v->getImage(); ?>" alt="">
                                        </div>
                                        <div class="text-block">
                                            <h6><?= $v->name; ?></h6>
                                            <p>Производитель: <?= $v->manufacturerName; ?></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="count-input">
                                        <span></span>
                                        <input class="quantity input-text" min="1" name="count" value="<?=$v->count?>" data-id="<?=$v->id?>" id="countProduct<?=$v->id?>" type="number" disabled>
                                        <span></span>
                                    </div>
                                </td>

                                    <td>Бесплатно</td>
                                    <td>Бесплатно</td>
                                <td>
                                    <div class="delete-icon btn-delete-product-from-basket-gift" data-id="<?=$v->id?>">
                                        <img src="/images/delete-ico.png" alt="">
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    <? endforeach; ?>
                        
                    <? foreach ($_SESSION['basket'] as $v):?>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="product-item">
                                        <div class="image">
                                            <img src="<?= $v->getImage(); ?>" alt="">
                                        </div>
                                        <div class="text-block">
                                            <h6><?= $v->name; ?></h6>
                                            <p>Производитель: <?= $v->manufacturerName; ?></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="count-input">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation-minus" data-id="<?=$v->id?>">-</span>
                                        <input class="quantity input-text" min="1" name="count" value="<?=$v->count?>" data-id="<?=$v->id?>" id="countProduct<?=$v->id?>" type="number" >
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation-plus" data-id="<?=$v->id?>">+</span>
                                    </div>
                                </td>

                                    <td><?=number_format($v->getCalculatePrice(), 0, '', ' ');?> ₸</td>
                                <td><p id="sumProduct<?=$v->id?>" style="margin-bottom:0;">
                                        <?= number_format(intval($v->calculatePrice * $v->count), 0, '', ' ');?> ₸</p></td>
                                <td>
                                    <div class="delete-icon btn-delete-product-from-basket" data-id="<?=$v->id?>">
                                        <img src="/images/delete-ico.png" alt="">
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    <? endforeach; ?>
                    </table>
                </div>

                <div class="clear-basket">
                    <div class="delete-icon btn-delete-all-product-from-basket">
                        <img src="/images/delete-ico.png" alt="">Очистить корзину
                    </div>

                </div>

                <div class="basket-select">
                    <select id="typePay">
                        <option value="1" <?=Yii::$app->session['payment_choice'] == 1 ? "selected" : "";?>>Наличными в аптеке</option>
                        <option value="2" <?=Yii::$app->session['payment_choice'] == 2 ? "selected" : "";?>>Онлайн оплата</option>
                    </select>
                </div>


                <? if(!Yii::$app->user->isGuest):?>
                    <div class="bonus-cost">
                        <p>
                            Оплата бонусами: <span><?=number_format($bonus, 0, '', ' ');?> ₸</span>
                        </p>
                    </div>
                    <div class="bonus-costt">
                        <input type="number" id="bonus" placeholder="Укажите сумму бонуса, чтобы использовать" min="0" max="<?= $bonus; ?>">
                    </div>
                <? endif;?>

                <div class="total-cost">
                    <p>Всего к оплате: <span id="sumBasket"><?=number_format($sum, 0, '', ' ');?> ₸</span></p>
                    <? if(!Yii::$app->user->isGuest):?>
                        <p><?=Yii::$app->view->params['basket_texts'][0]->text;?> <span id="sumBasket"><?=$discount_for_senior_citizen?> %</span></p>
                        <p><?=Yii::$app->view->params['basket_texts'][1]->text;?> <span id="sumBasket"><?=$discount_for_regular_customer?> %</span></p>
                    <? endif;?>


                    <div class="order-btn">
                        <p><?=Yii::$app->view->params['basket_texts'][2]->text;?></p>
                        <button type="button" class="button-pay"  ><?=Yii::$app->session['payment_choice'] == 1 ? "Оформить заказ":"Оплатить";?></button>
                    </div>
                </div>

                <?php else:?>
                    <div style="margin-top: 50px;margin-bottom: 200px;">
                        <p>В вашей корзине пусто :(</p>
                    </div>
                <? endif;?>
            </div>
        </div>
    </div>

    </div>
