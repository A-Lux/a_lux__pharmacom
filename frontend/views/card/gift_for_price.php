<?
$this->title = 'Вы получили подарок!';
?>

<div class="desktop-version">
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li>/</li>
                    <li>Подарок</li>
                </ul>
            </div>

            <div class="bonus-main__wrapper">

                <div class="bonus text-center">
                    <div class="row" id = "checkResult">
                        <div class="col-sm-12">
                            <div class="popup-title">
                                <h6> Вы получили подарок! Выберите любой <br> доступный</h6>
                            </div>
                        </div>
                    <? foreach ($_SESSION['gift_for_price'] as $v):?>
                        <div class="col-sm-3">
                                <div class="catalog-item">
                                    <div class="image">
                                        <img src="<?= $v->getImage(); ?>" alt="">
                                    </div>
                                    <div class="name">
                                        <p><?= $v->name; ?>
                                            <span> Производитель: <?= $v->manufacturerName; ?>)</span>
                                        </p>
                                    </div>
                                    <div class="price">
                                        <p>Подарок</p>
                                    </div>
                                    <a href="/card/save-gift?id=<?=$v->id;?>"><button class="btn btn-dark">Получить</button></a>
                                </div>
                        </div>
                    <? endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
