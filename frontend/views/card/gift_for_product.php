<?
$this->title = 'Вы получили подарок!';
?>

<div class="desktop-version">
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li>/</li>
                    <li>Подарок</li>
                </ul>
            </div>

            <div class="bonus-main__wrapper">

                <div class="bonus text-center">
                    <div class="row" id = "checkResult">
                        <div class="col-sm-12">
                            <div class="popup-title">
                                <h6> Вы получили подарок! Выберите любой <br> доступный</h6>
                            </div>
                        </div>
                        <? foreach ($_SESSION['gift_for_product'] as $v):?>
                           <?=$this->render('/partials/gift_product_list', compact('v'));?>
                        <? endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
