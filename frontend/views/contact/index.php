<div class="main">
    <div class="container">
        <?=$this->render('/partials/breadcrumbs', ['page' => 'контакты']);?>
        <div class="main-title text-left">
            <h2>контакты</h2>
        </div>
        <div class="row bg-white">
            <div class="col-sm-4">
                <div class="fot-text">
                    <h6><i class="fas fa-map-marker-alt"></i>Адрес:</h6>
                    <p><?= $contact->address; ?></p>
                    <h6><i class="fas fa-phone"></i>Телефон:</h6>
                    <p><a href="tel:<?= $contact->telephone; ?> "><?= $contact->telephone; ?> </a><br>
                        <a href="tel:<?= $contact->phone; ?> "><?= $contact->phone; ?> </a></p>
                    <h6><i class="far fa-envelope"></i>Почта:</h6>
                    <a href="mailto:<?= $contact->email; ?>"><p><?= $contact->email; ?></p></a>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27650.53961549817!2d76.90095778794546!3d43.242934053436734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1561008870511!5m2!1sru!2skz"
                            width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
</div>