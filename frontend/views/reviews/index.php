<?php

use yii\widgets\LinkPager;
use yii\widgets\Pjax;

?>
<div class="main">
    <div class="container">
        <?=$this->render('/partials/breadcrumbs', ['page' => 'Отзывы']);?>
        <div class="main-title text-left">
            <h2>Отзывы</h2>
        </div>
        <div class="bg-white ">
            <? foreach ($reviews['data'] as $value): ?>
            <div class="review-item">
                <div class="date">
                    <p><?= $value->getDate(); ?></p>
                </div>
                <div class="name">
                    <p><?= $value->name; ?></p>
                </div>
                <div class="rating">
                    <? for ($i = 1; $i <= $value->raiting; $i++): ?>
                        <label for="star5" style="color: #48cf88; font-size: 20px;">&#x2605</label>
                    <? endfor; ?>
                    <? for ($i = $value->raiting+1; $i <= 5; $i++): ?>
                        <label for="star5" style="font-size: 20px;">&#x2605</label>
                    <? endfor; ?>
                </div>
                <div class="text">
                    <p><?= $value->message; ?></p>
                </div>
            </div>
            <? endforeach;?>
            <div class="pagination">
                <ul>
                    <?=LinkPager::widget([
                        'pagination' => $reviews['pagination'],
                        'activePageCssClass' => 'active',
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '',
                        'prevPageCssClass' => ['class' => 'prev-page'],
                        'nextPageLabel' => '&raquo;',
                        'nextPageCssClass' => ['class' => 'next-page'],
                        // Настройки контейнера пагинации
                        'options' => [
                            'tag' => 'div',
                            'class' => 'pagination',
                        ],

                        // Настройки классов css для ссылок

                    ]);
                    ?>
                </ul>
            </div>
            <div class="comment-title">
                <h5>Оставить комментарий на сайте</h5>
            </div>
            <form action="/site/comment">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="comment-input">
                            <label for="">Имя</label>
                            <input type="text" name="Reviews[name]" value="<?= $profile->name; ?>" required>
                        </div>
                        <div class="comment-input">
                            <label for="">Телефон</label>
                            <input type="text" name="Reviews[phone]" value="<?= $user->username; ?>" required>
                        </div>
                        <div class="comment-input">
                            <label for="">Почта</label>
                            <input type="text" name="Reviews[email]" value="<?= $user->email; ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="comment-input">
                            <label for="">Ваш комментарий</label>
                            <textarea name="Reviews[message]" id="" cols="30" rows="10"
                                      placeholder="Введите текст сообщения" required></textarea>
                        </div>
                        <div class="send-btn">
                            <button type="button" class="sendReviews">Отправить</button>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="comment-input">
                            <label for="">Поставьте свою оценку</label>
                        </div>


                        <div class="rating raiting-r">
                            <input id="star5" name="Reviews[raiting]" type="radio" value="5" class="radio-btn hide"/>
                            <label for="star5" style="font-size: 20px;">&#x2605</label>
                            <input id="star4" name="Reviews[raiting]" type="radio" value="4" class="radio-btn hide"/>
                            <label for="star4" style="font-size: 20px;">&#x2605</label>
                            <input id="star3" name="Reviews[raiting]" type="radio" value="3" class="radio-btn hide"/>
                            <label for="star3" style="font-size: 20px;">&#x2605</label>
                            <input id="star2" name="Reviews[raiting]" type="radio" value="2" class="radio-btn hide"/>
                            <label for="star2" style="font-size: 20px;">&#x2605</label>
                            <input id="star1" name="Reviews[raiting]" type="radio" value="1" class="radio-btn hide"/>
                            <label for="star1" style="font-size: 20px;">&#x2605</label>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

