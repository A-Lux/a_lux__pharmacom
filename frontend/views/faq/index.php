<div class="main">
    <div class="container">
        <?=$this->render('/partials/breadcrumbs', ['page' => 'Часто задаваемые вопросы']);?>
        <div class="main-title text-left">
            <h2>Часто задаваемые вопросы</h2>
        </div>
        <div class="bg-white questions">
            <div class="wrapper-question">
                <ul class="navbar-nav">
                    <? foreach ($faq as $value): ?>
                    <li class="nav-item wrapper-question-tab"><a href="#wrapper-text-<?=$value->id;?>" class="nav-link wrapper-question-tab">
                            <?= $value->question ?><img src="/images/list-down.png"></a>
                        <div class="wrapper-question-text" style="display:none;" id="wrapper-text-<?=$value->id;?>">
                            <p><?= $value->answer; ?></p>
                        </div>
                    </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>