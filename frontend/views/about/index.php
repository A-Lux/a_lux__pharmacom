<div class="main">
    <div class="container">
        <?=$this->render('/partials/breadcrumbs', ['page' => 'О компании']);?>
        <div class="main-title text-left">
            <h2>О компании</h2>
        </div>
        <div class="bg-white">
            <div class="green-title">
                <h2 class="green-text">О нас</h2>
            </div>
            <div class="about-us-text">
                <img src="<?= $aboutUs->getImage(); ?>" class="img-fluid" alt="">
                <?= $aboutUs->content; ?>
            </div>
            <? foreach ($aboutContent as $value): ?>
            <div class="about-item">
                <div class="image">
                    <img src="<?= $value->getImage(); ?>" alt="">
                </div>
                <?= $value->content; ?>
            </div>
            <? endforeach; ?>

            <div class="green-title m-lg">
                <h2 class="green-text">наши история</h2>
            </div>
            <div class="row">
                <? foreach ($aboutStory as $value):?>
                <div class="col">
                    <div class="history-item">
                        <img src="<?= $value->getImage(); ?>" alt="">
                        <?= $value->content; ?>
                    </div>
                </div>
                <? endforeach; ?>

            </div>
            <div class="green-title  m-lg">
                <h2>наши услуги</h2>
            </div>
            <div class="row">
                <div class="offset-lg-1 col-lg-10">
                    <div class="row">
                        <? foreach ($aboutServices as $value): ?>
                        <div class="col-sm-4">
                            <div class="service-item">
                                <img src="<?= $value->getImage(); ?>" alt="">
                                <?= $value->content; ?>
                            </div>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
