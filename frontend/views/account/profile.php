<?php

use yii\widgets\LinkPager;

?>
<div class="main">
<!-- ЛИЧНЫЙ-КАБИНЕТ -->

<div class="personal-area">
    <div class="container">
        <div class="link-home">
            <a href="/">Главная/</a>
            <a>Личный кабинет</a>
        </div>
        <div class="personal-area-title">
            <h2>ЛИЧНЫЙ КАБИНЕТ</h2>
        </div>
        <div class="personal-area-block">
            <div class="personal-data text-center">
                <ul class="row">
                    <li class="col-12 col-lg-3 <?=$tab == 'tab-1'?"personal-tab-active":""?>" tab="tab-1"
                        onclick="setProfileBlock('tab-1')">Личные данные</li>
                    <li class="col-12 col-lg-3 <?=$tab == 'tab-2'?"personal-tab-active":""?>" tab="tab-2"
                        onclick="setProfileBlock('tab-2')">Мои заказы</li>
                    <li class="col-12 col-lg-3 <?=$tab == 'tab-3'?"personal-tab-active":""?>" tab="tab-3"
                        onclick="setProfileBlock('tab-3')">Бонусы</li>
                    <li class="col-12 col-lg-3 <?=$tab == 'tab-4'?"personal-tab-active":""?>" tab="tab-4"
                        onclick="setProfileBlock('tab-4')">Избранное</li>
                </ul>
            </div>
            <div id="tab-1" class="personal-body <?=$tab == 'tab-1'?"personal-active":""?>">
                <form class="row">
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                           value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                    <div class="col-12 col-lg-3">

                        <label for="">Имя</label>
                        <input type="text" placeholder="Имя" value="<?= $profile->name; ?>" name="UserProfile[name]">

                        <label for="">Фамилия</label>
                        <input type="text" placeholder="Фамилия" value="<?= $profile->surname; ?>"
                               name="UserProfile[surname]">

                        <label for="">Отчество</label>
                        <input type="text" placeholder="Отчество" value="<?= $profile->father; ?>"
                               name="UserProfile[father]">

                    </div>
                    <div class="col-12 col-lg-3">

                        <label for="">Телефон</label>
                        <input type="text" placeholder="Логин" value="<?= $user->username; ?>" name="User[username]" disabled>

                        <label for="">Почта</label>
                        <input type="text" placeholder="Почта" value="<?= $user->email; ?>" name="User[email]">

                        <label for="">Адрес</label>
                        <? $m = 0;?>
                        <? foreach ($address as $v): ?>
                            <? $m++;?>
                            <input type="text" placeholder="Адрес" value="<?= $v->address; ?>" name="UserAddress[address][]">
                            <? if($m == 1):?>
                                <? break;?>
                            <? endif;?>
                        <? endforeach; ?>

                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="personal-adres">
                            <p>+ Добавить адрес</p>
                            <div class="personal-adres-input">
                                <div class="adress">
                                    <? $m = 0;?>
                                    <? foreach ($address as $v): ?>
                                        <? $m++;?>
                                        <? if($m > 1):?>
                                            <div class="input-box" style="position: relative;">
                                                <i class="fas fa-minus-circle close-adres" onclick="$(this).closest('div').remove();"></i>
                                                <input type="text"  placeholder="Адрес..." value="<?= $v->address; ?>" name="UserAddress[address][]">
                                            </div>
                                        <? endif;?>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="personal-link">
                            <button type="button" class="update-profile-button">Сохранить</button>
                            <br>
                            <a href="#" data-toggle="modal" data-target="#exampleModal">Сменить пароль</a>
                        </div>
                    </div>
                </form>


            </div>

            <div id="tab-2" class="personal-body <?=$tab == 'tab-2'?"personal-active":""?>">
                <table class="table">

                    <tr>
                        <th scope="col">Заказ</th>
                        <th scope="col">Дата оформление</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Сумма</th>
                    </tr>

                    <? foreach ($orders['data'] as $v): ?>
                        <tr>
                            <th scope="row"><?= $v->id; ?></th>
                            <td><?= $v->created_at; ?></td>
                            <td><?= $v->statusProgress  == 1 ? "Получено" : "Не получено"; ?></td>
                            <td><?= $v->sum; ?> тг</td>
                        </tr>
                    <? endforeach; ?>
                </table>

                <?=$this->render('/partials/pagination', ['pagination' => $orders['pagination']]);?>
            </div>
            <!-- БОНУС -->
            <div id="tab-3" class="personal-body <?=$tab == 'tab-3'?"personal-active":""?>">
                <div class="bonus text-center">
                    <img src="/images/bonus.png" alt="">
                    <?if($userBonus):?>
                        <p>У вас <?=$userBonus->bonus;?> бонус</p>
                    <?else:?>
                        <p>На данный момент у Вас отсутствуют бонусы</p>
                    <?endif;?>
                </div>
                <div class="row text-center">

                    <? if ($gifts != null): ?>
                    <? foreach($gifts as $v):?>
                            <?$product = $v->product?>
                            <div class="col-lg-3">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <img src="<?= $product->getImage(); ?>" alt="">
                                        </div>
                                        <div class="name">
                                            <p><?= $product->name; ?>
                                                <span> Производитель: <?= $product->manufacturerName; ?></span>
                                            </p>
                                        </div>
                                        <div class="price">
                                            <?//= $value->price; ?>
                                           <p>Подарок</p>
                                        </div>
                                        <div class="basket">
                                            <button class="btn-in-basket-gift"
                                                    data-id="<?= $product->id; ?>"><img
                                                        src="/images/basket-ico-light.png"
                                                        alt="">Заказать
                                            </button>
                                        </div>
                                    </div>
                            </div>
                        <? endforeach;?>
                        <? endif; ?>
                    
                </div>
            </div>
            <!-- БОНУС -->

            <!-- ИЗБРАННОЕ -->
            <div id="tab-4" class="personal-body <?=$tab == 'tab-4'?"personal-active":""?>">
               <?=$this->render("favorite", compact('favorites'))?>
            </div>

            <!-- ИЗБРАННОЕ -->

        </div>


    </div>
</div>



<!-- ЛИЧНЫЙ КАБИНЕТ -->

<!-- Мои заказы -->



    

</div>
</div>


