<div class="favorites">
    <div class="row">
        <? if ($favorites == null): ?>
            Ваша избранное пуста!
        <? endif; ?>

        <? if ($favorites != null): ?>
            <? foreach ($favorites as $v): ?>
                <div class="col-sm-4">
                    <div class="catalog-item">
                        <? if($v->discount != null):?>
                            <div class="promo">
                                <a href=""><img src="images/promo.png" alt=""></a>
                            </div>
                        <? endif;?>
                        <div class="delete-from-fav">
                            <a class="delete-icon btn-delete-product-from-favorite"
                               data-id="<?= $v->id; ?>"><img src="images/delete-ico.png" alt=""></a>
                        </div>
                        <div class="image">
                            <img src="<?=$v->getImage(); ?>" alt="">
                        </div>
                        <div class="name">
                            <p><?= $v->name ?>
                                <span>Производитель: <?= $v->manufacturerName; ?> </span
                            </p>
                        </div>
                        <div class="price">
                            <? if($v->discount != null):?>
                                <p><?=$v->getPrice();?> ₸ <span><?=$v->price;?> ₸</span></p>
                            <? else:?>
                                <p><?=$v->price;?> ₸ </p>
                            <? endif;?>
                        </div>
                        <div class="basket">
                            <button class="btn btn-dark btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/basket-ico-light.png" alt="">Купить</button>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        <? endif; ?>

    </div>

</div>
