<div class="row text-center">
    <? if ($gift != null): ?>
        <? foreach ($gift as $value): ?>
        <div class="col-lg-3">
                <div class="catalog-item">
                    <div class="image">
                        <img src="<?= $value->getImage(); ?>" alt="">
                    </div>
                    <div class="name">
                        <p><?= $value->name; ?>
                            <span> Производитель: <?= $value->manufacturer; ?> (<?= $value->country; ?>)</span>
                        </p>
                    </div>
                    <div class="price">
                        <?//= $value->price; ?>
                       <p>Подарок</p>
                    </div>
                    <div class="basket">
                        <button class="btn-in-basket-gift"
                                data-id="<?= $value->id; ?>"><img
                                    src="/images/basket-ico-light.png"
                                    alt="">Заказать
                        </button>
                    </div>
                </div>
        </div>
        <? endforeach; ?>
    <? endif; ?>
</div>