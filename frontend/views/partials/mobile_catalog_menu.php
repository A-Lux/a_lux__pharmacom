<ul>
    <? foreach ($catalog as $value): ?>
        <li>
            <a href="<?=$value->getUrl(); ?>"><?= $value->name; ?></a>
            <? if($value->childs):?>
                <?=$this->render('/partials/mobile_catalog_menu', ['catalog' => $value->childs]);?>
            <? endif;?>
        </li>
    <? endforeach; ?>
</ul>