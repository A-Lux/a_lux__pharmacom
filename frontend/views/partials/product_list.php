<div class="col-lg-4 col-md-6 col-sm-6">
    <div class="catalog-item">
        <?if($v->discount != null):?>
            <div class="promo">
                <a href="<?=$v->getUrl();?>"><img src="/images/promo.png" alt=""></a>
            </div>
        <?endif;?>
        <div class="add-to-fav">
            <a class="btn-add-favorite" data-id="<?=$v->id;?>" data-user-id="<?=Yii::$app->user->id?>"><img src="/images/star-ico.png" alt=""></a>
        </div>
        <div class="image">
            <?if($v->status_products == 1){?>
                <div class="red-label">
                    Дефицит
                </div>
            <?}?>
            <a href="<?=$v->getUrl();?>">
                <img src="<?= $v->getImage(); ?>" width="104" height="104">
            </a>
        </div>
        <div class="name">
            <p>
                <?= $v->name; ?>
                <? if(isset($v->manufacturerName)):?>
                    <span> Производитель: <?= $v->manufacturerName; ?> </span>
                <? endif;?>
            </p>
        </div>
        <div class="price">
            <p><?=number_format($v->calculatePrice, 0, '', ' ');?> ₸</p>
        </div>

        <div class="basket">
            <? if($v->checkRemainder()):?>
            <button class="btn-in-basket" data-id="<?=$v->id;?>" data-slider-id="relatedProductsFromCatalog">
                <img src="/images/basket-ico-light.png" alt="" >Купить</button>
            <? endif;?>
        </div>

    </div>
</div>


