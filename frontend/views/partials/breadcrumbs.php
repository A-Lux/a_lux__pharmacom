<div class="bread-crumbs">
    <ul>
        <li><a href="/">Главная</a></li>
        <li>/</li>
        <? if(isset($catalog)):?>
            <li><a href="#">Каталог</a></li>
            <?=$this->render('/catalog/breadcrumbs', ['parent' => $catalog, 'product' => isset($product) ? $product : null]);?>

        <? else:?>
            <li><a><?=$page;?></a></li>
        <? endif;?>
    </ul>
</div>