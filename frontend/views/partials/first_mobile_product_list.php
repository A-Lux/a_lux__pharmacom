<div class="catalog-item">
    <div class="add-to-fav">
        <a class="btn-add-favorite" data-id="<?= $value->id; ?>"
           data-user-id="<?= Yii::$app->user->id ?>"><img src="/images/star-ico.png" alt=""></a>
    </div>
    <div class="image">
        <? if ($value->status_products == 1) { ?>
            <div class="red-label">
                Дефицит
            </div>
        <? } ?>
        <a href="/product/<?= $value->url ?>"><img src="<?= $value->getImage(); ?>" alt=""></a>
    </div>
    <div class="name">
        <p><?= $value->name; ?>
            <span> Производитель: <?= $value->manufacturerName; ?></span></p>
    </div>
    <div class="price">
        <p><?= number_format($value->calculatePrice, 0, '', ' ');?> ₸</p>
    </div>
    <div class="basket">
        <button class="btn-in-basket" data-id="<?= $value->id; ?>"><img src="/images/basket-ico-light.png"
                                                                        alt="">Купить
        </button>
    </div>
</div>