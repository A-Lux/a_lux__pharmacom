<?php

use yii\widgets\LinkPager;
use yii\widgets\Pjax;

?>
<div class="main">
    <div class="container">
        <div class="bread-crumbs">
            <ul>
                <li><a href="">Главная</a></li>
                <li>/</li>
                <li>Вакансии</li>
            </ul>
        </div>
        <div class="main-title text-left">
            <h2>Вакансии</h2>
        </div>
        <div class="bg-white jobs">
            <div class="row">
                <? foreach ($vacancy['data'] as $value): ?>
                <div class="col-sm-12">
                    <div class="job-card">
                        <p class="job-card-title"><?= $value->title; ?></p>
                        <p class="job-card-text"><?= $value->content; ?></p>
                        <ul>
                            <li class="salary"><span><?= $value->salary; ?></span></li>
                            <li class="time"><span><?= $value->time; ?></span></li>
                            <li class="location"><span><?= $value->location; ?></span></li>
                        </ul>
                    </div>
                </div>
                <? endforeach; ?>
                <div class="col-sm-12">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <?=LinkPager::widget([
                                'pagination' => $vacancy['pagination'],
                                'activePageCssClass' => 'active',
                                'hideOnSinglePage' => true,
                                'prevPageLabel' => '',
                                'prevPageCssClass' => ['class' => 'prev-page'],
                                'nextPageLabel' => '&raquo;',
                                'nextPageCssClass' => ['class' => 'next-page'],
                                // Настройки контейнера пагинации
                                'options' => [
                                    'tag' => 'ul',
                                    'class' => 'pagination',
                                ],

                                // Настройки классов css для ссылок

                            ]);
                            ?>
<!--                            <li class="page-item"><a class="page-link" href="#">1</a></li>-->
<!--                            <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--                            <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--                            <li class="page-item">-->
<!--                                <a class="page-link" href="#" aria-label="Next">-->
<!--                                    <span aria-hidden="true">&raquo;</span>-->
<!--                                </a>-->
<!--                            </li>-->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
</div>