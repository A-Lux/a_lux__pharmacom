<div class="main">
        <!-- ХЛЕБНЫЕ КРОШКИ -->
        <div class="bread-crumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li>/</li>
                    <li class="active"><a >Поиск</a></li>
                </ul>
            </div>
        </div>
        <!-- END ХЛЕБНЫЕ КРОШКИ -->

        <div class="container">
            <?php
            if($count){?>
                <div class="" style="font-size: 22px;margin-bottom: 30px;margin-right: 500px;">Результаты поиска по запросу <span style="font-weight: bold;"><?=$keyword?></span>.</div>
            <?}else{?>
                <div class="" style="font-size: 22px;margin-bottom: 300px;">По запросу <span style="font-weight: bold;"><?=$keyword?></span> ничего не найдено.</div>
            <?}?>

            <? if(count($product) != 0):?>

                <div class="col-lg-9">
                    <div class="tablet-version-hide row items">
                        <? foreach ($product as $v):?>
                            <?=$this->render('/partials/product_list', compact('v'));?>
                        <? endforeach;?>
                    </div>
                </div>


                <?=$this->render('/partials/pagination', compact('pagination'));?>

            <? endif;?>

        </div>


</div>
</div>

