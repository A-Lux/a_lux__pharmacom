
<? if($sop_tovary):?>

    <div class="col-sm-12">
        <div class="main-title text-left">
            <? if(!empty($sop_tovary)):?>
                <h2>Сопутствующие товары</h2>
            <? endif;?>
        </div>
    </div>
    <?foreach ($sop_tovary as $v):?>
        <? $product = $v->product;?>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="catalog-item">
                <div class="add-to-fav">
                    <a class="btn-add-favorite" data-id="<?=$product->id;?>" data-user-id="<?=Yii::$app->user->id?>"><img src="/images/star-ico.png" alt=""></a>
                </div>
                <div class="image">
                    <?if($product->status_products == 1){?>
                        <div class="red-label">
                            Дефицит
                        </div>
                    <?}?>
                    <a href="/product/<?=$product->url?>"><img src="<?= $product->getImage();?>" alt=""></a>
                </div>
                <div class="name">
                    <p><?=$product->name?>
                        <span> Производитель: <?= $product->manufacturerName; ?></span></p>
                </div>
                <div class="price">
                    <p><?= $product->calculatePrice; ?> ₸</p>
                </div>
                <div class="basket">
                    <button class="btn-in-basket" data-id="<?=$product->id;?>"><img src="/images/basket-ico-light.png" alt="">Купить</button>
                </div>
            </div>

        </div>
    <?endforeach; ?>

<? endif;?>
