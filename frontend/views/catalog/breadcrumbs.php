<? if($parent->parent != null):?>
    <?=$this->render('/catalog/breadcrumbs', ['parent' => $parent->parent])?>
<? endif;?>

<li>/</li>
<? if($parent->parent == null):?>
    <li><a href="<?=$parent->getUrl();?>"><?=$parent->name;?></a></li>
<? else:?>
    <? if($product != null):?>
        <li><a href="<?=$parent->getUrl();?>"><?=$parent->name;?></a></li>
        <li>/</li>
        <li><a ><?=$product->name;?></a></li>
    <? else:?>
        <li><a ><?=$parent->name;?></a></li>
    <? endif;?>
<? endif;?>


