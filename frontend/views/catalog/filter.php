<div class="row">
    <input type="hidden" value="<?=$category_id?>" class="category_id">
    <? if($products != null):?>
        <? foreach ($products as $v): ?>
            <?=$this->render("/partials/product_list", compact('v'));?>
        <?endforeach;?>
    <? endif;?>
</div>

<?=$this->render('/partials/pagination', compact('pagination'));?>
