
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?=$this->render('/partials/breadcrumbs', compact('catalog', 'product'));?>
            </div>
            <div class="col-sm-5">
                <div class="product-img">
                    <img src="<?= $product->getImage(); ?>" class="img-fluid" alt="">
                </div>
                <div class="product-description">
                    <ul>
                        <li><p>Наличие</p><span><?=$product->status ? 'Есть' : 'Нет'; ?> в наличии  <?if($product->status_products == 1){?>
                                    (дефицит)
                                <?}?> </span></li>
                        <li><p>Модель</p><span><?=$product->model;?></span></li>
                        <li><p>Производитель</p><span><?=$product->manufacturerName;?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="inner-title">
                    <h3>
                        <?= $product->name;?>
                    </h3>
                </div>
                <div class="rating">
                    <img src="/images/raiting.png" alt="">
                </div>
                <div class="product-price">
                    <p><?= number_format($product->calculatePrice, 0, '', ' '); ?> ₸</p>
                </div>

                <? if($product->checkRemainder()):?>
                    <div class="count-input">
                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                        <input class="quantity input-text" min="1" max="<?=$product->remainderAmount;?>" name="count" value="1" type="number" id="amount">
                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                    </div>
                <? endif;?>

                <div class="button-row">
                    <? if($product->checkRemainder()):?>
                        <div class="basket-dark">
                            <button class="btn-in-basket" data-id="<?=$product->id;?>">
                                <img src="/images/basket-ico-light.png" alt="">Купить</button>
                        </div>
                    <? endif;?>
                    <button class="btn-add-favorite" data-id="<?=$product->id;?>" data-user-id="<?=Yii::$app->user->id?>">В избранное</button>

                </div>
            </div>
            <div class="col-sm-12">
                <div class="main-title text-left">
                    <? if(!empty($sop_tovary)):?>
                    <h2>Сопутствующие товары</h2>
                    <? endif;?>
                </div>
            </div>

            <? if($sop_tovary != null):?>
                <? foreach ($sop_tovary as $v):?>
                    <?=$this->render('/partials/product_list', compact('v'));?>
                <?endforeach; ?>
            <? endif;?>


            <div class="col-sm-12">
                <div class="main-title text-left">
                    <? if(!empty($analogy)):?>
                        <h2>Аналоги лекарственных средств</h2>
                    <? endif;?>
                </div>
            </div>

            <? if($analogy != null):?>
                <? foreach ($analogy as $v):?>
                    <?=$this->render('/partials/product_list', compact('v'));?>
                <?endforeach; ?>
            <? endif;?>

        </div>
    </div>
</div>
</div>
