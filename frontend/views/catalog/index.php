
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?=$this->render('/partials/breadcrumbs', compact('catalog'));?>
                <div class="bg-white">
                    <div class="inner-title">
                        <h3>
                            <?= $catalog->name; ?>
                        </h3>
                    </div>
                    <div class="catalog-categories">
                        <? if(count($categories) > 4):?>
                            <? $eachCol = count($categories)/4;$m=0;?>
                            <? foreach ($categories as $k => $v):?>
                                <? $m++;?>
                                <? if($m % $eachCol == 1):?>
                                <div class="col-sm-6 col-lg-3">
                                <? endif;?>
                                    <div class="list-item">
                                        <a href="<?=$v->getUrl($url="",$v);?>">
                                            <p><?=$v->title;?></p>
                                        </a>
                                    </div>
                                <? if($m % $eachCol == 0):?>
                                </div>
                                <? endif;?>
                            <? endforeach;?>

                            <? if($m % $eachCol != 0):?>
                            </div>
                            <? endif;?>
                        <? else:?>
                            <? foreach ($categories as $k => $v):?>
                                <div class="col-sm-6 col-lg-3">
                                    <div class="list-item">
                                        <a href="<?=$v->getUrl($url="",$v);?>">
                                            <p><?=$v->title;?></p>
                                        </a>
                                    </div>
                                </div>
                            <? endforeach;?>
                        <?endif;?>
                    </div>
                    <? if($catalog->parent != null):?>
                        <div class="row-list">
                            <div class="back-link">
                                <a href="<?=$catalog->parent->getUrl();?>">
                                    <span>&#8592</span>
                                    Назад к разделу <?=$catalog->parent->name;?>
                                </a>
                            </div>
                        </div>
                    <? endif;?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bg-white">
                    <div class="box-title">
                        <h6>Цена</h6>
                    </div>

                    <div class="filter-row">
                        <p><label for="from">От</label></p>
                        <input type="number" id="from" class="from_price" value="<?=$filter["from"]?>">
                        <p>тг</p>
                    </div>
                    <div class="filter-row">
                        <p><label for="to">До</label></p>
                        <input type="number" id="to" class="to_price" value="<?=$filter["to"]?>">
                        <p>тг</p>
                    </div>
                </div>
                <div class="bg-white">
                    <div class="box-title">
                        <h6>Фильтры</h6>
                    </div>
                    <div class="checkbox-list">
                        <ul>

                            <li>
                                <label class="checkbox-button">
                                    <input type="checkbox" class="isNew checkbox-button__input" id="choice1-1" name="choice1"
                                        <?=$filter['isNew'] == 1 ? 'checked="checked"':'';?>>
                                    <span class="checkbox-button__control"></span>
                                    <span class="checkbox-button__label"> Новые</span>
                                </label>
                            </li>
                            <li>
                                <label class="checkbox-button">
                                    <input type="checkbox" checked="checked" class="available checkbox-button__input" id="choice1-1" name="choice1"
                                        <?=$filter['available'] == 1 ? 'checked="checked"':'';?>>
                                    <span class="checkbox-button__control"></span>
                                    <span class="checkbox-button__label">Есть в наличии</span>
                                </label>
                            </li>
                            <li>
                                <label class="checkbox-button">
                                    <input type="checkbox" class="discount checkbox-button__input" id="choice1-1" name="choice1"
                                        <?=$filter['discount'] == 1 ? 'checked="checked"':'';?>>
                                    <span class="checkbox-button__control"></span>
                                    <span class="checkbox-button__label">Акционные товары</span>
                                </label>
                            </li>
                            <li>
                                <label class="checkbox-button">
                                    <input type="checkbox" class="isHit checkbox-button__input" id="choice1-1" name="choice1"
                                        <?=$filter['isHit'] == 1 ? 'checked="checked"':'';?>>
                                    <span class="checkbox-button__control"></span>
                                    <span class="checkbox-button__label">Популярные</span>
                                </label>
                            </li>
                            <li>
                                <label class="checkbox-button">
                                    <input type="checkbox" class="top_month checkbox-button__input" id="choice1-1" name="choice1"
                                        <?=$filter['top_month'] == 1 ? 'checked="checked"':'';?>>
                                    <span class="checkbox-button__control"></span>
                                    <span class="checkbox-button__label"> Товар месяца</span>
                                </label>
                            </li>
                        </ul>
<!--                        <div class="apply">-->
<!--                            <button class="btn-apply">Применить</button>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="bg-white">
                    <div class="gift">
                        <img src="/images/gift.png" alt="">
                        <p>Получайте бонусы за каждую покупку и в любой момент
                            обменивайте их на подарки!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 tablet-version-hide">
                <?=$this->render('/catalog/filter', compact('pagination', 'products', 'category_id'));?>
            </div>

            <div class="container">
                <div class="row" id="relatedProductsFromCatalog"></div>
            </div>
        </div>
    </div>
</div>
</div>
