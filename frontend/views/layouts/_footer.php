<!-- FOOTER START -->
<div class="desktop-version">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-sm-6">
                    <div class="fot-logo">
                        <a href=""><img src="/images/fot-logo.png" class="img-fluid" alt=""></a>
                    </div>
                </div>
                <div class="offset-lg-1 col-lg-2 col-sm-6">
                    <div class="fot-list">
                        <ul>
                            <? foreach (Yii::$app->view->params['footer_menu'] as $value): ?>
                                <? if($value->url == '#'):?>
                                    <li>
                                        <a href class="catalog-toggle">
                                           <?= $value->text;?> <span>+</span>
                                        </a>
                                    </li>
                                <? else:?>
                                    <li>
                                        <a href="<?= $value->getUrl(); ?>">
                                            <?= $value->text; ?>
                                        </a>
                                    </li>
                                <? endif;?>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="offset-lg-1 col-lg-3 col-sm-6">
                    <div class="fot-text">
                        <h6><i class="fas fa-map-marker-alt"></i>Головной офис:</h6>
                        <p><?= Yii::$app->view->params['contact']->address;?></p>
                        <h6><i class="fas fa-phone"></i>Телефон:</h6>
                        <p>
                            <a href="tel:<?= Yii::$app->view->params['contact']->telephone;?>">
                                <?= Yii::$app->view->params['contact']->telephone;?>
                            </a> <br>
                            <a href="tel:<?= Yii::$app->view->params['contact']->phone;?>">
                                <?= Yii::$app->view->params['contact']->phone;?>
                            </a>
                        </p>
                        <h6><i class="far fa-envelope"></i>E-mail:</h6>
                        <a href="mailto:<?= Yii::$app->view->params['contact']->email;?>">
                            <p><?= Yii::$app->view->params['contact']->email;?></p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="fot-text">
                        <p class="no-space-left">У Вас остались еще вопросы?
                            Оставьте заявку и наши менеджера
                            в скором времени с Вами свяжутся!</p>
                    </div>
                    <div class="call-btn">
                        <a href="#" data-toggle="modal" data-target="#feedback">
                            <img src="/images/phone-ico-light.png" >
                            Позвоните мне
                        </a>
                    </div>
                    <div class="socials">
                        <ul>
                            <? if(Yii::$app->view->params['contact']->instagram):?>
                                <li>
                                    <a href="<?=Yii::$app->view->params['contact']->instagram;?>" target="_blank">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            <? endif;?>
                            <? if(Yii::$app->view->params['contact']->vk):?>
                                <li>
                                    <a href="<?=Yii::$app->view->params['contact']->vk;?>" target="_blank">
                                        <i class="fab fa-vk"></i>
                                    </a>
                                </li>
                            <? endif;?>
                            <? if(Yii::$app->view->params['contact']->facebook):?>
                                <li>
                                    <a href="<?=Yii::$app->view->params['contact']->facebook;?>" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                            <? endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="dark-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="copyright">
                        <p><?=Yii::$app->view->params['logo']->copyright;?></p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="dev-link">
                        <a href="https://a-lux.kz" target="_blank">Сайт разработан в: <img src="/images/alux.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mobile-version">
    <div class="collapse-block-light toggle-open">
        <a href="" data-toggle="collapse" data-target="#contact">
            <p>Контакты</p>
            <div class="toggle-open">
                <div class="plus">
                    +
                </div>
                <div class="minus">
                    -
                </div>
            </div>
        </a>
    </div>
    <div id="contact" class="collapse-block-light collapse">
        <div class="fot-text">
            <h6><i class="fas fa-map-marker-alt"></i>Головной офис:</h6>
            <p>
                <?= Yii::$app->view->params['contact']->address;?>
            </p>
            <h6><i class="fas fa-phone"></i>Телефон:</h6>
            <p>
                <a href="tel:<?= Yii::$app->view->params['contact']->telephone;?> " >
                    <?= Yii::$app->view->params['contact']->telephone;?>
                </a><br>
                <a href="tel:<?= Yii::$app->view->params['contact']->phone;?> ">
                    <?= Yii::$app->view->params['contact']->phone;?>
                </a>
            </p>
            <h6><i class="far fa-envelope"></i>E-mail:</h6>
            <a href="mailto:<?= Yii::$app->view->params['contact']->email;?>">
                <p>
                    <?= Yii::$app->view->params['contact']->email;?>
                </p>
            </a>
        </div>
    </div>
    <div class="collapse-block-light toggle-open">
        <a href="" data-toggle="collapse" data-target="#socials">
            <p>Мы в соцсетях</p>
            <div class="toggle-open">
                <div class="plus">
                    +
                </div>
                <div class="minus">
                    -
                </div>
            </div>
        </a>
    </div>
    <div id="socials" class="collapse-block-light collapse socials">
        <ul>
            <? if(Yii::$app->view->params['contact']->instagram):?>
                <li>
                    <a href="<?=Yii::$app->view->params['contact']->instagram;?>" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
            <? endif;?>
            <? if(Yii::$app->view->params['contact']->vk):?>
                <li>
                    <a href="<?=Yii::$app->view->params['contact']->vk;?>" target="_blank">
                        <i class="fab fa-vk"></i>
                    </a>
                </li>
            <? endif;?>
            <? if(Yii::$app->view->params['contact']->facebook):?>
                <li>
                    <a href="<?=Yii::$app->view->params['contact']->facebook;?>" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
            <? endif;?>
        </ul>
    </div>
    <div class="mobile-footer">
        <div class="copyright">
            <p><?= Yii::$app->view->params['logo']->copyright; ?></p>
            <p>Сайт разработан в: <a href="https://a-lux.kz" target="_blank">A-Lux</a></p>
        </div>
    </div>
</div>
<!-- FOOTER END -->
<?php $this->endBody(); ?>
</body>
</html>
