<?

use yii\helpers\Html;

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=b5acda67-323a-48a6-9bf4-1666f0b3e106&lang=ru_RU" type="text/javascript"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
<?php $this->beginBody() ?>

<script>
    <? if (Yii::$app->session['modal-pass-reset']) : ?>
        swal("Уважаемый пользователь.", "Ваш старый пароль был сброшен. Новый пароль был отправлен на вашу электронную почту.");
        <? unset(Yii::$app->session['modal-pass-reset']); ?>
    <? endif; ?>

    <? if (Yii::$app->session['guest-order-success']) : ?>
        swal("Спасибо!", "Ваш заказ принят!", "success");
        <? unset(Yii::$app->session['guest-order-success']); ?>
    <? endif; ?>
</script>

<!-- HEADER START -->
<div class="content">
    <div class="desktop-version">
        <header class="header">
            <div class="container">
                <div class="row header_align">
                    <div class="col-lg-4 col-sm-4">
                        <div class="logo">
                            <? if($_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/site/index"):?>
                                <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" class="img-fluid" alt="">
                            <? else:?>
                                <a href="/"><img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" class="img-fluid" alt=""></a>
                            <? endif;?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <div class="work-time">
                            <p>График работы</p>
                            <a ><?= Yii::$app->view->params['contact']->working;?></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <div class="work-time">
                            <p>Ваш город</p>
                            <select  class="language-button selectpicker" onchange="location = this.options[this.selectedIndex].value;">
                                <? foreach (Yii::$app->view->params['cities'] as $value):?>
                                    <option value="/city/set-city?city=<?=$value->id;?>" <?=Yii::$app->session['city_id'] == $value->id ? "selected":"";?>>
                                        <?= $value->name; ?></option>
                                <? endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <div class="work-time">
                            <p style="cursor: pointer" data-toggle="modal" data-target="#feedback">Заказать звонок</p>
                            <a href="tel:<?= Yii::$app->view->params['contact']->telephone;?>"><?= Yii::$app->view->params['contact']->telephone;?></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-8">
                        <div class="icon-row">
                            <a href="tel:<?= Yii::$app->view->params['contact']->phone;?>" class="phone-icon">
                                <img src="/images/phone-ico.png" alt="">
                            </a>
                            <a href="<?= \yii\helpers\Url::toRoute(['/account', 'tab' => 'tab-4']);?>" class="fav-icon">
                                <img src="/images/star-ico.png" alt="">
                            </a>
                            <a href="<?= \yii\helpers\Url::toRoute(['/card']);?>" class="basket-icon">
                                <img src="/images/basket-ico.png" alt="">
                                <span class="count"><?= Yii::$app->view->params['count'];?></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4 space-top-sm">
                        <div class="menu">
                            <div class="hamburger hamburger--spring">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                            <p class="close-menu">закрыть</p>
                            <p class="open-menu">меню</p>
                        </div>
                        <div class="menu-level1">
                            <ul>
                                <? foreach ( Yii::$app->view->params['header_menu'] as $v):?>
                                    <? if($v->url == '#'):?>
                                        <li>
                                            <a href class="catalog-toggle">
                                                <span class="icon">
                                                    <img src="<?= $v->getDesktopImage();?>" alt="">
                                                </span>
                                                <p><?= $v->text;?> <span>+</span></p>
                                            </a>
                                        </li>
                                    <? else:?>
                                        <li>
                                            <a href="<?= \yii\helpers\Url::toRoute([$v->getUrl()]);?>">
                                                <span class="icon">
                                                   <img src="<?= $v->getDesktopImage();?>" alt="">
                                                </span>
                                                <p><?= $v->text;?></p>
                                            </a>
                                        </li>
                                    <? endif;?>
                                <? endforeach;?>
                            </ul>

                            <div class="socials">
                                <ul>
                                    <? if(Yii::$app->view->params['contact']->instagram):?>
                                        <li>
                                            <a href="<?=Yii::$app->view->params['contact']->instagram;?>" target="_blank">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    <? endif;?>
                                    <? if(Yii::$app->view->params['contact']->vk):?>
                                        <li>
                                            <a href="<?=Yii::$app->view->params['contact']->vk;?>" target="_blank">
                                                <i class="fab fa-vk"></i>
                                            </a>
                                        </li>
                                    <? endif;?>
                                    <? if(Yii::$app->view->params['contact']->facebook):?>
                                        <li>
                                            <a href="<?=Yii::$app->view->params['contact']->facebook;?>" target="_blank">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                    <? endif;?>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="hidden-menu">
                        <div class="menu-level2 ">
                            <div class="row">
                                <? $sumEachCol = (int)(count( Yii::$app->view->params['catalog'])/3);?>
                                <div class="col-sm-4">
                                    <ul>
                                        <? $m=0;?>
                                        <? foreach ( Yii::$app->view->params['catalog'] as $k => $v):?>
                                            <? if($m <= $sumEachCol):?>
                                                <li><a href="<?=$v->getUrl();?>"><?=$v->title;?></a></li>
                                            <? endif;?>
                                            <? $m++;?>
                                        <? endforeach;?>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>
                                        <? $m=0;?>
                                        <? foreach ( Yii::$app->view->params['catalog'] as $k => $v):?>
                                            <? if($m > $sumEachCol && $m <= $sumEachCol*2):?>
                                                <li><a href="<?=$v->getUrl();?>"><?=$v->title;?></a></li>
                                            <? endif;?>
                                            <? $m++;?>
                                        <? endforeach;?>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>
                                        <? $m=0;?>
                                        <? foreach ( Yii::$app->view->params['catalog'] as $k => $v):?>
                                            <? if($m > $sumEachCol*2):?>
                                                <li><a href="<?=$v->getUrl();?>"><?=$v->title;?></a></li>
                                            <? endif;?>
                                            <? $m++;?>
                                        <? endforeach;?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8 col-sm-8 space-top-sm">
                        <form action="<?= \yii\helpers\Url::toRoute(['/search']);?>">
                            <div class="search">
                                <input type="text" placeholder="Введите название лекарства/препарата" name="text" id="searchbox">
                                <button><img src="/images/search-ico.png" alt="">Найти</button>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-2 col-sm-4 space-top-sm">
                        <? if(!Yii::$app->user->isGuest):?>
                            <div class="auth">
                                <a href="<?=\yii\helpers\Url::toRoute(['/account', 'tab' =>'tab-1']);?>">
                                    <p>Личный кабинет/</p>
                                </a>
                            </div>
                            <div class="register">
                                <a href="<?= \yii\helpers\Url::toRoute(['/site/logout']);?>">Выйти</a>
                            </div>
                            <? endif;?>
                            <? if(Yii::$app->user->isGuest):?>
                                <div class="auth">
                                    <a href="<?= \yii\helpers\Url::toRoute(['/site/sign-in']);?>">Вход/</a>
                                </div>
                                <div class="register">
                                    <a href="<?= \yii\helpers\Url::toRoute(['/site/sign-up']);?>">Регистрация</a>
                                </div>
                        <? endif;?>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <div class="mobile-version">
        <div class="mobile-header">
            <div class="container">
                <div class="mobile-row">
                    <nav id="main-nav">
                        <ul>
                            <li>
                                <div class="menu-logo">
                                    <img src="/images/logo-menu.png" class="img-fluid" alt="">
                                </div>
                            </li>

                            <? foreach ( Yii::$app->view->params['header_menu'] as $v):?>
                                <li>
                                    <? if($v->url == '#'):?>
                                        <a href="#">
                                            <img src="<?=$v->getMobileImage();?>" alt="">
                                            <?= $v->text;?>
                                        </a>
                                        <?=$this->render('/partials/mobile_catalog_menu', ['catalog' => Yii::$app->view->params['catalog']]);?>
                                    <? else:?>
                                        <a href="<?= \yii\helpers\Url::toRoute([$v->getUrl()]);?>">
                                            <img src="<?=$v->getMobileImage();?>" alt="">
                                            <?= $v->text;?>
                                        </a>
                                    <? endif;?>
                                </li>
                            <? endforeach;?>
                        </ul>
                    </nav>

                    <div class="logo">
                        <a href="/"><img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" class="img-fluid" alt=""></a>
                    </div>

                    <div class="head-icon">
                        <a href=""><img src="/images/phone-ico.png" alt="" ></a>
                    </div>

                    <div class="head-icon">
                        <a href="<?= \yii\helpers\Url::toRoute(['/account', 'tab' => 'tab-4']);?>">
                            <img src="/images/star-ico.png" alt="" ></a>
                    </div>

                    <div class="head-icon icon-row">
                        <a href="<?= \yii\helpers\Url::toRoute(['/card']);?>" class="basket-icon">
                            <img src="/images/basket-ico.png" alt="" >
                            <span class="count"><?= Yii::$app->view->params['count'];?></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="mobile-search">
            <div class="container">
                <form action="/search">
                    <div class="search">
                        <input type="text" placeholder="Введите название лекарства/препарата" name="text">
                        <button><img src="/images/search-ico.png" alt="">Найти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- HEADER END -->
