<div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="updatePassword" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="updatePassword">Забыли пароль?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="modal-text">
                        <p></p>
                    </div>
                    <div class="authorization-input">
                        <input type="text" placeholder="Ваш email" id="forget_password_email">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn sendForgotPassword">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Сменить пароль</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="modal-text">
                        <p></p>
                    </div>
                    <div class="authorization-input">
                        <input type="password" id="password-type" placeholder="Старый пароль" name="PasswordUpdate[old_password]">
                        <img src="/images/show-icon.png" id="sea-pass" alt="" data-password="1">
                    </div>
                    <div class="authorization-input">
                        <input type="password" id="password-type2" placeholder="Новый пароль" name="PasswordUpdate[password]">
                        <img src="/images/show-icon.png" id="sea-pass-1" alt="" data-password="2">
                    </div>
                    <div class="authorization-input">
                        <input type="password" id="password-type3" placeholder="Подтвердите новый пароль" name="PasswordUpdate[password_verify]">
                        <img src="/images/show-icon.png" id="sea-pass-2" alt="" data-password="3">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn update-password-button">Сохранить</button>
                    <a href="#" data-toggle="modal" data-target="#updatePassword" onclick="$('#exampleModal').modal('hide');">Забыли пароль?</a>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="feedbackModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Заказать звонок</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="authorization-input">
                        <input id="feedback-name" type="text" class="form-control"  placeholder="Имя *" name="FeedbackForm[name]">
                    </div>
                    <div class="authorization-input">
                        <input id="feedback-phone" type="text" class="form-control"  placeholder="Номер телефона *" name="FeedbackForm[phone]">
                    </div>
                    <div class="authorization-input">
                        <input id="feedback-time" type="text" class="form-control"  placeholder="Удобное время для звонка" name="FeedbackForm[time]">
                    </div>
                    <div class="authorization-input">
                        <textarea id="feedback-comment" type="text" class="form-control"  placeholder="Ваши комментарии" name="FeedbackForm[comment]" cols="10" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn saveFeedback" >Жду звонка</button>
                </div>
            </form>
        </div>
    </div>
</div>








<div class="modal fade card-confirm-modal" id="card-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-personal">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Пожалуйста, заполните следующие поля</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="modal-body">
                    <div class="modal-text">
                        <p></p>
                    </div>
                    <div class="authorization-input">
                        <input type="text" id="order-fio" placeholder="Ваше ФИО" name="OrderFormForGuest[fio]">
                    </div>
                    <div class="authorization-input">
                        <input type="text" id="order-telephone" placeholder="Ваш номер телефона" name="OrderFormForGuest[telephone]">
                    </div>
                    <div class="authorization-input">
                        <input type="text" id="order-email" placeholder="Ваш e-mail" name="OrderFormForGuest[email]">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn order_guest_confirm">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#order-telephone').inputmask("8(999) 999-9999");
    $('#feedback-phone').inputmask("8(999) 999-9999");
    $('input[name="OrderFormForGuest[fio]"]').keyup(function(e) {
        var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
        if (regex.test(this.value) !== true)
            this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
    });
</script>
