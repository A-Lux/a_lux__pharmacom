<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li>/</li>
                    <li>Часто задаваемые вопросы</li>
                </ul>
            </div>
            <div class="main-title text-left">
                <h2>Часто задаваемые вопросы</h2>
            </div>
            <div class="bg-white questions">
                <div class="wrapper-question">
                    <ul class="navbar-nav">
                        <li class="nav-item wrapper-question-tab"><a href="#wrapper-text-1" class="nav-link">Как посмотреть историю заказов? <img src="images/list-down.png"></a>
                            <div class="wrapper-question-text" style="display:none;" id="wrapper-text-1">
                                <p>Для того чтобы просматривать историю заказов Вам необходимо быть зарегистрированным на сайте в личном кабинете в разделе «Мои заказы» будет отображаться история заказов. </p>
                            </div>
                        </li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">В течение какого времени осуществляется доставка? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">В каких городах действует онлайн оплата? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">Как произвести онлайн оплату? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">При оплате онлайн снимается ли комиссия за оплату? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">Каковы условия оформления заказа через АО КазПочта? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">Как отследить заказ при доставке АО КазПочта? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">Нужна ли регистрация для оформления заказа? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">Что нужно для регистрации? <img src="images/list-down.png"></a></li>
                        <li class="nav-item"><a href="#" class="nav-link wrapper-question-tab">На какой номер звонить для уточнения заказа? <img src="images/list-down.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>