<div class="desktop-version">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-sm-6">
                    <div class="fot-logo">
                        <a href=""><img src="images/fot-logo.png" class="img-fluid" alt=""></a>
                    </div>
                </div>
                <div class="offset-lg-1 col-lg-2 col-sm-6">
                    <div class="fot-list">
                        <p> О компании</p>
                        <ul>
                            <li><a href="catalog.php">Каталог</a></li>
                            <li><a href="cabinet.php">Личный кабинет</a></li>
                            <li><a href="contacts.php">Контакты</a></li>
                            <li><a href="">Доставка</a></li>
                            <li><a href="">Оплата</a></li>
                            <li><a href="faq.php">Часто задаваемые вопросы</a></li>
                            <li><a href="">Поиск лекарств в аптеках</a></li>
                            <li><a href="vacancies.php">Вакансии</a></li>
                            <li><a href="reviews.php">Отзывы</a></li>
                        </ul>
                    </div>
                </div>
                <div class="offset-lg-1 col-lg-3 col-sm-6">
                    <div class="fot-text">
                        <h6><i class="fas fa-map-marker-alt"></i>Адрес:</h6>
                        <p>050004, Республика Казахстан, <br>
                            г.Алматы, ул. Проспект <br>Нурсултана Назарбаева, 51</p>
                        <h6><i class="fas fa-phone"></i>Телефон:</h6>
                        <p>+7 (968) 666-41-23 <br>
                            +7 (968) 666-41-23</p>
                        <h6><i class="far fa-envelope"></i>Почта:</h6>
                        <a href="mailto:pharmacom@mail.ru"><p>pharmacom@mail.ru</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="fot-text">
                        <p class="no-space-left">У Вас остались еще вопросы?
                            Оставьте заявку, и наши менеджера
                            в скором времени с Вами свяжутся!</p>
                    </div>
                    <div class="call-btn">
                        <a href=""><img src="images/phone-ico-light.png" alt="">Позвоните мне</a>
                    </div>
                    <div class="socials">
                        <p>Мы в соц. сетях </p>
                        <ul>
                            <li><a href=""><i class="fab fa-instagram"></i></a></li>
                            <li><a href=""><i class="fab fa-vk"></i></a></li>
                            <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="dark-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="copyright">
                        <p>© ТОО «Фармаком»</p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="dev-link">
                        <a href="">Сайт создан в: <img src="images/alux.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mobile-version">
    <div class="collapse-block-light toggle-open">
        <a href="" data-toggle="collapse" data-target="#contact">
            <p>Контакты</p>
            <div class="toggle-open">
                <div class="plus">
                    +
                </div>
                <div class="minus">
                    -
                </div>
            </div>
        </a>
    </div>
    <div id="contact" class="collapse-block-light collapse">
        <div class="fot-text">
            <h6><i class="fas fa-map-marker-alt"></i>Адрес:</h6>
            <p>050004, Республика Казахстан, <br>
                г.Алматы, ул. Проспект <br>Нурсултана Назарбаева, 51</p>
            <h6><i class="fas fa-phone"></i>Телефон:</h6>
            <p><a href="tel:+7(968)6664123 ">+7 (968) 666-41-23 </a><br>
                <a href="tel:+7(968)6664123 ">+7 (968) 666-41-23 </a></p>
            <h6><i class="far fa-envelope"></i>Почта:</h6>
            <a href="mailto:pharmacom@mail.ru"><p>pharmacom@mail.ru</p></a>
        </div>
    </div>
    <div class="collapse-block-light toggle-open">
        <a href="" data-toggle="collapse" data-target="#socials">
            <p>Мы в соцсетях</p>
            <div class="toggle-open">
                <div class="plus">
                    +
                </div>
                <div class="minus">
                    -
                </div>
            </div>
        </a>
    </div>
    <div id="socials" class="collapse-block-light collapse socials">
        <ul>
            <li><a href=""><i class="fab fa-instagram"></i></a></li>
            <li><a href=""><i class="fab fa-vk"></i></a></li>
            <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
        </ul>
    </div>
    <div class="mobile-footer">
        <div class="copyright">
            <p>© ТОО «Фармаком»</p>
            <p>Сайт создан в: <a href="http://a-lux.kz">A-Lux</a></p>
        </div>
    </div>
</div>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/hc-offcanvas-nav.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
</html>
