(function($) {
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots:true,
        responsive:{
            0:{
                items:3
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
    $('.minus').hide();
    $(".toggle-open").click(function(){
        $(this).find('.plus, .minus').toggle();
    });
    $(".show-more").click(function(){
        $('.show-more img').toggleClass('rotate');
    });
    $(".collapse-dark-block").click(function(){
        $('.collapse-dark-block img').toggleClass('rotate');
    });
    $(".catalog-list .list-item").click(function(e){
        e.preventDefault();
        $(this).find('.fas').toggleClass('rotate2');
    });
    $(".collapse-block-light").click(function(){
        $(this).find('.arrow img').toggleClass('rotate2');
    });
    $('#main-nav').hcOffcanvasNav({
        maxWidth: 980
    });
    $(".menu").click(function(){
       $('.close-menu').toggle();
       $('.open-menu').toggle();
       $('.hamburger').toggleClass('is-active');
       $('.menu-level1').slideToggle();
        $('.hidden-menu').slideUp();
    });
    $(".catalog-toggle").click(function(e){
        e.preventDefault();
        $('.hidden-menu').toggle();
    });

    // question tabs
    $('.wrapper-question-tab').click(function(e){
        e.preventDefault();
        var attr = $(this).children('a').attr('href');
        var children = 	$(this).children('a');
        $(this).children('a').toggleClass('active-btn');
        $(children).children('img').toggleClass('active-img');
        if($(attr).css("display") == "none"){
            $(attr).animate({height: 'show'}, 500);
        }else{
            $(attr).animate({height: 'hide'}, 500);
        }
    });

    $('.personal-data ul li').click(function(){
        var tab = $(this).attr('tab');

        $('.personal-data ul li').removeClass('personal-tab-active');
        $(this).addClass('personal-tab-active');

        $('.personal-body').removeClass('personal-active');
        $('#'+tab).addClass('personal-active');

    });

    $('#addAdress').click(function(e){
        e.preventDefault();
        $('.adres-input').append('<input type="text"> </input>');
    });
    new WOW().init();
})(jQuery);







