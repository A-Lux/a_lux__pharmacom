<?php require_once 'header.php'?>
<div class="main">
    <!-- ЛИЧНЫЙ-КАБИНЕТ -->
    <div class="personal-area">
        <div class="container">
            <div class="link-home">
                <a href="#">Главная/</a>
                <a href="#">Линый кабинет</a>
            </div>
            <div class="personal-area-title">
                <h2 >ЛИЧНЫЙ КАБИНЕТ</h2>
            </div>
            <div class="personal-area-block">
                <div class="personal-data text-center">
                    <ul class="row">
                        <li class="col-12 col-lg-3 personal-tab-active" tab="tab-1">Личные данные</li>
                        <li class="col-12 col-lg-3" tab="tab-2">Мои заказы</li>
                        <li class="col-12 col-lg-3" tab="tab-3">Бонусы</li>
                        <li class="col-12 col-lg-3" tab="tab-4">Избранное</li>
                    </ul>
                </div>
                <div id="tab-1" class="personal-body personal-active">
                    <div class="row">
                        <div class="col-12 col-lg-3">
                            <form action="">
                                <label for="">Имя</label>
                                <input type="text">
                                <label for="">Фамилия</label>
                                <input type="text">
                                <label for="">Телефон</label>
                                <input type="text">
                            </form>
                        </div>
                        <div class="col-12 col-lg-3">
                            <form action="">
                                <label for="">Почта</label>
                                <input type="text">
                                <label for="">Адрес</label>
                                <input type="text">
                                <label for="">Пароль</label>
                                <input type="text">
                            </form>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="adres-input" style="display: flex;flex-direction: column;"></div>
                            <div class="personal-adres">
                                <a href="#" id="addAdress"><i class="fas fa-plus-circle"></i>Добавить адрес</a>
                            </div>

                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="personal-link">
                                <a href="#">Редактировать</a>
                                <br>
                                <a href="#">Сменить пароль</a>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- БОНУС -->
                <div id="tab-3" class="personal-body">
                    <div class="bonus text-center">
                        <img src="images/bonus.png" alt="">
                        <p>У вас 563 бонус</p>
                        <button class="btn btn-dark">Потратить</button>
                    </div>
                </div>
            </div>
            <!-- БОНУС -->

            <!-- ИЗБРАННОЕ -->
            <div id="tab-4" class="personal-body">
                <div class="favorites">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="delete-from-fav">
                                    <a href=""><img src="images/delete-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img2.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="promo">
                                    <a href=""><img src="images/promo.png" alt=""></a>
                                </div>
                                <div class="delete-from-fav">
                                    <a href=""><img src="images/delete-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img3.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>3 121 ₸ <span>3485 ₸</span></p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="delete-from-fav">
                                    <a href=""><img src="images/delete-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img2.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <!-- ИЗБРАННОЕ -->
        </div>
    </div>


    <!-- ЛИЧНЫЙ КАБИНЕТ -->

    <!-- Мои заказы -->
    <div class="container">
        <div id="tab-2" class="personal-body table-responsive">
            <table class="table">
                <tr>
                    <th scope="col">Заказ</th>
                    <th scope="col">Дата оформление</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Сумма</th>
                </tr>
                <tr>
                    <th scope="row">2-Хлорпропил-бис-(β-хлорэтил)-амина гидрохлорид</th>
                    <td>19. 04. 2019 13:52</td>
                    <td>Доставлено</td>
                    <td>1200 тг</td>
                </tr>
                <tr>
                    <th scope="row">Амоксициллин + клавулановая кислота</th>
                    <td>19. 04. 2019 13:52</td>
                    <td>Доставлено</td>
                    <td>1200 тг</td>
                </tr>
                <tr>
                    <th scope="row">Алзолам</th>
                    <td>19. 04. 201913:52</td>
                    <td>Доставлено</td>
                    <td>1200 тг</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>
<?php require_once 'footer.php'?>

