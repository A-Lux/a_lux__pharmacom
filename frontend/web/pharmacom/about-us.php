<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li>/</li>
                    <li>О компании</li>
                </ul>
            </div>
            <div class="main-title text-left">
                <h2>О компании</h2>
            </div>
            <div class="bg-white">
                <div class="green-title">
                    <h2 class="green-text">О нас</h2>
                </div>
                <div class="about-us-text">
                    <img src="images/about-us-img.jpg" class="img-fluid" alt="">
                    <p class="green-text">Мы ежедневно снабжаем тысячи покупателей доступными лекарственными препаратами широкого ассортимента с профессиональным сервисом и консультацией высококвалифицированных специалистов.</p>
                    <p>Мы стараемся предложить клиентам высоко профессиональный сервис, через консультации сертифицированных провизоров/фармацевтов, а также врачей-консультантов, как индивидуальную услугу для каждого клиента с мини диагностикой и рекомендациями.</p>
                    <p>Мы имеем в наличие широкий ассортимент лекарственных препаратов (отечественного и западного пр-ва), а также большой выбор сопутствующих товаров, медицинского назначения, детских товаров и лечебной косметики. Наши цены прозрачны и выгодны для всех слоёв населения (с обязательным наличием ЖВЛС). </p>
                </div>
                <div class="about-item">
                    <div class="image">
                        <img src="images/about-icon1.png" alt="">
                    </div>
                    <p>Мы стремимся соответствовать всем лучшим международным стандартам качества для аптек, вводимым в РК. Мы даём возможность
                        обучения и информирования клиентов о новшествах в медицине, фармацевтике и законодательстве Министерства Здравоохранения РК.</p>
                </div>
                <div class="about-item">
                    <div class="image">
                        <img src="images/about-icon2.png" alt="">
                    </div>
                    <p>Мы стремимся соответствовать всем лучшим международным стандартам качества для аптек, вводимым в РК. Мы даём возможность
                        обучения и информирования клиентов о новшествах в медицине, фармацевтике и законодательстве Министерства Здравоохранения РК.</p>
                </div>
                <div class="about-item">
                    <div class="image">
                        <img src="images/about-icon3.png" alt="">
                    </div>
                    <p>Мы стремимся соответствовать всем лучшим международным стандартам качества для аптек, вводимым в РК. Мы даём возможность
                        обучения и информирования клиентов о новшествах в медицине, фармацевтике и законодательстве Министерства Здравоохранения РК.</p>
                </div>
                <div class="about-item">
                    <div class="image">
                        <img src="images/about-icon4.png" alt="">
                    </div>
                    <p>Мы стремимся соответствовать всем лучшим международным стандартам качества для аптек, вводимым в РК. Мы даём возможность
                        обучения и информирования клиентов о новшествах в медицине, фармацевтике и законодательстве Министерства Здравоохранения РК.</p>
                </div>
                <div class="green-title m-lg">
                    <h2 class="green-text">наши история</h2>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="history-item">
                        <img src="images/about1.png" alt="">
                        <p>Открытие первой
                            аптеки в г.Алматы</p>
                    </div>
                    </div>
                    <div class="col">
                        <div class="history-item">
                        <img src="images/about2.png" alt="">
                        <p>Открытие первой
                            в Казахстане аптеки
                            открытого формата</p>
                    </div>
                    </div>
                    <div class="col">
                        <div class="history-item">
                        <img src="images/about3.png" alt="">
                        <p>Открытие клиники вакцинации в
                            г.Алматы</p>
                    </div>
                    </div>
                    <div class="col">
                        <div class="history-item">
                        <img src="images/about4.png" alt="">
                        <p>Открыто 10 аптек</p>
                    </div>
                    </div>
                    <div class="col">
                        <div class="history-item">
                        <img src="images/about5.png" alt="">
                        <p>Сертификат МК СТ
                            РК ИСО 9001-2009</p>
                    </div>
                    </div>
                </div>
                <div class="green-title  m-lg">
                    <h2>наши услуги</h2>
                </div>
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="service-item">
                                    <img src="images/service-icon1.png" alt="">
                                    <p>Присутствие во всех
                                        городах РК</p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="service-item">
                                    <img src="images/service-icon2.png" alt="">
                                    <p>Комфортный график
                                        работы аптек
                                        (включая круглосуточные)</p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="service-item">
                                    <img src="images/service-icon3.png" alt="">
                                    <p>Все форматы торговли
                                        (от аптечного
                                        киоска до фарм. маркета)</p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="service-item">
                                    <img src="images/service-icon4.png" alt="">
                                    <p>Широкий ассортимент
                                        товаров</p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="service-item">
                                    <img src="images/service-icon5.png" alt="">
                                    <p>Профессиональная
                                        консультация
                                        провизоров</p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="service-item">
                                    <img src="images/service-icon6.png" alt="">
                                    <p>Выгодные цены
                                        и спец. предложения</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>