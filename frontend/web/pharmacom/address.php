<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li>/</li>
                    <li>Адреса аптек</li>
                </ul>
            </div>
            <div class="main-title text-left">
                <h2>Адреса аптек</h2>
            </div>
            <div class="bg-white ">
                <div class="pick-city">
                    <p>Ваш город:</p>
                    <select name="" id="">
                        <option value="">Алматы</option>
                        <option value="">Алматы2</option>
                        <option value="">Алматы3</option>
                    </select>
                </div>
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27650.53961549817!2d76.90095778794546!3d43.242934053436734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1561008870511!5m2!1sru!2skz" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <div class="map-search">
                        <input type="text">
                        <button><img src="images/search-ico.png" alt="">Найти</button>
                    </div>
                </div>
                <div class="row">
                   <div class="col-sm-4">
                       <div class="drugstore-item">
                           <p class="green-text">Толе Би, 201/уг. ул. Розыбакиева</p>
                           <p>тел.: +7 (727) 374 80 81</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Гоголя, 140/уг. Муратбаева</p>
                           <p>тел.: +7 (727) 220 95 17</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Тимирязева, 34/уг. ул. Шашкина</p>
                           <p>тел.: +7 (727) 387 21 15</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Жибек Жолы, 81а/уг. ул. Панфилова</p>
                           <p>тел.: +7 (727) 273 83 48</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                   </div>
                   <div class="col-sm-4">
                       <div class="drugstore-item">
                           <p class="green-text">Толе Би, 201/уг. ул. Розыбакиева</p>
                           <p>тел.: +7 (727) 374 80 81</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Гоголя, 140/уг. Муратбаева</p>
                           <p>тел.: +7 (727) 220 95 17</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Тимирязева, 34/уг. ул. Шашкина</p>
                           <p>тел.: +7 (727) 387 21 15</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Жибек Жолы, 81а/уг. ул. Панфилова</p>
                           <p>тел.: +7 (727) 273 83 48</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                   </div>
                   <div class="col-sm-4">
                       <div class="drugstore-item">
                           <p class="green-text">Толе Би, 201/уг. ул. Розыбакиева</p>
                           <p>тел.: +7 (727) 374 80 81</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Гоголя, 140/уг. Муратбаева</p>
                           <p>тел.: +7 (727) 220 95 17</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Тимирязева, 34/уг. ул. Шашкина</p>
                           <p>тел.: +7 (727) 387 21 15</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                       <div class="drugstore-item">
                           <p class="green-text">Жибек Жолы, 81а/уг. ул. Панфилова</p>
                           <p>тел.: +7 (727) 273 83 48</p>
                           <p>Круглосуточно, без выходных</p>
                       </div>
                   </div>
                </div>
                <div class="pagination">
                    <ul>
                        <li><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">5</a></li>
                        <li><a href="">6</a></li>
                        <li><a href="">7</a></li>
                        <li><a href="">8</a></li>
                        <li><a href="">9</a></li>
                        <li><a href="">»</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>