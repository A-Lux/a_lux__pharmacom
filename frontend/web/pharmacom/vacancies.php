<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li>/</li>
                    <li>Вакансии  </li>
                </ul>
            </div>
            <div class="main-title text-left">
                <h2>Вакансии</h2>
            </div>
            <div class="bg-white jobs">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="job-card">
                            <p class="job-card-title">Вакансия</p>
                            <p class="job-card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <ul>
                                <li class="salary"><span>200 000 тг / За месяц</span></li>
                                <li class="time"><span>Полная занятось</span></li>
                                <li class="location"><span>Торайгырова 33, Бостандыкский район</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="job-card">
                            <p class="job-card-title">Вакансия</p>
                            <p class="job-card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <ul>
                                <li class="salary"><span>200 000 тг / За месяц</span></li>
                                <li class="time"><span>Полная занятось</span></li>
                                <li class="location"><span>Торайгырова 33, Бостандыкский район</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="job-card">
                            <p class="job-card-title">Вакансия</p>
                            <p class="job-card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <ul>
                                <li class="salary"><span>200 000 тг / За месяц</span></li>
                                <li class="time"><span>Полная занятось</span></li>
                                <li class="location"><span>Торайгырова 33, Бостандыкский район</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>