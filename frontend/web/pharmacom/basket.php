<?php require_once 'header.php'?>
    <div class="desktop-version">
        <div class="main">
            <div class="container">
                <div class="bread-crumbs">
                    <ul>
                        <li><a href="">Главная</a></li>
                        <li>/</li>
                        <li>Корзина</li>
                    </ul>
                </div>
                <div class="main-title text-left">
                    <h2>корзина</h2>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>

                                </th>
                                <th>Количество</th>
                                <th>Стоимость</th>
                                <th>Итог</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="product-item">
                                        <div class="image">
                                            <img src="images/basket-img.jpg" alt="">
                                        </div>
                                        <div class="text-block">
                                            <h6>Череды трава 50 гр, фито чай</h6>
                                            <p> Страна: Казахстан <br>
                                                Производитель: PLANTA ТОО</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="count-input">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation2">-</span>
                                        <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation2">+</span>
                                    </div>
                                </td>
                                <td>103 тг</td>
                                <td>103 тг</td>
                                <td>
                                    <div class="delete-icon">
                                        <a href=""><img src="images/delete-ico.png" alt=""></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="product-item">
                                        <div class="image">
                                            <img src="images/basket-img.jpg" alt="">
                                        </div>
                                        <div class="text-block">
                                            <h6>Череды трава 50 гр, фито чай</h6>
                                            <p> Страна: Казахстан <br>
                                                Производитель: PLANTA ТОО</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="count-input">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation2">-</span>
                                        <input class="quantity input-text" min="1" name="count" value="1" type="number">
                                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation2">+</span>
                                    </div>
                                </td>
                                <td>103 тг</td>
                                <td>103 тг</td>
                                <td>
                                    <div class="delete-icon">
                                        <a href=""><img src="images/delete-ico.png" alt=""></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear-basket">
                    <a href=""><img src="images/delete-ico.png" alt="">Очистить корзину</a>
                </div>
                <div class="total-cost">
                    <p>Всего к оплате: <span>1 250 тг</span></p>
                    <div class="order-btn">
                        <a href="">Оформить заказ</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="mobile-version">


    </div>
    </div>
<?php require_once 'footer.php'?>