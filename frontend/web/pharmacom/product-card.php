<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bread-crumbs">
                        <ul>
                            <li><a href="">Главная</a></li>
                            <li>/</li>
                            <li><a href="">Каталог</a></li>
                            <li>/</li>
                            <li><a href="">Витамины и бады </a></li>
                            <li>/</li>
                            <li>Линекс для детей капли 8 мл</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="product-img">
                        <img src="images/product-img.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="product-description">
                        <ul>
                            <li><p>Наличие</p><span>Есть в наличии</span></li>
                            <li><p>Модель</p><span>3838957023066</span></li>
                            <li><p>Страна</p><span>Словения</span></li>
                            <li><p>Производитель</p><span>Сандоз Фармасьютикалс д.д.</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="inner-title">
                        <h3>
                            Линекс для детей капли 8 мл
                        </h3>
                    </div>
                    <div class="rating">
                        <img src="images/raiting.png" alt="">
                    </div>
                    <div class="product-price">
                        <p>
                            2 976 ₸
                        </p>
                    </div>
                    <div class="count-input">
                        <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation">-</span>
                        <input class="quantity input-text" min="1" name="count" value="1" type="number">
                        <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation">+</span>
                    </div>
                    <div class="button-row">
                        <div class="basket-dark">
                            <button><img src="images/basket-ico-light.png" alt="">Купить</button>
                        </div>
                        <button>В избранное</button>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="main-title text-left">
                        <h2>Сопутствующие товары</h2>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="catalog-item">
                        <div class="add-to-fav">
                            <a href=""><img src="images/star-ico.png" alt=""></a>
                        </div>
                        <div class="image">
                            <img src="images/catalog-img.jpg" alt="">
                        </div>
                        <div class="name">
                            <p>Горный кальций D3 с мумие
                                №80 таблетки
                                <span> Производитель: Эвалар (Россия)</span></p>
                        </div>
                        <div class="price">
                            <p>5 121 ₸</p>
                        </div>
                        <div class="basket">
                            <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="catalog-item">
                        <div class="promo">
                            <a href=""><img src="images/promo.png" alt=""></a>
                        </div>
                        <div class="add-to-fav">
                            <a href=""><img src="images/star-ico.png" alt=""></a>
                        </div>
                        <div class="image">
                            <img src="images/catalog-img.jpg" alt="">
                        </div>
                        <div class="name">
                            <p>Горный кальций D3 с мумие
                                №80 таблетки
                                <span> Производитель: Эвалар (Россия)</span></p>
                        </div>
                        <div class="price">
                            <p>3 121 ₸ <span>3485 ₸</span></p>
                        </div>
                        <div class="basket">
                            <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="catalog-item">
                        <div class="add-to-fav">
                            <a href=""><img src="images/star-ico.png" alt=""></a>
                        </div>
                        <div class="image">
                            <img src="images/catalog-img.jpg" alt="">
                        </div>
                        <div class="name">
                            <p>Горный кальций D3 с мумие
                                №80 таблетки
                                <span> Производитель: Эвалар (Россия)</span></p>
                        </div>
                        <div class="price">
                            <p>5 121 ₸</p>
                        </div>
                        <div class="basket">
                            <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="catalog-item">
                        <div class="add-to-fav">
                            <a href=""><img src="images/star-ico.png" alt=""></a>
                        </div>
                        <div class="image">
                            <img src="images/catalog-img.jpg" alt="">
                        </div>
                        <div class="name">
                            <p>Горный кальций D3 с мумие
                                №80 таблетки
                                <span> Производитель: Эвалар (Россия)</span></p>
                        </div>
                        <div class="price">
                            <p>5 121 ₸</p>
                        </div>
                        <div class="basket">
                            <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>