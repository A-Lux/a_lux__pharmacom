<?php require_once 'header.php'?>
    <div class="desktop-version">
       <div class="main">
           <div class="container">
               <div class="row">
                   <div class="col-sm-12">
                       <div class="main-img">
                       </div>
                       <div class="category-hover-block">
                           <div class="category-hover-item">
                               <a href="">
                                   <div class="category-hover-img">
                                       <img src="images/hover1.png" alt="">
                                   </div>
                                   <p>Лекарственные <br>
                                       средства</p>
                               </a>
                           </div>
                           <div class="category-hover-item">
                               <a href="">
                                   <div class="category-hover-img">
                                       <img src="images/hover2.png" alt="">
                                   </div>
                                   <p>Витамины <br>
                                       и бады</p>
                               </a>
                           </div>
                           <div class="category-hover-item">
                               <a href="">
                                   <div class="category-hover-img">
                                       <img src="images/hover3.png" alt="">
                                   </div>
                                   <p>Изделия <br>
                                       мед. назначения</p>
                               </a>
                           </div>
                           <div class="category-hover-item">
                               <a href="">
                                   <div class="category-hover-img">
                                       <img src="images/hover4.png" alt="">
                                   </div>
                                   <p>Мать и дитя</p>
                               </a>
                           </div>
                           <div class="category-hover-item">
                               <a href="">
                                   <div class="category-hover-img">
                                       <img src="images/hover5.png" alt="">
                                   </div>
                                   <p>Красота <br>
                                       и гигиена</p>
                               </a>
                           </div>
                       </div>
                       <div class="main-title">
                           <div class="sep"></div>
                           <h2>каталог</h2>
                       </div>
                   </div>
                   <div class="offset-lg-2 col-lg-8 col-sm-12">
                       <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                               <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Популярные</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Новинки</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Акции и скидки</a>
                           </li>
                       </ul>
                   </div>
                   <div class="col-sm-12">
                       <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                               <div class="row">
                                   <div id="carousel1" class="carousel slide" data-ride="carousel">
                                       <div class="carousel-inner">
                                           <div class="carousel-item active">
                                               <div class="row">
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="promo">
                                                               <a href=""><img src="images/promo.png" alt=""></a>
                                                           </div>
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>3 121 ₸ <span>3485 ₸</span></p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>1 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="promo">
                                                               <a href=""><img src="images/promo.png" alt=""></a>
                                                           </div>
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>3 121 ₸ <span>3485 ₸</span></p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>1 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="carousel-item">
                                               <div class="row">
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="promo">
                                                               <a href=""><img src="images/promo.png" alt=""></a>
                                                           </div>
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>3 121 ₸ <span>3485 ₸</span></p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>1 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="promo">
                                                               <a href=""><img src="images/promo.png" alt=""></a>
                                                           </div>
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>3 121 ₸ <span>3485 ₸</span></p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>1 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="carousel-item">
                                               <div class="row">
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="promo">
                                                               <a href=""><img src="images/promo.png" alt=""></a>
                                                           </div>
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>3 121 ₸ <span>3485 ₸</span></p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>1 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="promo">
                                                               <a href=""><img src="images/promo.png" alt=""></a>
                                                           </div>
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>3 121 ₸ <span>3485 ₸</span></p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>1 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="col-lg-3">
                                                       <div class="catalog-item">
                                                           <div class="add-to-fav">
                                                               <a href=""><img src="images/star-ico.png" alt=""></a>
                                                           </div>
                                                           <div class="image">
                                                               <img src="images/catalog-img.jpg" alt="">
                                                           </div>
                                                           <div class="name">
                                                               <p>Горный кальций D3 с мумие
                                                                   №80 таблетки
                                                                   <span> Производитель: Эвалар (Россия)</span></p>
                                                           </div>
                                                           <div class="price">
                                                               <p>5 121 ₸</p>
                                                           </div>
                                                           <div class="basket">
                                                               <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <ol class="carousel-indicators">
                                           <li data-target="#carousel1" data-slide-to="0" class="active"></li>
                                           <li data-target="#carousel1" data-slide-to="1"></li>
                                           <li data-target="#carousel1" data-slide-to="2"></li>
                                       </ol>
                                   </div>
                               </div>
                           </div>
                           <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                               <div id="carouse2" class="carousel slide" data-ride="carousel">
                                   <div class="carousel-inner">
                                       <div class="carousel-item active">
                                           <div class="row">
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="carousel-item">
                                           <div class="row">
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="carousel-item">
                                           <div class="row">
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <ol class="carousel-indicators">
                                       <li data-target="#carouse2" data-slide-to="0" class="active"></li>
                                       <li data-target="#carouse2" data-slide-to="1"></li>
                                       <li data-target="#carouse2" data-slide-to="2"></li>
                                   </ol>
                               </div>
                           </div>
                           <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                               <div id="carouse3" class="carousel slide" data-ride="carousel">
                                   <div class="carousel-inner">
                                       <div class="carousel-item active">
                                           <div class="row">
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="carousel-item">
                                           <div class="row">
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="carousel-item">
                                           <div class="row">
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="promo">
                                                           <a href=""><img src="images/promo.png" alt=""></a>
                                                       </div>
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>3 121 ₸ <span>3485 ₸</span></p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>1 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-lg-3">
                                                   <div class="catalog-item">
                                                       <div class="add-to-fav">
                                                           <a href=""><img src="images/star-ico.png" alt=""></a>
                                                       </div>
                                                       <div class="image">
                                                           <img src="images/catalog-img.jpg" alt="">
                                                       </div>
                                                       <div class="name">
                                                           <p>Горный кальций D3 с мумие
                                                               №80 таблетки
                                                               <span> Производитель: Эвалар (Россия)</span></p>
                                                       </div>
                                                       <div class="price">
                                                           <p>5 121 ₸</p>
                                                       </div>
                                                       <div class="basket">
                                                           <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <ol class="carousel-indicators">
                                       <li data-target="#carouse3" data-slide-to="0" class="active"></li>
                                       <li data-target="#carouse3" data-slide-to="1"></li>
                                       <li data-target="#carouse3" data-slide-to="2"></li>
                                   </ol>
                               </div>
                           </div>
                       </div>
                       <div class="show-all">
                           <a href="">посмотреть все</a>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div class="pills-bg">
           <div class="container">
               <form action="">
                   <h2>Подпишитесь на рассылку и получайте скидки!</h2>
                   <input type="text" placeholder="Ваш E-mail">
                   <div class="send">
                       <a href="">Отправить</a>
                   </div>

               </form>
           </div>
       </div>
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
                   <div class="main-title">
                       <div class="sep"></div>
                       <h2>Наши аптеки</h2>
                   </div>
                   <div class="map">
                       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27650.53961549817!2d76.90095778794546!3d43.242934053436734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1561008870511!5m2!1sru!2skz" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                       <div class="map-search">
                           <input type="text">
                           <button><img src="images/search-ico.png" alt="">Найти</button>
                       </div>
                   </div>
                   <div class="main-title">
                       <div class="sep"></div>
                       <h2>О компании</h2>
                   </div>
               </div>
               <div class="col-sm-6">
                   <div class="about-text">
                       <p>Мы стараемся предложить клиентам высоко профессиональный сервис, через консультации сертифицированных провизоров/фармацевтов, а также врачей-консультантов, как индивидуальную услугу для каждого клиента с мини диагностикой и рекомендациями. Мы имеем в наличие широкий ассортимент лекарственных препаратов (отечественного и западного пр-ва), а также... </p>
                       <div class="more-btn">
                           <a href="">Подробнее</a>
                       </div>
                   </div>
               </div>
               <div class="col-sm-6">
                   <img src="images/about-img.jpg" class="img-fluid" alt="">
               </div>
           </div>
       </div>
   </div>
    <div class="mobile-version">
        <img src="images/banner.jpg" class="mobile-main" alt="">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="category-hover-item">
                    <a href="">
                        <div class="category-hover-img">
                            <img src="images/hover1.png" alt="">
                        </div>
                        <p>Лекарственные <br>
                            средства</p>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="category-hover-item">
                    <a href="">
                        <div class="category-hover-img">
                            <img src="images/hover2.png" alt="">
                        </div>
                        <p>Витамины <br>
                            и бады</p>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="category-hover-item">
                    <a href="">
                        <div class="category-hover-img">
                            <img src="images/hover3.png" alt="">
                        </div>
                        <p>Изделия <br>
                            мед. назначения</p>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="category-hover-item">
                    <a href="">
                        <div class="category-hover-img">
                            <img src="images/hover4.png" alt="">
                        </div>
                        <p>Мать и дитя</p>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="category-hover-item">
                    <a href="">
                        <div class="category-hover-img">
                            <img src="images/hover5.png" alt="">
                        </div>
                        <p>Красота <br>
                            и гигиена</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="main-title">
            <h2>Популярные</h2>
        </div>
        <div class="square-flex">
            <div class="catalog-item">
                <div class="add-to-fav">
                    <a href=""><img src="images/star-ico.png" alt=""></a>
                </div>
                <div class="image">
                    <img src="images/catalog-img2.jpg" alt="">
                </div>
                <div class="name">
                    <p>Горный кальций D3 с мумие
                        №80 таблетки
                        <span> Производитель: Эвалар (Россия)</span></p>
                </div>
                <div class="price">
                    <p>5 121 ₸</p>
                </div>
                <div class="basket">
                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                </div>
            </div>
            <div class="catalog-item">
                <div class="promo">
                    <a href=""><img src="images/promo.png" alt=""></a>
                </div>
                <div class="add-to-fav">
                    <a href=""><img src="images/star-ico.png" alt=""></a>
                </div>
                <div class="image">
                    <img src="images/catalog-img3.jpg" alt="">
                </div>
                <div class="name">
                    <p>Горный кальций D3 с мумие
                        №80 таблетки
                        <span> Производитель: Эвалар (Россия)</span></p>
                </div>
                <div class="price">
                    <p>3 121 ₸ <span>3485 ₸</span></p>
                </div>
                <div class="basket">
                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                </div>
            </div>
            <div class="catalog-item">
                <div class="add-to-fav">
                    <a href=""><img src="images/star-ico.png" alt=""></a>
                </div>
                <div class="image">
                    <img src="images/catalog-img.jpg" alt="">
                </div>
                <div class="name">
                    <p>Горный кальций D3 с мумие
                        №80 таблетки
                        <span> Производитель: Эвалар (Россия)</span></p>
                </div>
                <div class="price">
                    <p>5 121 ₸</p>
                </div>
                <div class="basket">
                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                </div>
            </div>
            <div class="catalog-item">
                <div class="promo">
                    <a href=""><img src="images/promo.png" alt=""></a>
                </div>
                <div class="add-to-fav">
                    <a href=""><img src="images/star-ico.png" alt=""></a>
                </div>
                <div class="image">
                    <img src="images/catalog-img.jpg" alt="">
                </div>
                <div class="name">
                    <p>Горный кальций D3 с мумие
                        №80 таблетки
                        <span> Производитель: Эвалар (Россия)</span></p>
                </div>
                <div class="price">
                    <p>3 121 ₸ <span>3485 ₸</span></p>
                </div>
                <div class="basket">
                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                </div>
            </div>
        </div>
        <div class="see-all">
            <a href="">Посмотреть все</a>
        </div>
        <div class="main-title">
            <h2>Новинки</h2>
        </div>
        <div class="new-item">
            <div class="image">
                <img src="images/catalog-img4.jpg" alt="">
            </div>
            <div class="text-block">
                <div class="name">
                    <p>Vichy флюид для нормальной и комбинированной кожи тон 25 Идеа...</p>
                </div>
                <div class="new-item-row">
                    <div class="price">
                        <p>5 121 ₸</p>
                    </div>
                    <div class="basket">
                        <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="new-item">
            <div class="image">
                <img src="images/catalog-img2.jpg" alt="">
            </div>
            <div class="text-block">
                <div class="name">
                    <p>Huggies Подгузники UC Conv 4+ (10-16кг) Boy 17*8</p>
                </div>
                <div class="new-item-row">
                    <div class="price">
                        <p>2 121 ₸</p>
                    </div>
                    <div class="basket">
                        <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="new-item">
            <div class="image">
                <img src="images/catalog-img.jpg" alt="">
            </div>
            <div class="text-block">
                <div class="name">
                    <p>Vichy флюид для нормальной и комбинированной кожи тон 25 Идеа...</p>
                </div>
                <div class="new-item-row">
                    <div class="price">
                        <p>5 121 ₸</p>
                    </div>
                    <div class="basket">
                        <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="new-item">
            <div class="image">
                <img src="images/catalog-img4.jpg" alt="">
            </div>
            <div class="text-block">
                <div class="name">
                    <p>Vichy флюид для нормальной и комбинированной кожи тон 25 Идеа...</p>
                </div>
                <div class="new-item-row">
                    <div class="price">
                        <p>5 121 ₸</p>
                    </div>
                    <div class="basket">
                        <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="see-all">
            <a href="">Посмотреть все</a>
        </div>
        <div class="about-text">
            <p>Мы стараемся предложить клиентам высоко профессиональный сервис, через консультации сертифицированных провизоров/фармацевтов, а также врачей-консультантов, как индивидуальную услугу для каждого клиента с мини диагностикой и рекомендациями. Мы имеем в наличие широкий ассортимент лекарственных препаратов (отечественного и западного пр-ва), а также большой выбор сопутствующих товаров, медицинского назначения, детских товаров и лечебной косметики. </p>
            <p id="demo" class="collapse">Наши цены прозрачны и выгодны для всех слоёв населения (с обязательным наличием ЖВЛС). Мы стремимся соответствовать всем лучшим международным стандартам качества для аптек, вводимым в РК. Мы даём возможность обучения и информирования клиентов о новшествах в медицине, фармацевтике и законодательстве Министерства Здравоохранения РК.</p>
            <div class="show-more" >
                <img data-toggle="collapse" data-target="#demo" src="images/show-more.png" alt="">
            </div>
        </div>
        <div class="collapse-dark-block" data-toggle="collapse" data-target="#demo2">
            <p>Каталог товаров</p>
            <div class="arrow">
                <img src="images/light-arrow.png" alt="">
            </div>
        </div>
        <div id="demo2" class="collapse">
            <div class="collapse-block-light">
                <a href="" data-toggle="collapse" data-target="#demo3">
                    <p>Лекарственные средства</p>
                    <div class="arrow">
                        <img src="images/dark-arrow.png" alt="">
                    </div>
                </a>
            </div>
            <div id="demo3" class="sublist collapse">
                <ul>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                </ul>
            </div>
            <div class="collapse-block-light">
                <a href="" data-toggle="collapse" data-target="#demo4">
                    <p>Витамины и бады</p>
                    <div class="arrow">
                        <img src="images/dark-arrow.png" alt="">
                    </div>
                </a>
            </div>
            <div id="demo4" class="sublist collapse">
                <ul>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                </ul>
            </div>
            <div class="collapse-block-light">
                <a href="" data-toggle="collapse" data-target="#demo5">
                    <p>Изделия мед. назначения</p>
                    <div class="arrow">
                        <img src="images/dark-arrow.png" alt="">
                    </div>
                </a>
            </div>
            <div id="demo5" class="sublist collapse">
                <ul>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                </ul>
            </div>
            <div class="collapse-block-light">
                <a href="" data-toggle="collapse" data-target="#demo6">
                    <p>Мать и дитя</p>
                    <div class="arrow">
                        <img src="images/dark-arrow.png" alt="">
                    </div>
                </a>
            </div>
            <div id="demo6" class="sublist collapse">
                <ul>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                </ul>
            </div>
            <div class="collapse-block-light">
                <a href="" data-toggle="collapse" data-target="#demo7">
                    <p>Красота и гигиена</p>
                    <div class="arrow">
                        <img src="images/dark-arrow.png" alt="">
                    </div>
                </a>
            </div>
            <div id="demo7" class="sublist collapse">
                <ul>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                    <li><a href="">submenu1</a></li>
                </ul>
            </div>


        </div>
        <div class="main-title">
            <h2>Наши аптеки</h2>
        </div>
        <div class="mobile-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d372175.96387086256!2d76.66398216062525!3d43.217360079519224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1561461991532!5m2!1sru!2skz" width="100%" height="210" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

    </div>
</div>
<?php require_once 'footer.php'?>


