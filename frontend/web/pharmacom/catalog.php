<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bread-crumbs">
                        <ul>
                            <li><a href="">Главная</a></li>
                            <li>/</li>
                            <li><a href="">Каталог</a></li>
                            <li>/</li>
                            <li>Витамины и бады</li>
                        </ul>
                    </div>
                    <div class="bg-white">
                        <div class="inner-title">
                            <h3>
                                Витамины и бады
                            </h3>
                        </div>
                        <div class="catalog-list">
                            <div class="col-sm-6 col-lg-3">
                                <div class="list-item">
                                    <a><p>Бады</p></a>
                                </div>
                                <div class="list-item">
                                    <a><p>Бады</p></a>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="list-item">
                                    <a><p>Бады</p></a>
                                </div>
                                <div class="list-item">
                                    <a><p>Бады</p></a>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="list-item">
                                    <a><p>Бады</p></a>
                                </div>
                                <div class="list-item">
                                    <a><p>Бады</p></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="bg-white">
                        <div class="box-title">
                            <h6>Цена</h6>
                        </div>
                        <div class="filter-row">
                            <p>От</p>
                            <input type="text">
                            <p>тг</p>
                        </div>
                        <div class="filter-row">
                            <p>До</p>
                            <input type="text">
                            <p>тг</p>
                        </div>
                    </div>
                    <div class="bg-white">
                        <div class="box-title">
                            <h6>Фильтры</h6>
                        </div>
                        <div class="checkbox-list">
                            <ul>
                                <li>
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="checkbox-button__input" id="choice1-1" name="choice1">
                                        <span class="checkbox-button__control"></span>
                                        <span class="checkbox-button__label"> Сначала новые</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="checkbox-button__input" id="choice1-1" name="choice1">
                                        <span class="checkbox-button__control"></span>
                                        <span class="checkbox-button__label">Есть в наличии</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="checkbox-button__input" id="choice1-1" name="choice1">
                                        <span class="checkbox-button__control"></span>
                                        <span class="checkbox-button__label">Акционные товары</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="checkbox-button__input" id="choice1-1" name="choice1">
                                        <span class="checkbox-button__control"></span>
                                        <span class="checkbox-button__label">Популярные</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-button">
                                        <input type="checkbox" class="checkbox-button__input" id="choice1-1" name="choice1">
                                        <span class="checkbox-button__control"></span>
                                        <span class="checkbox-button__label"> Товар месяца</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="bg-white">
                        <div class="gift">
                            <img src="images/gift.png" alt="">
                            <p>Получайте бонусы за каждую покупку и в любой момент
                                обменивайте их на подарки!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="promo">
                                    <a href=""><img src="images/promo.png" alt=""></a>
                                </div>
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>3 121 ₸ <span>3485 ₸</span></p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="promo">
                                    <a href=""><img src="images/promo.png" alt=""></a>
                                </div>
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>3 121 ₸ <span>3485 ₸</span></p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="promo">
                                    <a href=""><img src="images/promo.png" alt=""></a>
                                </div>
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>3 121 ₸ <span>3485 ₸</span></p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item">
                                <div class="add-to-fav">
                                    <a href=""><img src="images/star-ico.png" alt=""></a>
                                </div>
                                <div class="image">
                                    <img src="images/catalog-img.jpg" alt="">
                                </div>
                                <div class="name">
                                    <p>Горный кальций D3 с мумие
                                        №80 таблетки
                                        <span> Производитель: Эвалар (Россия)</span></p>
                                </div>
                                <div class="price">
                                    <p>5 121 ₸</p>
                                </div>
                                <div class="basket">
                                    <a href=""><img src="images/basket-ico-light.png" alt="">Купить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagination">
                        <ul>
                            <li><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">»</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>
