<?php require_once 'header.php'?>
    <div class="main">
        <div class="container">
            <div class="bread-crumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li>/</li>
                    <li>Отзывы</li>
                </ul>
            </div>
            <div class="main-title text-left">
                <h2>Отзывы</h2>
            </div>
            <div class="bg-white ">
               <div class="review-item">
                   <div class="date">
                       <p>10 мар., 15:30</p>
                   </div>
                   <div class="name">
                       <p>Талгат Исабаев</p>
                   </div>
                   <div class="rating">
                       <img src="images/raiting.png" alt="">
                   </div>
                   <div class="text">
                       <p>Пользуюсь сервисом более 2 лет. Цена всегда выгодная, доставка быстрая, описание четкое.</p>
                   </div>
               </div>
                <div class="review-item">
                    <div class="date">
                        <p>10 мар., 15:30</p>
                    </div>
                    <div class="name">
                        <p>Бурыкина Светлана</p>
                    </div>
                    <div class="rating">
                        <img src="images/raiting.png" alt="">
                    </div>
                    <div class="text">
                        <p>Каждый товар описан, и вы можете сравнить цены, состав, действие, не выходя из дома, и заказать то, что вам нравится больше.</p>
                    </div>
                </div>
                <div class="review-item">
                    <div class="date">
                        <p>10 мар., 15:30</p>
                    </div>
                    <div class="name">
                        <p>Айжан Маратова</p>
                    </div>
                    <div class="rating">
                        <img src="images/raiting.png" alt="">
                    </div>
                    <div class="text">
                        <p>
                            *Заказ обрабатывается быстро <br>
                            *Оператор звонит для уточнения заказа, вежливо общается с покупателем <br>
                            *Быстрая доставка заказа в указанный аптечный пункт <br>
                            *Достаточно обширная сеть пунктов выдачи товаров <br>
                            *Характеристики полученного мною товара совпадают с указанными на сайте.
                        </p>
                    </div>
                </div>
                <div class="pagination">
                    <ul>
                        <li><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">»</a></li>
                    </ul>
                </div>
                <div class="comment-title">
                    <h5>Оставить комментарий на сайте</h5>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="comment-input">
                            <label for="">Имя</label>
                            <input type="text">
                        </div>
                        <div class="comment-input">
                            <label for="">Телефон</label>
                            <input type="text">
                        </div>
                        <div class="comment-input">
                            <label for="">Почта</label>
                            <input type="text">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="comment-input">
                            <label for="">Ваш комментарий</label>
                            <textarea name="" id="" cols="30" rows="10" placeholder="Введите текст сообщения"></textarea>
                        </div>
                        <div class="send-btn">
                            <a href="">Отправить</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="comment-input">
                            <label for="">Поставьте свою оценку</label>
                        </div>
                        <div class="rating">
                            <img src="images/raiting.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php require_once 'footer.php'?>