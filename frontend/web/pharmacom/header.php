<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pharmacom</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/hamburgers.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/hc-offcanvas-nav.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" >
</head>
<body>
<div class="content">
    <div class="desktop-version">
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="logo">
                            <a href="index.php"><img src="images/logo.png" class="img-fluid" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <div class="work-time">
                            <p>График работы</p>
                            <a href="">с 8:00 до 24:00</a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <div class="work-time">
                            <p>Ваш город</p>
                            <select name="" id="">
                                <option value=""> Алматы</option>
                                <option value=""> Алматы2</option>
                                <option value=""> Алматы3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <div class="work-time">
                            <p>Заказать звонок</p>
                            <a href="tel:+7(495)7872450">+7 (495) 787- 2450</a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-8">
                        <div class="icon-row">
                            <a href="" class="phone-icon"><img src="images/phone-ico.png" alt=""></a>
                            <a href="" class="fav-icon"><img src="images/star-ico.png" alt=""></a>
                            <a href="" class="basket-icon"><img src="images/basket-ico.png" alt=""><span>5</span></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4 space-top-sm">
                        <div class="menu">
                            <div class="hamburger hamburger--spring">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                            <p class="close-menu">закрыть</p>
                            <p class="open-menu">меню</p>
                        </div>
                        <div class="menu-level1">
                            <ul>
                                <li>
                                    <a href="about-us.php">
                                   <span class="icon">
                                       <img src="images/menu-ico1.png" alt="">
                                   </span>
                                        <p>О компании</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="catalog-toggle">
                                   <span class="icon">
                                       <img src="images/menu-ico2.png" alt="">
                                   </span>
                                        <p>Каталог <span>+</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="contacts.php">
                                   <span class="icon">
                                       <img src="images/menu-ico3.png" alt="">
                                   </span>
                                        <p>Контакты</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                   <span class="icon">
                                       <img src="images/menu-ico4.png" alt="">
                                   </span>
                                        <p>Доставка</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                   <span class="icon">
                                       <img src="images/menu-ico5.png" alt="">
                                   </span>
                                        <p>Оплата</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="faq.php">
                                   <span class="icon">
                                       <img src="images/menu-ico6.png" alt="">
                                   </span>
                                        <p>Часто задаваемые вопросы</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="address.php">
                                   <span class="icon">
                                       <img src="images/menu-ico7.png" alt="">
                                   </span>
                                        <p>Адреса аптек</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="vacancies.php">
                                   <span class="icon">
                                       <img src="images/menu-ico8.png" alt="">
                                   </span>
                                        <p>Вакансии</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="reviews.php">
                                   <span class="icon">
                                       <img src="images/menu-ico9.png" alt="">
                                   </span>
                                        <p>Отзывы</p>
                                    </a>
                                </li>
                            </ul>
                            <div class="socials">
                                <ul>
                                    <li><a href=""><i class="fab fa-instagram"></i></a></li>
                                    <li><a href=""><i class="fab fa-vk"></i></a></li>
                                    <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="hidden-menu">
                        <div class="menu-level2 ">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul>
                                        <li><a href="">Антибиотики</a></li>
                                        <li><a href="">Антисептики</a></li>
                                        <li><a href="">Гинекологические препараты</a></li>
                                        <li><a href="">Гормональные препараты</a></li>
                                        <li><a href="">Для повышения иммунитета</a></li>
                                        <li><a href="">Жаропонижающие</a></li>
                                        <li><a href="">Женские препараты</a></li>
                                        <li><a href="">Инъекционные растворы</a></li>
                                        <li><a href="">Кожные заболевания</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>
                                        <li><a href="">Лечение алкоголизма, наркомании, табакокурения</a></li>
                                        <li><a href="">Лечение артериального давления</a></li>
                                        <li><a href="">Лечение геморроя</a></li>
                                        <li><a href="">Лечение глаз</a></li>
                                        <li><a href="">Лечение головного кровооброщения</a></li>
                                        <li><a href="">Лечение десен</a></li>
                                        <li><a href="">Лечение жкт</a></li>
                                        <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        <li><a href="">Лечение заболеваний опорно-двигательного аппарата</a></li>
                                        <li><a href="">Лечение лор заболевания</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>
                                        <li><a href="">Лечение неврологических нарушений</a></li>
                                        <li><a href="">Лечение печени</a></li>
                                        <li><a href="">Лечение почек</a></li>
                                        <li><a href="">Лечение ран и ожогов</a></li>
                                        <li><a href="">Лечение сахарного диабета</a></li>
                                        <li><a href="">Лечение сердечно-сосудистых заболеваний</a></li>
                                        <li><a href="">Мужские препараты</a></li>
                                        <li><a href="">Обезболивающие средства</a></li>
                                        <li><a href="">От паразитов</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>
                                        <li><a href="">Лечение алкоголизма, наркомании, табакокурения</a></li>
                                        <li><a href="">Лечение артериального давления</a></li>
                                        <li><a href="">Лечение геморроя</a></li>
                                        <li><a href="">Лечение глаз</a></li>
                                        <li><a href="">Лечение головного кровооброщения</a></li>
                                        <li><a href="">Лечение десен</a></li>
                                        <li><a href="">Лечение жкт</a></li>
                                        <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                        <li><a href="">Лечение заболеваний опорно-двигательного аппарата</a></li>
                                        <li><a href="">Лечение лор заболевания</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>
                                        <li><a href="">Лечение неврологических нарушений</a></li>
                                        <li><a href="">Лечение печени</a></li>
                                        <li><a href="">Лечение почек</a></li>
                                        <li><a href="">Лечение ран и ожогов</a></li>
                                        <li><a href="">Лечение сахарного диабета</a></li>
                                        <li><a href="">Лечение сердечно-сосудистых заболеваний</a></li>
                                        <li><a href="">Мужские препараты</a></li>
                                        <li><a href="">Обезболивающие средства</a></li>
                                        <li><a href="">От паразитов</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8 space-top-sm">
                        <div class="search">
                            <input type="text" placeholder="Введите название лекарства/препарата">
                            <button><img src="images/search-ico.png" alt="">Найти</button>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-4 space-top-sm">
                        <div class="auth">
                            <a href="">Вход/</a>
                        </div>
                        <div class="register">
                            <a href="">Регистрация</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="mobile-version">
        <div class="mobile-header">
            <div class="container">
                <div class="mobile-row">
                    <nav id="main-nav">
                        <ul>
                            <li>
                                <div class="menu-logo">
                                    <img src="images/logo-menu.png" class="img-fluid" alt="">
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="images/mobile-menu.png" alt="">Каталог</a>
                                <ul>
                                    <li><a href="">catalog1</a></li>
                                    <li><a href="">catalog1</a></li>
                                    <li><a href="">catalog1</a></li>
                                    <li><a href="">catalog1</a></li>
                                    <li><a href="">catalog1</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><img src="images/mobile-menu1.png" alt="">О компании</a></li>
                            <li><a href="#"><img src="images/mobile-menu2.png" alt="">Личный кабинет</a></li>
                            <li><a href="#"><img src="images/mobile-menu3.png" alt="">Контакты</a></li>
                            <li><a href="#"><img src="images/mobile-menu4.png" alt="">Доставка</a></li>
                            <li><a href="#"><img src="images/mobile-menu5.png" alt="">Оплата</a></li>
                            <li><a href="#"><img src="images/mobile-menu6.png" alt="">Часто задаваемые вопросы</a></li>
                            <li><a href="#"><img src="images/mobile-menu7.png" alt="">Адреса аптек</a></li>
                        </ul>
                    </nav>
                    <div class="logo">
                        <a href=""><img src="images/logo.png" alt="" class="img-fluid"></a>
                    </div>


                    <div class="head-icon">
                        <a href=""><img src="images/phone-ico.png" alt="" ></a>
                    </div>


                    <div class="head-icon">
                        <a href=""><img src="images/star-ico.png" alt="" ></a>
                    </div>


                    <div class="head-icon icon-row">
                        <a href="" class="basket-icon">
                            <img src="images/basket-ico.png" alt="" >
                            <span>3</span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="mobile-search">
            <div class="container">
                <input type="text" placeholder="Название препарата"><button><img src="images/search-ico.png" alt=""></button>
            </div>
        </div>
    </div>