$('body').on('keyup', '.from_price', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        }
    });
});

$('body').on('keyup', '.to_price', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.available', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.discount', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.isNew', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.isHit', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.top_month', function () {
    showLoader();
    //if($(this).val().length >= 3)
    var available = 0;
    if($('.available').is(':checked'))
        available = 1;
    var discount = 0;
    if($('.discount').is(':checked'))
        discount = 1;
    var isNew = 0;
    if($('.isNew').is(':checked'))
        isNew = 1;
    var isHit = 0;
    if($('.isHit').is(':checked'))
        isHit = 1;
    var top_month = 0;
    if($('.top_month').is(':checked'))
        top_month = 1;
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            available: available, discount: discount, isNew: isNew, isHit: isHit, top_month: top_month},
        success: function(html){
            $('.tablet-version-hide').html(html);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});
