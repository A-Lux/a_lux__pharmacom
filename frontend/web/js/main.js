$(document).ready(function(){
    $('.personal-data ul li').click(function(){
        var tab = $(this).attr('tab');

        $('.personal-data ul li').removeClass('personal-tab-active');
        $(this).addClass('personal-tab-active');

        $('.personal-body').removeClass('personal-active');
        $('#'+tab).addClass('personal-active');

    });

    $('#addAdress').click(function(e){
        e.preventDefault();
        $('.adres-input').append('<input type="text"> </input>');
    });

    $('.btn-update-password').click(function (e) {
        e.preventDefault();
        $('.update-password').show();
    });

    $('.personal-adres p').click(function(){

        // var img = document.createElement('img');
        var i = document.createElement('i');
        var input = document.createElement('input');
        var div = document.createElement('div');
        var attrs = {
            placeholder:'Адрес...',
            type: 'text',
            name: 'UserAddress[address][]',
        }
        for(var j in attrs) {
            input.setAttribute(j,attrs[j]);
        }
        div.style.position = "relative";
        div.classList = "input-box";
        i.classList = "fas fa-minus-circle close-adres";

        $(div).append(i);
        i.addEventListener('click',function(event){
            event.preventDefault();
            div.remove();
        });
        // $(div).append(img);
        $(div).append(input);
        $('.adress').append(div);
        // $('.authorization-loc').append('<form action="" method="post"><i class="fas fa-minus-circle close-adres"></i><img src="images/loc-icon.png" ><input type="text" placeholder="Адрес" ></form>');
    });

    $('.new-adres p').click(function(){
        var img = document.createElement('img');
        var i = document.createElement('i');
        var input = document.createElement('input');
        var div = document.createElement('div');
        var attrs = {
            placeholder:'Адрес...',
            type: 'text',
            name: 'name',
        }
        for(var j in attrs) {
            input.setAttribute(j,attrs[j]);
        }
        div.style.position = "relative";
        i.classList = "fas fa-minus-circle close-adres";
        img.setAttribute('src','/images/loc-icon.png');


        $(div).append(i);
        i.addEventListener('click',function(event){
            event.preventDefault();
            div.remove();
        });
        $(div).append(img);
        $(div).append(input);
        $('.authorization-loc').append(div);
        // $('.authorization-loc').append('<form action="" method="post"><i class="fas fa-minus-circle close-adres"></i><img src="images/loc-icon.png" ><input type="text" placeholder="Адрес" ></form>');
    });




});





(function($) {
    $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        margin:10,
        nav:false,
        dots:true,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
    $('.minus').hide();
    $(".toggle-open").click(function(){
        $(this).find('.plus, .minus').toggle();
    });
    $(".show-more").click(function(){
        $('.show-more img').toggleClass('rotate');
    });
    $(".collapse-dark-block").click(function(){
        $('.collapse-dark-block img').toggleClass('rotate');
    });
    $(".catalog-list .list-item").click(function(e){
        e.preventDefault();
        $(this).find('.fas').toggleClass('rotate2');
    });
    $(".collapse-block-light").click(function(){
        $(this).find('.arrow img').toggleClass('rotate2');
    });
    $('#main-nav').hcOffcanvasNav({
        maxWidth: 980
    });
    $(".menu").click(function(){
        $('.close-menu').toggle();
        $('.open-menu').toggle();
        $('.hamburger').toggleClass('is-active');
        $('.menu-level1').slideToggle();
        $('.hidden-menu').slideUp();
    });
    $(".catalog-toggle").click(function(e){
        e.preventDefault();
        $('.hidden-menu').toggle();
    });

    // question tabs
    $('.wrapper-question-tab').click(function(e){
        e.preventDefault();
        var attr = $(this).children('a').attr('href');
        var children = 	$(this).children('a');
        $(this).children('a').toggleClass('active-btn');
        $(children).children('img').toggleClass('active-img');
        if($(attr).css("display") == "none"){
            $(attr).animate({height: 'show'}, 500);
        }else{
            $(attr).animate({height: 'hide'}, 500);
        }
    });

    $('.personal-data ul li').click(function(){
        var tab = $(this).attr('tab');

        $('.personal-data ul li').removeClass('personal-tab-active');
        $(this).addClass('personal-tab-active');

        $('.personal-body').removeClass('personal-active');
        $('#'+tab).addClass('personal-active');

    });

    $('#addAdress').click(function(e){
        e.preventDefault();
        $('.adres-input').append('<input type="text"> </input>');
    });
    new WOW().init();
})(jQuery);

function showPass() {
    var x = document.getElementById("password-type");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function showPass2() {
    var x = document.getElementById("password-type3");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

$(document).ready(function(){
    $('.personal-data ul li').click(function(){
        var tab = $(this).attr('tab');

        $('.personal-data ul li').removeClass('personal-tab-active');
        $(this).addClass('personal-tab-active');

        $('.personal-body').removeClass('personal-active');
        $('#'+tab).addClass('personal-active');

    });


    $('#sea-pass').click(function(e){
        e.preventDefault();
        var attr = $(this).attr('data-password');
        if(attr == '1'){
            $('#password-type').attr('type','text');
            $('#sea-pass').attr('data-password','0');
            $('#sea-pass').attr('src', '/images/show-icon-2.png');
        }else{
            $('#sea-pass').attr('data-password','1');
            $('#sea-pass').attr('src', '/images/show-icon.png');
            $('#password-type').attr('type','password');
        }
    });



    $('#sea-pass-1').click(function(){
        var attr = $(this).attr('data-password');

        if(attr == '2'){
            $('#password-type2').attr('type','text');
            $('#sea-pass-1').attr('data-password','0');
            $('#sea-pass-1').attr('src', '/images/show-icon-2.png');
        }else{
            $('#sea-pass-1').attr('data-password','2');
            $('#sea-pass-1').attr('src', '/images/show-icon.png');
            $('#password-type2').attr('type','password');
        }
    });
    $('#sea-pass-2').click(function(){
        var attr = $(this).attr('data-password');

        if(attr == '3'){
            $('#password-type3').attr('type','text');
            $('#sea-pass-2').attr('data-password','0');
            $('#sea-pass-2').attr('src', '/images/show-icon-2.png');
        }else{
            $('#sea-pass-2').attr('data-password','3');
            $('#sea-pass-2').attr('src', '/images/show-icon.png');
            $('#password-type3').attr('type','password');
        }
    });



});



$('input[name="Reviews[phone]"]').inputmask("8(999) 999-9999");
$(document).ready(function () {

    $('body').on('click', ".sendReviews", function (e) {
        showLoader();
        $(".raiting input:radio").attr( "checked", false);

        $('.raiting input').click(function () {
            $(".raiting span").removeClass('checked');
            $(this).parent().addClass('checked');
        });
        $.ajax({
            type: "GET",
            url: "/reviews/comment",
            data: $(this).closest('form').serialize(),
            success: function (data) {
                hideLoader();
                if (data == 1) {
                    $("#feedback form")[0].reset();
                    Swal.fire('Отзыв успешно отправлен!', 'Ваш комментарий скоро будет добавлен!', 'success');
                } else {
                    Swal.fire('Ошибка!', data, 'error');
                }
            },
            error: function () {
                hideLoader();
                swal('Error', 'error');
            }
        });

    });


    $(".saveFeedback").click(function () {
        showLoader();
        $.ajax({
            url: "/site/feedback",
            type: "GET",
            data: $(this).closest('form').serialize(),
            success: function(data){
                hideLoader();
                if(data == 1){
                    $("#feedback form")[0].reset();
                    Swal.fire('Заявка успешно отправлена!', 'В ближайшее время мы свяжемся с вами.', 'success');
                }else{
                    Swal.fire('', data, 'error');
                }
            },
            error: function () {
                hideLoader();
                Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    });


});








