$.busyLoadSetup({
    // animation: "slide",
    color: "#57ae80",
    background: "rgba(219, 222, 219 , 0.30)",
});


function showLoader() {
    $("body").css("overflow-y","visible");
    $.busyLoadFull('show');
}


function hideLoader() {
    $.busyLoadFull('hide');
}


$("body").on("click", ".btn-in-basket",function () {

    NProgress.start();
    var id = $(this).attr('data-id');
    var amount = $('#amount').val() == null ? 1 : parseInt($('#amount').val());
    var slider_id = $(this).attr('data-slider-id');

    $.ajax({
        url: "/card/add-product-to-basket",
        type: "GET",
        dataType: "json",
        data: {id:id, amount:amount},
        success: function(data){
            NProgress.done();
            if(data.status){
                $('.count').html(data.count);
            }else{
                swal("", "Извините, у нас нет больше!", "error");
            }
        },
        error: function () {
            NProgress.done();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });



    if (slider_id != null) {
        $.ajax({
            url: "/catalog/offer-related-products",
            type: "GET",
            data: { id: id },
            success: function (data) {
                if (data != 0) {
                    $('#' + slider_id).html(data);
                    $('.owl2').owlCarousel({
                        loop: false,
                        margin: 10,
                        nav: true,
                        responsive: {
                            0: {
                                items: 2
                            },
                            600: {
                                items: 3
                            },
                            1000: {
                                items: 5,
                                nav: true
                            }
                        }
                    });
                    document.getElementById(slider_id).scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' });
                }
            },
            error: function () {
                Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
    let owlItem2 = $('.owl2 > .owl-item');

    if (owlItem2.length < 5) {
        $('.owl-prev').hide();
        $('.owl-next').hide();
    }

});

$(".btn-in-basket-gift").click(function () {
    NProgress.start();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/add-gift-to-basket",
        type: "GET",
        dataType: "json",
        data: {id:id},
        success: function(data){
            if(data.status){
                $('.count').html(data.count);
            }
            NProgress.done();
        },
        error: function () {
            NProgress.done();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});



$(".btn-delete-product-from-basket").click(function () {
    var id = $(this).attr('data-id');
    swal({
        title: "Вы уверены?",
        text: "что хотите удалить этот товар!",
        icon: "warning",
        buttons: ["Отмена", "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/card/delete-product?id="+id;
            } else {

            }
        });
});


$(".btn-delete-all-product-from-basket").click(function () {
    var id = $(this).attr('data-id');
    swal({
        title: "Вы уверены?",
        text: "что хотите удалить весь товар!",
        icon: "warning",
        buttons: ["Отмена", "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/card/delete-all-product";
            } else {

            }
        });
});



$(".btn-delete-product-from-basket-gift").click(function () {
    var id = $(this).attr('data-id');
    swal({
        title: "Вы уверены?",
        text: "что хотите удалить этот товар!",
        icon: "warning",
        buttons: ["Отмена", "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/card/delete-gift-product?id="+id;
            } else {

            }
        });
});

$(".btn-operation-minus").click(function () {
    showLoader();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/down-clicked",
        type: "GET",
        dataType: "json",
        data: {id:id},
        success: function(data){
            $('#countProduct'+id).html(data.countProduct);
            $('#sumBasket').html(data.sum+" ₸");
            $('#sumProduct'+id).html(data.sumProduct+" ₸");
            $('.count').html(data.header_count);
            hideLoader();
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');

        }
    });
});


$(".btn-operation-plus").click(function () {
    showLoader();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/up-clicked",
        type: "GET",
        dataType: "json",
        data: {id:id},
        success: function(data){
            if(data.status){
                $('#countProduct' + id).html(data.countProduct);
                $('#sumBasket').html(data.sum+" ₸");
                $('#sumProduct' + id).html(data.sumProduct+" ₸");
                $('.count').html(data.header_count);
                hideLoader();
            }else{
                hideLoader();
                $("#countProduct" + id).val(data.countProduct);
                Swal.fire('', 'Извините, у нас нет больше!', 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('#bonus').on('change', function(){

    showLoader();
    var bonus = $(this).val();

    if(bonus > -1) {

        $.ajax({
            url: "/card/bonus",
            type: "GET",
            dataType: "json",
            data: {bonus:bonus},
            success: function (data) {
                $('#sumBasket').html(data.sum+" ₸");
                if(data.bonus != 0){
                    $("#bonus").val(data.bonus);
                }
                hideLoader();
            },
            error: function () {
                hideLoader();
                swal("Упс!", "Что-то пошло не так.", "error");
            },
        });
    }else{
        hideLoader();
    }

});


$(".quantity").on("change paste keyup",function () {
    showLoader();
    var id = $(this).attr('data-id');
    var v = $(this).val();
    $.ajax({
        url: "/card/count-changed",
        type: "GET",
        dataType: "json",
        data: {id:id,v:v},
        success: function(data){
            if(data.status){
                $('#countProduct' + id).html(data.countProduct);
                $('#sumBasket').html(data.sum+" ₸");
                $('#sumProduct' + id).html(data.sumProduct+" ₸");
                $('.count').html(data.header_count);
                hideLoader();
            }else{
                hideLoader();
                $("#countProduct" + id).val(data.countProduct);
                Swal.fire('', 'Извините, у нас нет больше!', 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$(".button-pay").click(function () {
    var typePay = $("#typePay").val();
    showLoader();
    $.ajax({
        type : "GET",
        url : "/card/order",
        data: {typePay:typePay},
        success: function(data){
            hideLoader();
            if (data == 1) {
                window.location.href = "/account/?tab=tab-2";
            } else if (data == 2) {
                window.location.href = "/card/gift";
            } else if (data == 0) {
                swal('', "Что-то пошло не так!", 'error');
            } else if (data == 3) {
                window.location.href = "/";
            } else if (data == 4) {
                swal({
                    title: "",
                    text: "Хотите ли Вы зарегистрироваться?",
                    icon: "info",
                    buttons: ["Нет", "Да"],
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location.href = "/account/sign-up";
                        } else {
                            $('#card-confirm').modal('show');
                        }
                    });
            } else if (data == 5) {
                window.location.href = "/paybox/index";
            } else swal('', data, 'error');
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$("#typePay").on("change", function () {
    if ($(this).val() == 1) {
        $('.button-pay').html("Оформить заказ");
    }
    else {
        $('.button-pay').html("Оплатить");
    }
    $.ajax({
        url: "/card/set-payment-choice",
        type: "GET",
        data: { payment_choice: $(this).val() },
        success: function (data) {

        },
    });
});



$('body').on('click', '.order_guest_confirm', function (e) {

    showLoader();
    $.ajax({
        type: 'POST',
        url: '/card/confirm-ordered-guest-data',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            hideLoader();
            if (response == 1) {
                $(".close").click();
                $(".button-pay").click();
            } else {
                swal('', response, 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});




