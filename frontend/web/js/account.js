

$('input[name="UserProfile[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="UserProfile[surname]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});


$('input[name="UserProfile[father]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});



$('body').on('click', '.register-button', function (e) {
    showLoader();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/site/register',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            hideLoader();
            if(response == 1){
                window.location.href = "/account?tab=tab-1";
            }else{
                $('#my-captcha-image').yiiCaptcha('refresh');
                Swal.fire('Ошибка!', response, 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.register-button-mob', function (e) {
    showLoader();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/site/register',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            hideLoader();
            if(response == 1){
                window.location.href = "/account";
            }else{
                $('#my-captcha-image').yiiCaptcha('refresh');
                Swal.fire('Ошибка!', response, 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});





$('body').on('click', '.login-button', function (e) {
    showLoader();
    $.ajax({
        type: 'POST',
        url: '/site/login',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            hideLoader();
            if(response == 1){
                window.location.href = "/account?tab=tab-1";
            }else{
                Swal.fire('Ошибка!', response, 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', '.login-button-mobile', function (e) {
    showLoader();
    $.ajax({
        type: 'POST',
        url: '/site/login',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            hideLoader();
            if(response == 1){
                window.location.href = "/account";
            }else{
                Swal.fire('Ошибка!', response, 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.btn-subscribe', function (e) {
    showLoader();
    $.ajax({
        type: 'GET',
        url: '/site/subscribe',
        data: $(this).closest('form').serialize(),
        success: function (data) {
            hideLoader();
            if(data == 1) {
                Swal.fire('','Вы успешно подписались на рассылку','success');
            }else{
                Swal.fire('', data,'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', '.update-profile-button', function (e) {
    showLoader();
    $.ajax({
        type: 'POST',
        url: '/account/update-account-data',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                hideLoader();
                Swal.fire('Ваши изменения',' успешно сохранен.', 'success');
                // setTimeout(function () {
                //     window.location.href = "/account?tab=tab-1";
                // }, 2000);
            }else{
                hideLoader();
                Swal.fire('Ошибка!', response, 'error');
            }
        },
        error: function () {
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.update-password-button', function (e) {
    showLoader();
    $.ajax({
        type: 'GET',
        url: '/account/update-password',
        data: $(this).closest('form').serialize(),
        success: function (data) {
            hideLoader();
            if(data == 1){
                Swal.fire('Ваши изменения',' успешно сохранен.', 'success');
                setTimeout(function () {
                    window.location.href = "/account?tab=tab-1";
                }, 2000);
            }else{
                Swal.fire('Ошибка!', data, 'error');
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', ".sendForgotPassword", function () {
    showLoader();
    var button = $(this).html();
    var email = $("#forget_password_email").val();
    $.ajax({
        url: "/site/forget-password",
        type: "GET",
        data: { email: email },
        success: function (data) {
            hideLoader();
            if (data == 1) {
                $(".close").click();
                swal(
                    "Уважаемый пользователь!",
                    "Инструкции по сбросу пароля были отправлены на вашу электронную почту"
                );
            } else {
                swal("Ошибка!", data, "error");
            }
        },
        error: function () {
            hideLoader();
            swal("Упс!", "Что-то пошло не так.", "error");
        },
    });
});



$("body").on("click",".btn-add-favorite",function () {
    showLoader();
    var product_id = $(this).attr('data-id');
    $.ajax({
        url: "/site/add-to-favorite",
        data: {product_id:product_id},
        type: "GET",
        datatype: 'JSON',
        success: function(data){
            hideLoader();
            data = JSON.parse(data);
            if(data.status){
                Swal.fire('', data.text);
            }else{
                window.location.href = "/site/sign-in";
            }
        },
        error: function (data) {
            hideLoader();
        }
    });

});



$("body").on("click",".btn-delete-product-from-favorite",function () {
    showLoader();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/account/delete-from-favorite",
        data: {id:id},
        type: "GET",
        success: function(data){
            hideLoader();
            if(data == 0){
                Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
            }else{
                $('#tab-4').html(data);
            }
        },
        error: function () {
            hideLoader();
            Swal.fire('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});





function setProfileBlock(tab_id) {
    $.ajax({
        url: "/account/set-tab",
        type: "GET",
        data: {tab:tab_id},
        success: function(data){

        },
    });
}
