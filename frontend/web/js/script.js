$('body').on('click', '.categoryBtn',function(e){
    var id = $(this).attr('data-id');
    var catalog_id = $(this).attr('data-catalog-id');
    e.preventDefault();
    $.ajax({
        url: '/catalog/get-subcategory',
        data: {id:id,catalog_id:catalog_id},
        type: 'GET',
        success: function (response) {
            $('#CategoryResult').html(response);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.subCategoryBtn',function(e){
    var id = $(this).attr('data-id');

    e.preventDefault();
    $.ajax({
        url: '/catalog/get-sub-subcategory',
        data: {id:id},
        type: 'GET',
        success: function (response) {
            $('#CategoryResult').html(response);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.prevBtn',function(e){
    var id = $(this).attr('data-id');
    var catalog_id = $(this).attr('data-catalog-id');
    e.preventDefault();
    $.ajax({
        url: '/catalog/get-back-category',
        data: {id:id, catalog_id:catalog_id},
        type: 'GET',
        success: function (response) {
            $('#CategoryResult').html(response);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});