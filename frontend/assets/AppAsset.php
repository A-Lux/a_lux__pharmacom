<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/owl.carousel.min.css',
        'css/owl.theme.default.min.css',
        'css/animate.css',
        'css/hamburgers.css',
        'css/nprogress.css',
        'css/style.css',
        'css/hc-offcanvas-nav.css',
        'https://use.fontawesome.com/releases/v5.7.1/css/all.css',
        'css/superfish.css',
        'css/aos.css',
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
        "https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.css"
    ];
    public $js = [
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.js",
        "js/bootstrap.min.js",
        "js/hc-offcanvas-nav.js",
        "js/wow.min.js",
        "js/owl.carousel.min.js",
        "js/main.js",
        'js/nprogress.js',
        "js/basket.js",
        "js/script.js",
        "js/filter.js",
        "js/account.js",
        "js/aos.js",
        "js/superfish.min.js",
        "js/search.js",

    ];
    public $depends = [
//        'yii\web\YiiAsset',
    ];
}
