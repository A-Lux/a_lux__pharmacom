<?php

/** @var array $params */

return [
        'class' => 'yii\web\UrlManager',

        'baseUrl' => '',
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'cache' => false,
        'rules' => [
            [
                'class' => 'yii\rest\UrlRule',
                'controller' => 'orders',
                'pluralize' => false,
            ],

            'catalog/offer-related-products' => 'catalog/offer-related-products',
            'catalog/filter' => 'catalog/filter',
            'catalog/<url:[\w-]+>' => 'catalog/index',
            'catalog/<url1:[\w-]+>/<url:[\w-]+>' => 'catalog/index',
            'catalog/<url1:[\w-]+>/<url2:[\w-]+>/<url:[\w-]+>' => 'catalog/index',
            'product/<id:[\w-]+>/<url:[\w-]+>' => 'catalog/product',
            'about' => 'about/index',
            'contact' => 'contact/index',
            'faq' => 'faq/index',
            'address' => 'address/index',
            'vacancies' => 'vacancies/index',
            'reviews' => 'reviews/index',
            'card' => 'card/index',
            'search' => 'search/index',
            'account' => 'account/index',
            'paybox/index' => 'paybox/index',
            '<url:[\w-]+>' => 'site/index2',


            '<controller:\w+>/<id:\d+>'=>'<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

    ],
];
