<?php
namespace app\models;

use Yii;

class Admission extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'admission';
    }

    public function rules()
    {
        return [
            [['user_id', 'menu', 'banner', 'client', 'user_bonus', 'clientele', 'filial', 'catalogs', 'products', 'country', 'gift', 'discounts', 'discount_clientele', 'orders', 'sms_notification_lists', 'aboutus', 'about_content', 'about_story', 'about_services', 'trigger_birthday', 'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'text', 'message', 'contact', 'emailforrequest', 'vacancy', 'faq', 'reviews', 'subscribe', 'logo'], 'required'],
            [['user_id', 'menu', 'banner', 'client', 'user_bonus', 'clientele', 'filial', 'catalogs', 'products', 'country', 'gift', 'discounts', 'discount_clientele', 'orders', 'sms_notification_lists', 'aboutus', 'about_content', 'about_story', 'about_services', 'trigger_birthday', 'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'text', 'message', 'contact', 'emailforrequest', 'vacancy', 'faq', 'reviews', 'subscribe', 'logo'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'menu' => 'Menu',
            'banner' => 'Banner',
            'client' => 'Client',
            'user_bonus' => 'User Bonus',
            'clientele' => 'Clientele',
            'filial' => 'Filial',
            'catalogs' => 'Catalogs',
            'products' => 'Products',
            'country' => 'Country',
            'gift' => 'Gift',
            'discounts' => 'Discounts',
            'discount_clientele' => 'Discount Clientele',
            'orders' => 'Orders',
            'sms_notification_lists' => 'Sms Notification Lists',
            'aboutus' => 'Aboutus',
            'about_content' => 'About Content',
            'about_story' => 'About Story',
            'about_services' => 'About Services',
            'trigger_birthday' => 'Trigger Birthday',
            'trigger_holiday' => 'Trigger Holiday',
            'trigger_tovar_month' => 'Trigger Tovar Month',
            'trigger_remind_basket' => 'Trigger Remind Basket',
            'text' => 'Текст',
            'message' => 'Message',
            'contact' => 'Contact',
            'emailforrequest' => 'Emailforrequest',
            'vacancy' => 'Vacancy',
            'faq' => 'Faq',
            'reviews' => 'Reviews',
            'subscribe' => 'Subscribe',
            'logo' => 'Logo',
        ];
    }
}
