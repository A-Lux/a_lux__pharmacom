<?php

namespace frontend\models;

use common\models\Country;
use common\models\Products;
use common\models\UserFavorites;
use Yii;

class ProductSearch extends Products
{
    public $path = 'images/products/';

    public $lev;

    public static function tableName()
    {
        return 'products_search_view';
    }

}
