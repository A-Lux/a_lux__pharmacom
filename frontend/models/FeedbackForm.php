<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 12:21
 */

namespace frontend\models;


use common\models\Feedback;
use common\models\Message;
use Yii;
use yii\base\Model;

/**
 * This is the model class for table "feedbacks".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $time
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 */

class FeedbackForm extends Model
{

    public $name;
    public $phone;
    public $time;
    public $comment;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required', 'message' => 'Необходимо заполнить поле «{attribute}».'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'phone', 'time'], 'string', 'max' => 255],
            [['comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'time' => 'Удобное время для звонка',
            'comment' => 'Ваши комментарии',
            'created_at' => 'Дата отправления',
            'updated_at' => 'Дата редактирования',
        ];
    }




    public function saveFeedback(){

        $model = new Feedback();
        $model->name = $this->name;
        $model->phone = $this->phone;
        $model->time = $this->time;
        $model->comment = $this->comment;
        $error = $this->validateModel();
        if(!$error){
            $this->sendMessage();
            return $model->save();
        }else{
            return $error;
        }

    }



    private function validateModel(){

        if ($this->validate()) {
            if ($this->validateTelephone()) {
                $error = false;
            }else{
                $error = "Необходимо заполнить «Телефон».";
            }
        } else {
            $error = "";
            $errors = $this->getErrors();
            foreach ($errors as $v) {
                $error .= $v[0];break;
            }
        }
        return $error;
    }


    private function validateTelephone(){
        $phoneStatus = true;
        for ($i = 0; $i < strlen($this->phone); $i++) {
            if ($this->phone[$i] == '_') {
                $phoneStatus = false;
            }
        }
        return $phoneStatus;
    }


    private function sendMessage(){

        $data = Message::getFeedbackMessage();
        return Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => 'ТОО «Фармаком»'])
            ->setTo(Yii::$app->view->params['adminEmail'])
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['name' => $this->name, 'phone' => $this->phone, 'time' => $this->time, 'comment' => $this->comment]))
            ->send();
    }

}
