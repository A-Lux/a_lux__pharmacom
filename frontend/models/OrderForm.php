<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.02.2020
 * Time: 10:29
 */

namespace frontend\models;
use common\models\Gift;
use common\models\Orderedproduct;
use common\models\Orders;
use common\models\SmsNotificationLists;
use common\models\User;
use common\models\UserBonus;
use common\models\UserGift;
use common\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\httpclient\Client;



/**
 * This is the model class for table "OrderFormForUser".
 *
 * @property int $paymentMethod
 * @property int $user_id
 * @property int $fio
 * @property int $telephone
 * @property int $email
 * @property int $statusPay
 * @property int $statusProgress
 * @property int $sum
 * @property int $used_bonus
 * @property int $used_discount_for_senior
 * @property int $used_discount_for_regular
 * @property int $access_token
 */

class OrderForm extends Model
{
    public $paymentMethod;

    public $user_id;
    public $fio;
    public $telephone;
    public $email;
    public $statusPay;
    public $statusProgress;
    public $sum;
    public $used_bonus;
    public $used_discount_for_senior;
    public $used_discount_for_regular;

    const isUserSuccessOrdered = 1;
    const isGuestSuccessOrdered = 3;
    const isError = 0;
    const haveGift = 2;
    const clientIsGuest = 4;
    const isPayMethod = 5;

    const paid = 1;
    const notPaid = 0;

    const isGift = 1;
    const isNotGift = 0;

    const received = 1;
    const notReceived = 0;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['statusPay', 'paymentMethod'], 'required'],
            [['fio','telephone','email'], 'string', 'max' => 255],
            [['user_id', 'statusPay','statusProgress', 'sum','used_bonus',
                'used_discount_for_senior','used_discount_for_regular'], 'integer'],
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
//            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function saveOrder()
    {
        $order = new Orders();
        $order->fio = null;
        $order->telephone = null;
        $order->email = null;
        $order->statusPay = self::notPaid;
        $order->statusProgress = self::notReceived;
        $order->sum = $this->sum;
        $order->paymentMethod = $this->paymentMethod;

        if($this->paymentMethod == 1){
            $order->is_active = 1;
        }else{
            $order->is_active = 0;
            $order->generateAuthKey();
        }

        if(Yii::$app->user->isGuest){
            $order->user_id = null;
            $order->fio = $this->fio;
            $order->telephone = $this->telephone;
            $order->email = $this->email;

            $order->used_discount_for_senior = 0;
            $order->used_discount_for_regular = 0;
            $order->used_bonus = 0;
        }else{
            $order->user_id = $this->user_id;
            $order->used_bonus = $this->used_bonus;
            $order->fio = null;
            $order->telephone = null;
            $order->email = null;

            $order->used_discount_for_senior = $this->used_discount_for_senior;
            $order->used_discount_for_regular = $this->used_discount_for_regular;

            UserBonus::deleteBonus($order->used_bonus);
        }

        if($order->save()){

            $this->saveOrderedGifts($order);
            $response = $this->saveOrderedProducts($order);

            if($this->paymentMethod == 1){
                $products = $order->products;
                $this->sendMessageToOurPhoneNumbers($order->id);
                $this->sendMessageToEmail($products, $order->used_bonus, $order->used_discount_for_senior, $order->used_discount_for_regular);
//                $this->sendMessageToCallCentre();
                $message_id = $this->sendMessageToPhoneNumber($order->id);

                if(Yii::$app->user->isGuest){
                    Yii::$app->session['guest-order-success'] = 1;
                }
            }


            $order->response = $response;
            $order->message_id = $message_id;
            $order->save(false);
            return $response;
        }else{
            return 0;
        }
    }


    private function saveOrderedProducts(Orders $order){

        $products = $_SESSION['basket'];
        if($products != null){
            $bonus = 0;
            foreach ($products as $k => $v){
                $orderedProduct = new Orderedproduct();
                $orderedProduct->order_id = $order->id;
                $orderedProduct->product_id = $v->id;
                $orderedProduct->count = $v->count;
                $orderedProduct->isGift = self::isNotGift;
                $orderedProduct->save();

                $bonus += $v->bonus;
            }
        }

        if($this->paymentMethod == 2) {
            $response = self::isPayMethod;
        }else{
            if($_SESSION['basket'] != null && !Yii::$app->user->isGuest){
                UserBonus::addBonus($bonus);
                $response = Gift::checkGift($this->sum);
            }elseif (Yii::$app->user->isGuest){
                if($paymentMethod == 1) {
                    Yii::$app->session['guest-order-success'] = 1;
                }
                $response = self::isGuestSuccessOrdered;
            }else{
                $response = self::isUserSuccessOrdered;
            }
        }

        $this->deleteSessions();
        return $response;
    }


    private function saveOrderedGifts(Orders $order){
        $gifts = $_SESSION['gift'];
        if($gifts != null){
            foreach ($gifts as $k => $v){
                $orderedProduct = new Orderedproduct();
                $orderedProduct->order_id = $order->id;
                $orderedProduct->product_id = $v->id;
                $orderedProduct->count = $v->count;
                $orderedProduct->isGift = self::isGift;
                if($orderedProduct->save()){
                    $check_gift = UserGift::findOne(['user_id' => Yii::$app->user->id,'product_id' => $orderedProduct->product_id]);
                    if($check_gift) $check_gift->delete();
                }
            }
        }
    }

    private function deleteSessions(){
        unset($_SESSION['basket'], $_SESSION['gift'], $_SESSION['guest_data'], Yii::$app->session['bonus']);
    }


    public function sendMessageToOurPhoneNumbers($order_id){

        $phone_lists = SmsNotificationLists::getActivePhones();
        $message = 'Ваш заказ принят под номером '.$order_id.'. Ожидайте, с вами свяжется Интернет-аптека Добрая.';

        if($phone_lists != null) {
            foreach ($phone_lists as $v) {
                $telephone = '7'.substr($v->phone, 1);
                $client = new Client();
                $client->createRequest()
                    ->setMethod('GET')
                    ->setUrl('http://kazinfoteh.org:9501/api')
                    ->setData([
                        'action' => 'sendmessage',
                        'username' => Yii::$app->params["PhoneMessageUsername"],
                        "password" => Yii::$app->params["PhoneMessagePassword"],
                        "recipient" => $telephone,
                        "messagetype" => "SMS:TEXT",
                        "originator" => "INFO_KAZ",
                        "messagedata" => $message,])
                    ->send();
            }
        }
    }


    public function sendMessageToPhoneNumber($order_id){

        $telephone = $this->user_id == null ? $this->telephone : User::findOne($this->user_id)->username;
        $telephone = '7'.substr(preg_replace('/[^a-zA-Z0-9]/','', $telephone), 1);
        $message = 'Ваш заказ принят под номером '.$order_id.'. Ожидайте, с вами свяжется Интернет-аптека Фармаком.';
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('http://kazinfoteh.org:9501/api')
            ->setData([
                'action' => 'sendmessage',
                'username' => Yii::$app->params["PhoneMessageUsername"],
                "password" => Yii::$app->params["PhoneMessagePassword"],
                "recipient" => $telephone,
                "messagetype" => "SMS:TEXT",
                "originator" => "INFO_KAZ",
                "messagedata" => $message,])
            ->send();

        $message_id = null;
        if ($response->isOk) {
            if(isset($response->data['data']) && isset($response->data['data']['acceptreport']) && $response->data['data']['acceptreport']['messageid']) {
                $message_id = $response->data['data']['acceptreport']['messageid'];
            }
        }

        return $message_id;
    }


    public function sendMessageToCallCentre(){

        $telephone = $this->user_id == null ? $this->telephone : User::findOne($this->user_id)->username;
        $telephone = preg_replace('/[^a-zA-Z0-9]/','', $telephone);
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('https://ok.7line.kz/api/v1/dialers/create_task')
            ->setData(['dialer_id' => '89', 'api_token' => '3d06b68f8e7f2d17788713c6d5aabf08', 'phones' => $telephone])
            ->send();

        return $response;
    }



    public function sendMessageToEmail($model, $used_bonus, $used_discount_for_senior, $used_discount_for_regular){

        $email = $this->user_id == null ? $this->email : User::findOne($this->user_id)->email;
        $fio =  $this->user_id == null ? $this->fio : UserProfile::findOne(['user_id' => $this->user_id])->getFio();
        $sum = $this->sum;
        $host = Yii::$app->request->hostInfo;

        if($email){
            $emailSend = Yii::$app->mailer
                ->compose('order', compact('model', 'fio', 'host', 'sum', 'used_bonus', 'used_discount_for_regular',
                'used_discount_for_senior'))
                ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «Фармаком»'])
                ->setTo($email)
                ->setSubject('Детали заказа');
            return $emailSend->send();
        }
    }


}
