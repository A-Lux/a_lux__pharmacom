<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.02.2020
 * Time: 10:34
 */

namespace frontend\models;
use common\models\Orderedproduct;
use common\models\Orders;
use common\models\UserGift;
use Yii;
use yii\base\Model;

/**
 * This is the model class for table "OrderFormForGuest".
 *
 * @property int $fio
 * @property int $telephone
 * @property int $email
 */

class OrderFormForGuest extends Model
{

    public $fio;
    public $telephone;
    public $email;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone'], 'required'],
            [['fio','telephone','email'], 'string', 'max' => 255],
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'telephone' => 'Телефон',
            'email' => 'E-mail',
        ];
    }

}
