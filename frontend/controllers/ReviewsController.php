<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 14:36
 */

namespace frontend\controllers;


use common\models\Menu;
use common\models\Reviews;
use common\models\User;
use common\models\UserProfile;
use Yii;
use yii\web\NotFoundHttpException;

class ReviewsController extends FrontendController
{

    public function actionIndex()
    {
        $reviews = Reviews::getAll();
        $user = User::findOne(Yii::$app->user->id);
        $profile = UserProfile::getUserProfile();

        return $this->render('index', compact('reviews', 'user', 'profile'));
    }


    public function actionComment()
    {
        if (Yii::$app->request->isAjax) {
            $model = new Reviews();

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->get())) {
                if ($model->validate()) {
                    if ($model->save()) {
                        return 1;
                    }
                } else {

                    $error = "";
                    $errors = $model->getErrors();
                    foreach ($errors as $v) {
                        $error .= $v[0];
                        break;
                    }
                    return $error;
                }
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


}
