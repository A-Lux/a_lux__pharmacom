<?php

namespace frontend\controllers;

use common\models\Analogy;
use common\models\Products;
use common\models\SopTovary;
use common\models\CatalogProducts;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


/**
 * Catalog controller
 */
class CatalogController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],

        ];
    }



    public function actionIndex($url)
    {
        $catalog = CatalogProducts::findOne(['url' => $url]);
        if($catalog){
            $this->setMeta($catalog->title);
            $category_id = $catalog->id;
            $data = CatalogProducts::getProducts($catalog->id);
            $categories = CatalogProducts::getCategories($catalog->id);
            $products = $data['products'];
            $pagination = $data['pagination'];
        }else{
            throw new NotFoundHttpException();
        }


        return $this->render('index', compact('catalog', 'products', 'category_id', 'categories', 'pagination'));

    }


    public function actionProduct($id, $url)
    {
        $product = Products::findOne(['url' => $url, 'id' => $id]);
        if($product && $product->category) {
            $this->setMeta($product->title);
            $catalog = CatalogProducts::findOne($product->category->id);
            $sop_tovary = SopTovary::getProductsBySql($product);
            $analogy = Analogy::getProductsBySql($product);

            return $this->render('product',compact( 'product', 'sop_tovary', 'catalog', 'analogy'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionFilter()
    {

        $category_id = $_GET['category_id'];
        $catalog = CatalogProducts::findOne($category_id);
        if(isset($_GET['from']) && isset($_GET['to']) && isset($_GET['category_id']) && isset($_GET['available']) && isset($_GET['discount'])
            && isset($_GET['isNew']) && isset($_GET['isHit']) && isset($_GET['top_month']) && $catalog != null) {

            $sql = '';
            $check = 0;

            // GET DISCOUNT PRODUCTS
            if ($_GET['discount']) {
                if ($check) $sql .= ' AND (discount != 0 OR discount != NULL)';
                else $sql .= ' (discount != 0 OR discount != NULL)';
                $check = 1;
            }

            // END GET DISCOUNT PRODUCTS


            // GET NEW PRODUCTS
            if ($_GET['isNew']) {
                if ($check) $sql .= ' AND (isNew != 0 OR isNew != NULL)';
                else $sql .= '(isNew != 0 OR isNew != NULL)';
                $check = 1;
            }

            // END GET NEW PRODUCTS


            // GET HIT PRODUCTS
            if ($_GET['isHit']) {
                if ($check) $sql .= ' AND (isHit != 0 OR isHit != NULL)';
                else  $sql .= ' (isHit != 0 OR isHit != NULL)';
                $check = 1;
            }

            // END GET HIT PRODUCTS


            // GET TOP MONTH PRODUCTS
            if ($_GET['top_month']) {
                if ($check) $sql .= ' AND (top_month != 0 OR top_month != NULL) ';
                else   $sql .= ' (top_month != 0 OR top_month != NULL) ';
                $check = 1;
            }

            // END TOP MONTH PRODUCTS


            // GET BY FROM AND TO PRICE
            $checkPrice = 0;
            if ($_GET['from']) {
                if ($check) $sql .= " AND (price >= " . $_GET['from'];
                else   $sql .= " (price >= " . $_GET['from'];
                $checkPrice = 1;
                $check = 1;
            }

            if ($_GET['to']) {
                if ($check) {
                    if ($checkPrice) $sql .= " AND price <= " . $_GET['to'];
                    else $sql .= " AND (price <= " . $_GET['to'];
                } else {
                    if ($checkPrice) $sql .= " price <= " . $_GET['to'];
                    else $sql .= " (price <= " . $_GET['to'];
                }

                $checkPrice = 1;
            }

            if ($checkPrice) $sql .= ")";

            // END GET BY FROM AND TO PRICE


            $data = CatalogProducts::getProducts($category_id,  $sql);
            $products = $data['products'];
            $pagination = $data['pagination'];

            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('filter', compact('products', 'pagination', 'category_id'));
            } else {
                $categories = CatalogProducts::getCategories($catalog->id);
                $this->setMeta($catalog->title);
                $filter = ["from" => $_GET['from'], "to" => $_GET['to'], 'available' => $_GET['available'], 'discount' => $_GET['discount'],
                    'isNew' => $_GET['isNew'], 'isHit' => $_GET['isHit'], 'top_month' => $_GET['top_month']];

                return $this->render('index', compact('catalog', 'products', 'top_products', 'category_id',
                    'categories', 'breadcrumbs', 'pagination', 'filter'));
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionOfferRelatedProducts(){
        if (Yii::$app->request->isAjax) {
            $product_id = $_GET['id'];
            $sop_tovary = SopTovary::findAll(['product1' => $product_id]);
            if ($sop_tovary) {
                return $this->renderAjax('relatedProducts', compact('error', 'sop_tovary'));
            } else {
                return 0;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }

}
