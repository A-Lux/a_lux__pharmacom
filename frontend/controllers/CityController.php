<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.02.2019
 * Time: 11:00
 */

namespace frontend\controllers;

use common\models\City;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class CityController extends FrontendController
{
    public function actionSetCity($city){

        $city = City::findOne(['id' => $city]);
        Yii::$app->session->set('city_id',$city->id);
        Yii::$app->session->set('city_name',$city->name);

        $this->redirect($_SERVER['HTTP_REFERER']);
    }



}