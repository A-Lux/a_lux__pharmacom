<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 14:26
 */

namespace frontend\controllers;


use common\models\Faq;
use common\models\Menu;

class FaqController extends FrontendController
{
    public function actionIndex()
    {
        $faq = Faq::getAll();

        return $this->render('index', compact('faq'));
    }
}
