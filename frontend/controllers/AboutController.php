<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 14:21
 */

namespace frontend\controllers;


use common\models\AboutContent;
use common\models\AboutServices;
use common\models\AboutStory;
use common\models\Aboutus;
use common\models\Menu;

class AboutController extends FrontendController
{
    public function actionIndex(){

        $aboutUs = Aboutus::getContent();
        $aboutContent = AboutContent::getAll();
        $aboutStory = AboutStory::getAll();
        $aboutServices = AboutServices::getAll();

        return $this->render('index', compact(
            'aboutUs',
            'aboutContent',
            'aboutStory',
            'aboutServices'
        ));
    }

}
