<?php

namespace frontend\controllers;


use common\models\Orders;
use frontend\models\OrderForm;
use Yii;
use Paybox\Pay\Facade as PayboxApi;
use yii\web\NotFoundHttpException;

class PayboxController extends CardController
{
    public function actionIndex()
    {
        if(isset($_SESSION['payment_token'])){
            $order = Orders::findByToken();
            if($order){

                $order->deleteAccessToken();

                $telephone =  $order->user_id == null ? $order->telephone : $order->user->username;
                $email =  $order->user_id == null ? $order->email : $order->user->email;

                $component = Yii::$app->paybox;
                $component->Index($order->id, 'Оплата заказа №'.$order->id, $order->sum, preg_replace('/[^a-zA-Z0-9]/','', $telephone), $email);

                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        throw new NotFoundHttpException();
    }


    public function actionResult()
    {

        if (isset($_GET['pg_order_id']) && isset($_GET['pg_payment_id']) &&
            isset($_GET['pg_result']) && $_GET['pg_result'] == 1) {

            $model = Orders::findOne($_GET['pg_order_id']);
            if($model && $model->is_active == 0){

                date_default_timezone_set('Asia/Almaty');

                $form = new OrderForm();
                $form->user_id = $model->user_id;
                $form->telephone = $model->user_id == null ? $model->telephone : $model->getPhone();
                $form->fio = $model->user_id == null ? $model->fio : $model->getFio();
                $form->email = $model->user_id == null ? $model->email : $model->getEmail();
                $form->sum = $model->sum;

                $products = $model->products;
                $form->sendMessageToOurPhoneNumbers($model->id);
//                $form->sendMessageToCallCentre();
                $form->sendMessageToEmail($products, $model->used_bonus, $model->used_discount_for_senior, $model->used_discount_for_regular);
                $message_id = $form->sendMessageToPhoneNumber($model->id);

                $model->is_active = 1;
                $model->message_id = $message_id;
                $model->payment_id = $_GET['pg_payment_id'];
                $model->statusPay = OrderForm::paid;
                $model->created_at = date('Y-m-d H:i:s');
                $model->save(false);

            }else{
                $paybox = new PayboxApi();
                return $paybox->cancel('Ошибка');
            }

        }elseif(isset($_GET['pg_order_id']) && isset($_GET['pg_result']) && $_GET['pg_result'] == 0){
            $paybox = new PayboxApi();
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionSuccess()
    {

        if (isset($_GET['pg_payment_id']) && isset($_GET['pg_order_id'])) {
            $model = Orders::findOne($_GET['pg_order_id']);
            if($model){

                if($model->response == 1){
                    return $this->redirect("/account/?tab=orders");
                }else if($model->response == 2){
                    return $this->redirect('card/gift');
                }else{
                    Yii::$app->session['guest-order-success'] = 1;
                    return $this->redirect(Yii::$app->homeUrl);
                }
            }
        }

        throw new NotFoundHttpException();

    }




    public function actionFailure()
    {
        return $this->redirect(Yii::$app->homeUrl);
    }



}
