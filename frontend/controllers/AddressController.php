<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 14:29
 */

namespace frontend\controllers;

use common\models\City;
use common\models\Filial;
use common\models\Menu;
use yii\helpers\ArrayHelper;

class AddressController extends FrontendController
{
    public function actionIndex()
    {
        $city = City::getAll();
        $filial = Filial::getAllByCity();

        return $this->render('index', compact('city', 'filial'));
    }

}
