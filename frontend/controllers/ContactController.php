<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 14:24
 */

namespace frontend\controllers;


use common\models\Contact;
use common\models\Menu;

class ContactController extends FrontendController
{
    public function actionIndex()
    {
        $contact = Contact::getContent();
        return $this->render('index', compact('contact'));
    }

}
