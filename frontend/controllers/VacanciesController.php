<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 14:32
 */

namespace frontend\controllers;


use common\models\Menu;
use common\models\Vacancy;

class VacanciesController extends FrontendController
{

    public function actionIndex()
    {
        $vacancy = Vacancy::getAll();

        return $this->render('index', compact('vacancy'));
    }
}
