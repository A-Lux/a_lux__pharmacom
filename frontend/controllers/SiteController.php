<?php

namespace frontend\controllers;

use common\models\AboutContent;
use common\models\Aboutus;
use common\models\Banner;
use common\models\Filial;
use common\models\LoginForm;
use common\models\Menu;
use common\models\Message;
use common\models\Products;
use common\models\Subscribe;
use common\models\UserAddress;
use common\models\User;
use common\models\UserBonus;
use common\models\UserFavorites;
use common\models\UserProfile;
use common\models\ForgotYourPassword;
use common\models\CatalogProducts;
use frontend\models\FeedbackForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;




/**
 * Site controller
 */
class SiteController extends FrontendController
{


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@frontend/web/fonts/Scada.ttf',
            ],
        ];
    }

    public function actionIndex()
    {
        $catalog = CatalogProducts::getMainCatalog();
        $productHit = Products::getSomeHitProducts(0);
        $productNew = Products::getSomeTopMonthProducts(0);
        $productDiscount = Products::getSomeDiscountProducts(0);
        $aboutUs = Aboutus::getContent();
        $aboutContent = AboutContent::getAll();
        $address = Filial::getAllByCity();
        $banners = Banner::getAll();

        return $this->render('index', compact('catalog', 'productHit', 'productNew', 'productDiscount', 'aboutUs',
            'address', 'result', 'aboutContent', 'banners'));
    }



    public function actionIndex2($url)
    {

        $page = Menu::findOne(['url' => $url]);
        if ($page && ($page->status || $page->footer_status)) {
            return $this->render('index2', compact('page'));
        } else {
            throw new NotFoundHttpException();
        }
    }




    public function actionSignIn(){
        if(Yii::$app->user->isGuest){
            return $this->render('login');
        }else{
            return $this->redirect('/account?tab=tab-1');
        }

    }


    public function actionSignUp(){
        if(Yii::$app->user->isGuest) {
            $model = new SignupForm();
            return $this->render('register', compact('model'));
        }else{
            return $this->redirect('/account?tab=tab-1');
        }
    }


    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }



    public function actionLogin()
    {
        if (Yii::$app->request->isAjax) {
            $model = new LoginForm();

            if ($model->load(Yii::$app->request->post())) {
                if ($user = $model->login()) {
                    return 1;
                } else {
                    $error = "";
                    $errors = $model->getErrors();

                    foreach ($errors as $v) {
                        $error .= $v[0];
                        break;
                    }
                    return $error;
                }
            }
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionRegister()
    {
        if (Yii::$app->request->isAjax) {
            $model = new SignupForm();
            $profile = new UserProfile();
            $address = new UserAddress();

            if ($model->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {
                if ($address['address'][0] != null) {
                    $password = $model->password;
                    if ($user = $model->signUp()) {
                        $profile->user_id = $user->id;
                        if ($profile->save()) {
                            foreach ($address['address'] as $v) {
                                $newAddress = new UserAddress();
                                $newAddress->user_id = $user->id;
                                $newAddress->address = $v;
                                $newAddress->save();
                            }
                            $this->sendInformationAboutRegistration($profile->fio, $user->email, $password, $user->username);
                            Yii::$app->getUser()->login($user);
                            $balance = new UserBonus();
                            $balance->saveBalance($user->id);
                            return 1;
                        }
                    } else {
                        $user_error = "";
                        $user_errors = $model->getErrors();
                        foreach ($user_errors as $v) {
                            $user_error .= $v[0];
                            break;
                        }
                        return $user_error;
                    }
                } else {
                    return 'Необходимо заполнить «Адрес».';
                }
            } else {
                $profile_error = "";
                $profile_errors = $profile->getErrors();
                foreach ($profile_errors as $v) {
                    $profile_error .= $v[0];
                    break;
                }

                return $profile_error;

            }
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionFeedback(){
        if (Yii::$app->request->isAjax) {
            $model = new FeedbackForm();
            if ($feedback = $model->load(Yii::$app->request->get())) {
                return $model->saveFeedback();
            }

            return "Что-то пошло не так";
        }else{
            throw new NotFoundHttpException();
        }

    }


    public function actionAddToFavorite(){

        if (Yii::$app->request->isAjax){
            if(!Yii::$app->user->isGuest) {
                $product_id = $_GET['product_id'];
                $favorite = UserFavorites::getByUserAndProduct($product_id);
                if ($favorite == null) {
                    $model = new UserFavorites();
                    $model->saveUserFavorite($product_id);
                    $text = 'Товар добавлен в избранное!';
                } else {
                    $favorite->delete();
                    $text = 'Товар удален с избранного!';
                }
                $response = ['status' => 1, 'text' => $text];
            }else{
                $response = ['status' => 0];
            }
            return json_encode($response);;
        }else{
            throw new NotFoundHttpException();
        }

    }



    public function actionSubscribe()
    {
        $model = new Subscribe();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                if ($model->save() && $model->sendEmail()) {
                    return 1;
                }
            } else {

                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionForgetPassword(){
        if (Yii::$app->request->isAjax) {
            $reset = new ResetPasswordForm();
            $reset->email = $_GET['email'];
            if ($reset->validate()) {
                $check = User::findOne(['email' => $_GET['email']]);
                if ($check) {
                    $check->generatePasswordResetToken();
                    if ($check->save(false)) {
                        $profile = UserProfile::findOne(['user_id' => $check->id]);
                        $link = Yii::$app->request->hostInfo . '/site/confirm?token=' . $check->password_reset_token;
                        return $this->sendUpdatePasswordInstruction($check->email, $link, $profile->getFio());
                    }
                } else {
                    return 'E-mail не зарегистрирован.';
                }
            } else {
                $error = "";
                $errors = $reset->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;

            }
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionConfirm($token)
    {
        if (empty($token) || !is_string($token) || null === $user = User::findByResetToken($token)) {
            echo 'Ошибка.';
        } else {
            $user->removePasswordResetToken();
            $newPassword = $user->getRandomPassword();
            $user->setPassword($newPassword);
            if($user->save(false)){
                Yii::$app->session['modal-pass-reset'] = 1;
                $profile = UserProfile::findOne(['user_id' => $user->id]);
                $this->sendNewPassword($user->email, $newPassword, $profile->getFio());
                return $this->redirect('/');
            }else{
                echo 'Oops, something is error!';
            }
        }
    }


    private function sendNewPassword($email, $password, $fio)
    {
        $data = Message::getGeneratedNewPasswordMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «Фармаком»'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['password' => $password, 'fio' => $fio]));
        return $emailSend->send();
    }




    private function sendUpdatePasswordInstruction($email, $link, $fio)
    {
        $data = Message::getForgetPasswordMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «Фармаком»'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['link' => $link, 'fio' => $fio]));
        return $emailSend->send();
    }


    private function sendInformationAboutRegistration($fio, $email, $password, $telephone)
    {
        $data = Message::getRegisterMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «Фармаком»'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['fio' => $fio, 'phone' => $telephone, 'password' => $password]));
        return $emailSend->send();
    }


}
