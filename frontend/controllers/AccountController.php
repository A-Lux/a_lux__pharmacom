<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2019
 * Time: 11:31
 */

namespace frontend\controllers;

use common\models\Menu;
use common\models\Message;
use common\models\Orders;
use common\models\UserFavorites;
use common\models\User;
use common\models\UserAddress;
use common\models\UserGift;
use common\models\UserProfile;
use common\models\UserBonus;
use frontend\models\PasswordUpdate;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class AccountController extends FrontendController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['profile'],
                'rules' => [

                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {

        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/sign-in');
        }
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }


    public function actionIndex($tab)
    {

        if(isset($_SESSION['tab']) && $_SESSION['tab'] == "tab-2") {
            $tab = $_SESSION['tab'];
        }

        $user = User::getUser();
        $profile = UserProfile::getUserProfile();
        $userBonus = UserBonus::getUserBonus();
        $address = UserAddress::getAllByUser();
        $orders = Orders::getAllByUser(10);
        $favorites = UserFavorites::getFavorites();
        $gifts = UserGift::getAllByUser();

        return $this->render('profile', compact('user','profile','address','orders','favorites','tab', 'gifts','userBonus'));

    }



    public function actionUpdateAccountData()
    {
        if (Yii::$app->request->isAjax) {
            $user = User::findOne(Yii::$app->user->id);
            $user->scenario = User::update_all_data;
            $profile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);
            $address = new UserAddress();

            if ($user->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {

                if ($user->password != null) {
                    $user->scenario = User::update_all_data;
                } else {
                    $user->scenario = User::update_data;
                }

                if ($address['address'][0] != null) {
                    if ($user->validate()) {
                        if ($user->password != null) {
                            $user->setPassword($user->password);
                        }
                        if ($profile->save() && $user->save()) {
                            UserAddress::deleteAll(['user_id' => Yii::$app->user->id]);
                            foreach ($address['address'] as $v) {
                                $newAddress = new UserAddress();
                                $newAddress->user_id = Yii::$app->user->id;
                                $newAddress->address = $v;
                                $newAddress->save();
                            }
                            return 1;
                        }
                    } else {
                        $user_error = "";
                        $user_errors = $user->getErrors();
                        foreach ($user_errors as $v) {
                            $user_error .= $v[0];
                            break;
                        }
                        return $user_error;
                    }
                } else return 'Необходимо заполнить «Адрес».';
            } else {
                $profile_error = "";
                $profile_errors = $profile->getErrors();
                foreach ($profile_errors as $v) {
                    $profile_error .= $v[0];
                    break;
                }
                return $profile_error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionUpdatePassword()
    {
        if (Yii::$app->request->isAjax) {
            $user = new PasswordUpdate();

            if ($user->load(Yii::$app->request->get()) && $user->validate()) {
                if ($user->signUp()) {
                    return 1;
                }
            } else {
                $error = "";
                $errors = $user->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }


    public function actionDeleteFromFavorite()
    {
        if (Yii::$app->request->isAjax) {
            $favorite = UserFavorites::getByUserAndProduct($_GET['id']);
            if ($favorite && $favorite->delete()) {
                $favorites = UserFavorites::getFavorites();
                return $this->renderAjax('favorite', compact('favorites', 'error'));
            } else {
                return 0;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionSetTab()
    {
        if (Yii::$app->request->isAjax) {
           $_SESSION['tab'] = $_GET['tab'];
        }else{
            throw new NotFoundHttpException();
        }
    }





}
