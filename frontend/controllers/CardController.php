<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2019
 * Time: 11:32
 */

namespace frontend\controllers;

use common\models\Discounts;
use common\models\FilialProduct;
use common\models\Products;
use common\models\DiscountClientele;
use common\models\UserBonus;
use common\models\UserGift;
use frontend\models\OrderForm;
use frontend\models\OrderFormForGuest;
use Yii;
use yii\web\NotFoundHttpException;

class CardController extends FrontendController
{

    public function actionIndex()
    {

        if(Yii::$app->user->isGuest) {
            $model = new OrderFormForGuest();
        }else{
            $bonus = UserBonus::getBonus();
            $discount_for_senior_citizen = Discounts::getCalculatedDiscount();
            $discount_for_regular_customer = DiscountClientele::getCalculatedDiscount();
        }

        $sum = $this->getSumBasket();;
        Yii::$app->session['payment_choice'] = 1;
        Yii::$app->session['bonus'] = 0;

        return $this->render('basket',compact('sum','model', 'bonus', 'discount_for_regular_customer',
            'discount_for_senior_citizen'));
    }

    
    public function actionGift()
    {

        if (count($_SESSION['gift_for_type']) != null){
            if ($_SESSION['gift_for_type'] == 1) {
                return $this->render('gift_for_price');
            } elseif ($_SESSION['gift_for_type'] == 0) {
                return $this->render('gift_for_product');
            } elseif ($_SESSION['gift_for_type'] == 2) {
                return $this->render('gift_for_price');
            } else {
                return $this->goHome();
            }
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionSaveGift($id){

        if(count($_SESSION['gift_for_type']) != null){
            $check = $this->checkGift($id);
            if($check){
                $product = Products::findOne($id);
                UserGift::addUserGift($product);
                if($_SESSION['gift_for_type'] == 2) {
                    unset($_SESSION['gift_id']);
                    unset($_SESSION['gift_for_price']);
                    $_SESSION['gift_for_type'] = 0;
                    return $this->redirect("/card/gift");
                }else{
                    unset($_SESSION['gift_id']);
                    unset($_SESSION['gift_for_price']);
                    unset($_SESSION['gift_for_product']);
                    unset($_SESSION['gift_for_type']);
                    return $this->redirect("/account?tab=tab-2");
                }
            }else{
                throw new NotFoundHttpException();
            }
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function checkGift($id){
        $status = false;
        if ($_SESSION['gift_for_type'] == 1 || $_SESSION['gift_for_type'] == 2) {
            foreach ($_SESSION['gift_for_price'] as $k => $v){
                if($v->id == $id){
                    $status = true;
                    break;
                }
            }
        }elseif($_SESSION['gift_for_type'] == 0){
            foreach ($_SESSION['gift_for_product'] as $k => $v){
                if($v->id == $id){
                    $status = true;
                    break;
                }
            }
        }

        return $status;
    }


    public function actionAddGiftToBasket()
    {
        if (Yii::$app->request->isAjax) {
            $basket = UserGift::getAllByUser();
            foreach ($basket as $val) {
                $check = true;
                $product = $val->product;
                foreach ($_SESSION['gift'] as $v) {
                    if ($product->id == $v->id) {
                        $check = false;
                        break;
                    }
                }
                if ($check) {
                    $product->count = $val->quantity;
                    $product->price = 0;
                    array_push($_SESSION['gift'], $product);
                }
            }

            $count = count($_SESSION['basket']) + count($_SESSION['gift']);
            $array = ['status' => 1, 'count' => $count];

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionDeleteGiftProduct($id)
    {

        $m = 0;
        foreach ($_SESSION['gift'] as $v) {
            if ($v->id == $id) {
                unset($_SESSION['gift'][$m]);
                $_SESSION['gift'] = array_values($_SESSION['gift']);
                break;
            }
            $m++;
        }
        return $this->redirect(['index']);
    }


    public function actionDeleteProduct($id)
    {

        $m = 0;
        foreach ($_SESSION['basket'] as $v) {
            if ($v->id == $id) {
                unset($_SESSION['basket'][$m]);
                $_SESSION['basket'] = array_values($_SESSION['basket']);
                break;
            }
            $m++;
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteAllProduct(){
        unset($_SESSION['basket']);
        return $this->redirect(['index']);
    }



    public function actionAddProductToBasket()
    {
        if (Yii::$app->request->isAjax) {

            $id = $_GET['id'];
            $amount = $_GET['amount'];

            $status = 0;
            $basket = Products::findOne($id);
            $basket->count = $amount;
            $check = true;

            $sum = FilialProduct::find()
                ->distinct()
                ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                ->where('filial.status = 2')
                ->andWhere(['filial.city_id' => Yii::$app->session["city_id"]])
                ->andWhere(['filial_product.product_id' => $id])
                ->sum('amount');

            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $id) {
                    $check = false;

                    if($sum >= $_SESSION['basket'][$k]['count']+$amount) {
                        $status = 1;
                        $_SESSION['basket'][$k]['count'] += $amount;
                    }

                    break;
                }
            }

            if ($check) {
                if($sum > 0){
                    $status = 1;
                    array_push($_SESSION['basket'], $basket);
                }

            }

            $count = $this->getCount();
            $array = [
                'status'    => $status,
                'count'     => $count
            ];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }

    }




    public function actionDownClicked()
    {

        if (Yii::$app->request->isAjax) {
            $count = 1;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id'] && $_SESSION['basket'][$k]->count > 1) {
                    $_SESSION['basket'][$k]['count'] -= 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $sum = $this->getSumBasket();;
            $sumProduct = $this->getSumBasketProduct($_GET['id']);;
            $header_count = $this->getCount();

            $array = [
                'countProduct'          => $count,
                'sum'                   => $sum,
                'sumProduct'            => $sumProduct,
                'header_count'          => $header_count
            ];

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionUpClicked()
    {
        if (Yii::$app->request->isAjax) {

            $status = false;
            $count = 1;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id']) {
                    $sum = FilialProduct::find()
                        ->distinct()
                        ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                        ->where('filial.status = 2')
                        ->andWhere(['filial.city_id' => Yii::$app->session["city_id"]])
                        ->andWhere(['filial_product.product_id' => $_GET['id']])
                        ->sum('amount');
                    if($sum >= $_SESSION['basket'][$k]['count']+1){
                        $_SESSION['basket'][$k]['count'] += 1;
                        $status = true;
                    }

                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $sum = $this->getSumBasket();;
            $sumProduct = $this->getSumBasketProduct($_GET['id']);;
            $header_count = $this->getCount();

            if($status){
                $array = [
                    'status'            => 1,
                    'countProduct'      => $count,
                    'sum'               => number_format(intval($sum), 0, '', ' '),
                    'sumProduct'        => number_format(intval($sumProduct), 0, '', ' '),
                    'header_count'      => $header_count
                ];
            }else{
                $array = [
                    'status' => 0,
                    'countProduct' => $count,
                ];
            }

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionCountChanged()
    {
        if (Yii::$app->request->isAjax) {

            $status = false;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id']) {
                    if ($_GET['v'] > 0) {
                        $sum = FilialProduct::find()
                            ->distinct()
                            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                            ->where('filial.status = 2')
                            ->andWhere(['filial.city_id' => Yii::$app->session["city_id"]])
                            ->andWhere(['filial_product.product_id' => $_GET['id']])
                            ->sum('amount');
                        if($sum >= $_GET['v']) {
                            $_SESSION['basket'][$k]['count'] = $_GET['v'];
                            $status = true;
                        }
                    } else {
                        $_SESSION['basket'][$k]['count'] = 1;
                        $status = true;
                    }

                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $sum = $this->getSumBasket();;
            $sumProduct = $this->getSumBasketProduct($_GET['id']);;
            $header_count = $this->getCount();

            if($status){
                $array = [
                    'status'                => 1,
                    'countProduct'          => $count,
                    'sum'                   => number_format(intval($sum), 0, '', ' '),
                    'sumProduct'            => number_format(intval($sumProduct), 0, '', ' '),
                    'header_count'          => $header_count
                ];
            }else{
                $array = [
                    'status' => 0,
                    'countProduct' => $count,
                ];
            }

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionOrder()
    {
        if(($_SESSION['basket'] != null ||  $_SESSION['gift'] != null) && Yii::$app->request->isAjax) {

            $paymentMethod = $_GET["typePay"];
            if($paymentMethod  != 1 && $paymentMethod != 2) return 'Неправильно выбран способ оплаты!';
            if (Yii::$app->user->isGuest && $_SESSION['guest_data'] == null) return OrderForm::clientIsGuest;

            $used_bonus = 0;

            $model = new OrderForm();
            $model->paymentMethod = $paymentMethod;
            $model->sum = $this->getSumBasket();;
            $model->used_bonus = $used_bonus;

            if (!Yii::$app->user->isGuest) {
                $model->user_id = Yii::$app->user->id;
                $model->used_bonus = Yii::$app->session['bonus'];
                $model->used_discount_for_senior = Discounts::getCalculatedDiscount();
                $model->used_discount_for_regular = DiscountClientele::getCalculatedDiscount();
            } else {
                $model->fio = $_SESSION['guest_data']->fio;
                $model->telephone = $_SESSION['guest_data']->telephone;
                $model->email = $_SESSION['guest_data']->email;
            }

            return $model->saveOrder();

        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionBonus(){

        if (Yii::$app->request->isAjax && !Yii::$app->user->isGuest) {

            $typed_bonus = $_GET['bonus'];
            Yii::$app->session['bonus'] = 0;

            if ($typed_bonus > 0) {

                $my_bonus = UserBonus::getBonus();
                $sum = $this->getSumBasket();

                if($typed_bonus <= $sum){
                    $bonus = $typed_bonus;
                }else{
                    $bonus = $sum;
                }

                if($my_bonus < $bonus){
                    $bonus = $my_bonus;
                }

                Yii::$app->session['bonus'] = $bonus;
            }

            $response = [
                'sum' => number_format(intval($this->getSumBasket()), 0, '', ' '),
                'bonus' => Yii::$app->session['bonus']
            ];
            return json_encode($response);

        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionConfirmOrderedGuestData(){
        if(Yii::$app->request->isAjax) {
            $model = new OrderFormForGuest();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $phoneStatus = true;
                for ($i = 0; $i < strlen($model->telephone); $i++) {
                    if ($model->telephone[$i] == '_') {
                        $phoneStatus = false;
                    }
                }
                if ($phoneStatus) {
                    $_SESSION['guest_data'] = $model;
                    return 1;
                } else {
                    return "Необходимо заполнить «Телефон».";
                }

            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }




    private function getSumBasket()
    {
        $sum = 0;
        if ($_SESSION['basket'] != null) {
            foreach ($_SESSION['basket'] as $v) {
                $price = (int) $v->calculatePrice;
                $sum += (int) $v->count * $price;
            }
        }

        $discount_for_senior_citizen = Discounts::getCalculatedDiscount();
        $discount_for_regular_customer = DiscountClientele::getCalculatedDiscount();

        if(!Yii::$app->user->isGuest && $discount_for_senior_citizen != 0){
            $sum = $sum * ((100 - $discount_for_senior_citizen) / 100);
        }

        if(!Yii::$app->user->isGuest && $discount_for_regular_customer != 0){
            $sum = $sum * ((100 - $discount_for_regular_customer) / 100);
        }

        if(isset(Yii::$app->session['bonus']) && Yii::$app->session['bonus'] > 0){
            $sum = $sum - Yii::$app->session['bonus'];
        }

        return intval($sum);
    }


    private function getSumBasketProduct($id)
    {
        $sum = 0;
        if ($_SESSION['basket'] != null) {
            foreach ($_SESSION['basket'] as $v) {
                if ($v->id == $id) {
                    $price = (int) $v->calculatePrice;
                    $sum += (int) $v->count * $price;
                }
            }
        }
        return intval($sum);
    }


}
