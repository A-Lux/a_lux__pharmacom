<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_bonus".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bonus
 */
class UserBonus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_bonus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'bonus'], 'required'],
            [['user_id', 'bonus'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'bonus' => 'Бонус',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }


    public function getUserName()
    {
        return (isset($this->user))?$this->user->fio: 'Не задан';
    }

    public static function getData(){
        return UserBonus::findOne(['user_id'=>Yii::$app->user->id]);
    }

    public static function getBonus(){
        $bonus = 0;
        if($data = UserBonus::getData()) $bonus = $data->bonus;
        return $bonus;
    }

    public static function addBonus($bonus){
        $data = UserBonus::getData();
        if($data){
            $data->bonus += $bonus;
            $data->save(false);
        }
    }

    public static function deleteBonus($used_bonus){
        $data = UserBonus::getData();
        if($data){
            $data->bonus -= $used_bonus;
            return $data->save(false);
        }
    }

    public static function getUserBonus(){
        return UserBonus::findOne(['user_id' => Yii::$app->user->id]);
    }


    public function saveBalance($user_id, $bonus = 0){
        $this->user_id = $user_id;
        $this->bonus = $bonus;
        return $this->save();
    }
}
