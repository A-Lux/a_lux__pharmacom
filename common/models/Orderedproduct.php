<?php
namespace common\models;

use Yii;


/**
 * This is the model class for table "OrderFormForUser".
 *
 * @property int $order_id
 * @property int $product_id
 * @property int $count
 * @property int $isGift
 */



class Orderedproduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orderedproduct';
    }

    public function rules()
    {
        return [
            [['order_id', 'product_id', 'count', 'isGift'], 'required'],
            [['order_id', 'product_id', 'count', 'isGift'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
