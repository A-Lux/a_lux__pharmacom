<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Country".
 *
 * @property int $id
 * @property string $name
 */

class Country extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'country';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getAll(){
        return Country::find()->orderBy('name', 'ASC')->all();
    }
}
