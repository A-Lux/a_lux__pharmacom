<?php
namespace common\models;

use Yii;

class UserGift extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_gift';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id','quantity'], 'required'],
            [['user_id', 'product_id','quantity'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количества'
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(),['id'=>'product_id']);
    }

    public static function addUserGift($product){
        $check = UserGift::findOne(['product_id'=>$product->id, 'user_id' => Yii::$app->user->id]);
        if($check){
            $gift = $check;
            $gift->quantity += 1;
        }else {
            $gift = new UserGift();
            $gift->quantity = 1;
        }
        $gift->user_id = Yii::$app->user->id;
        $gift->product_id = $product->id;

        return $gift->save(false);
    }

    public static function getAllByUser(){
        return UserGift::findAll(['user_id' => Yii::$app->user->id]);
    }

    public static function getOneByProduct($id){
        return UserGift::find()->where(['product_id' => $id])->one();
    }
}
