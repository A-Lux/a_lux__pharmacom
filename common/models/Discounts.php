<?php

namespace common\models;

use Yii;
use common\models\UserProfile;

/**
 * This is the model class for table "discounts".
 *
 * @property int $id
 * @property string $discount
 * @property string $year
 */
class Discounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discount', 'year'], 'required'],
            [['discount', 'year'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount' => 'Скидка (%)',
            'year' => 'От (лет)',
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'long');
    }

    public static function getData(){
        return Discounts::findOne(['id' => 1]);
    }


    public static function getCalculatedDiscount()
    {
        $discount = 0;
        if(!Yii::$app->user->isGuest){
            $profile = UserProfile::findOne(["user_id" => Yii::$app->user->id]);
            $data = Discounts::getData();
            if($profile->checkDiscountForSeniorCitizen($data->discount)) $discount = $data->discount;
        }

        return $discount;
    }  

}
