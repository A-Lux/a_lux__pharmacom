<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $image
 * @property string $copyright
 */
class Logo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright'], 'required'],
            [['copyright'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Логотип',
            'copyright' => 'Копирайт',
        ];
    }


    public static function getContent(){
        return Logo::find()->one();
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }



}
