<?php
namespace common\models;

use Yii;

class UserFavorites extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_favorites';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'product_id' => 'Продукт',
        ];
    }

    public static function getFavorites(){
        $favorites = UserFavorites::findAll(['user_id'=>Yii::$app->user->id]);
        if($favorites != null){
            $arr = '(';
            foreach ($favorites as $v){
                $arr.=$v->product_id.',';
            }
            $arr = substr($arr,0,strlen($arr)-1);
            $arr.= ')';
            return Products::find()->where('id in '.$arr)->all();
        }
    }

    public static function getByUserAndProduct($product_id){
        return UserFavorites::findOne(['user_id' => Yii::$app->user->id, 'product_id' => $product_id]);
    }

    public function saveUserFavorite($product_id){
        $fav = new UserFavorites();
        $fav->user_id = Yii::$app->user->id;
        $fav->product_id = $product_id;
        return $fav->save(false);
    }

}
