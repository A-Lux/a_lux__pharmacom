<?php

namespace common\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $message
 * @property int $status
 * @property string $date
 * @property int $raiting
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'message'], 'required', 'message' => 'Необходимо заполнить поле «{attribute}».'],
            ['raiting', 'required'],
            [['message'], 'string'],
            [['date'], 'safe'],
            [['raiting', 'status'], 'integer'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'message' => 'Комментарий',
            'status' => 'Статус',
            'date' => 'Время',
            'raiting' => 'Оценка',
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'long');
    }

    public static function getAll($pageSize=3)
    {
        $query =  Reviews::find()->where('status=1')->orderBy('date DESC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize, 'pageParam' => 'reviews','pageSizeParam' => false, 'forcePageParam' => false]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
