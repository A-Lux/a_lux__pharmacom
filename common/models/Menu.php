<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $text
 * @property int $status
 * @property int $footer_status
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 * @property string $image
 * @property string $mobile_image
 * @property int $sort
 * @property string $url
 * @property int $isNew
 */

class Menu extends \yii\db\ActiveRecord
{

    public $path = 'images/menu/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'status', 'metaName', 'footer_status'], 'required'],
            [['status', 'footer_status', 'isNew'], 'integer'],
            [['metaDesc', 'metaKey', 'content'], 'string'],
            [['text', 'metaName', 'url'], 'string', 'max' => 255],
            [['image', 'mobile_image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => '	Заголовок',
            'status' => 'Статус (Хэдер)',
            'footer_status' => 'Статус (Футер)',
            'content' => 'Содержание',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'image' => 'Иконка для десктопу',
            'mobile_image' => 'Иконка для мобилку',
            'url' => 'Ссылка'
        ];
    }

    public function getDesktopImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path . $this->image : '';
    }

    public function getMobileImage()
    {
        return ($this->image) ? '/backend/web/'.$this->path . $this->mobile_image : '';
    }


    public function getUrl()
    {
        return '/'.$this->url;
    }

    public static function getAllByHeader(){
        return Menu::find()->where('status=1')->orderBy('sort ASC')->all();
    }

    public static function getAllByFooter(){
        return Menu::find()->where('footer_status=1')->orderBy('sort ASC')->all();
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\common\models\Menu::find()->all(),'id','text');
    }

    public static function getPage($url){
        return Menu::findOne(['url' => $url]);
    }

}
