<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $phone
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const update_all_data = 'update_all_data';
    const update_data = 'update_data';
    const client = 'client';
    const admin = 'admin';

    public $old_password;
    public $password;
    public $password_verify;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['old_password', 'password', 'password_verify'], 'required'],
            [['old_password', 'password', 'password_verify'], 'string', 'min' => 6],
            [['password_verify'], 'compare', 'compareAttribute' => 'password'],
            ['old_password', 'validatePassword'],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'password_verify' => 'Подтверждение пароля',
            'created_at' => 'Дата создание',
            'updated_at' => 'Дата редактирование'
        ];
    }

    public function scenarios()
    {
        return [
            self::update_all_data => ['email','password','password_verify'],
            self::update_data => ['email'],
            self::admin => ['username'],
            self::client => ['username', 'password']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone]);
    }

    public static function findByPassword($old_password)
    {
        return static::findOne(['password' => $old_password]);
    }

    public static function findByResetToken($token)
    {
        return static::findOne(['password_reset_token' => $token]);
    }




    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if(isset($this->password)){
                    $this->setPassword($this->password);
                }elseif(empty($this->password_hash)) {
                    $this->setPassword(\Yii::$app->security->generateRandomString());
                }
                $this->generateAuthKey();
            }
            return true;
        }

        return false;
    }


    public function getRandomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $password.= $alphabet[$n];
        }
        return $password;
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'username');
    }


    public static function getUserList()
    {
        return \yii\helpers\ArrayHelper::map(self::findAll(['role' => null]), 'id', 'username');
    }

    public static function getAdmin(){
        return User::find()->where('role='.Yii::$app->user->identity->role)->one();
    }

    public static function getUser(){
        return User::findOne(Yii::$app->user->id);
    }


    public function getAddress(){
        return $this->hasOne(UserAddress::className(), ['user_id' => 'id']);
    }


    public function getFio(){
        return $this->profile ? $this->profile->fio : "Не указано";
    }


    public function getDateOfBirth(){
        return $this->profile ? $this->profile->date_of_birth : "Не указано";
    }

    public function getAddressName(){
        return $this->address ? $this->address->address : "Не указано";
    }

    public function deleteAdmission(){
        Admission::deleteAll(['user_id' => $this->id]);
    }
}
