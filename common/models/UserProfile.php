<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * UserProfile model
 *
 * @property integer $id
 * @property string $user_id
 * @property string $name
 * @property string $surname
 * @property string $father
 * @property string $date_of_birth

 */

class UserProfile extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_profile';
    }

    public function rules()
    {
        return [
            [['name', 'surname', 'father'], 'required'],
            [['user_id'], 'integer'],
            [['date_of_birth'], 'safe'],
            [['name', 'surname', 'father'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'father' => 'Отчество',
            'date_of_birth' => 'День рождения',
        ];
    }

    public function fields()
    {
        return [
            'name'
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date_of_birth, 'long');
    }

    public function getFio(){
        return $this->name.' '.$this->surname.' '.$this->father;
    }

    public static function getAvailableList()
    {
        return ArrayHelper::map(
            self::find()->where(['not in', 'user_id', Clientele::getList()])->all(),
            'user_id', 'fio');
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'user_id', 'fio');
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function checkDiscountForSeniorCitizen($year){

        $year = Yii::$app->formatter->asTimestamp($year);
        $date_of_birthday = Yii::$app->formatter->asTimestamp($this->date_of_birth);

        if($year >= $date_of_birthday){
            return true;
        }else{
            return false;
        }
    }

    public static function getUserProfile(){
        return UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
    }
}
