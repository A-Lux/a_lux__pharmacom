<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clientele".
 *
 * @property int $id
 * @property int $user_id
 */
class Clientele extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientele';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
        ];
    }

    public static function checkUser()
    {
        $identity = Clientele::findOne(['user_id' => Yii::$app->user->id]);

        return $identity;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getUserName()
    {
        return (isset($this->user))?$this->user->username: 'Не задан';
    }

    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }


    public function getFio()
    {
        return $this->profile ? $this->profile->fio : "Не указано";
    }

    public static function checkDiscountForRegularCustomers(){
        return Clientele::findOne(["user_id" => Yii::$app->user->id]);
    }


    public static function getList(){
        return ArrayHelper::map(Clientele::find()->all(), 'id', 'user_id');
    }

}
