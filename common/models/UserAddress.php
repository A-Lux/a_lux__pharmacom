<?php
namespace common\models;

use Yii;

class UserAddress extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_address';
    }

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['user_id'], 'integer'],
            [['address'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'address' => 'Адрес',
        ];
    }

    public static function getAllByUser(){
        return UserAddress::findAll(['user_id'=>Yii::$app->user->id]);;
    }
}
