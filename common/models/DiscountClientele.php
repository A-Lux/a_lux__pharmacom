<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discount_clientele".
 *
 * @property int $id
 * @property int $discount
 */
class DiscountClientele extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discount_clientele';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discount'], 'required'],
            [['discount'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount' => 'Скидка (%)',
        ];
    }

    public static function getCalculatedDiscount(){


        $discount = 0;
        if(!Yii::$app->user->isGuest){
            $clientele = Clientele::checkDiscountForRegularCustomers();
            if($clientele){
                $discount = DiscountClientele::getDiscount();
            }

        }

        return $discount;
    }

    public static function getData(){
        return Discounts::findOne(['id' => 1]);
    }

    public static function getDiscount(){
        $data = Discounts::getData();
        return $data->discount;
    }
}
