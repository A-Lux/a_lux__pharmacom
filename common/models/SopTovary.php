<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "SopTovary".
 *
 * @property int $id
 * @property int $product1
 * @property int $product2
 */


class SopTovary extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'sop_tovary';
    }

    public function rules()
    {
        return [
            [['product1', 'product2'], 'required'],
            [['product1', 'product2'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product1' => 'Product1',
            'product2' => 'Product2',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'product2']);
    }

    public static function getProducts($id){

        $product = self::findAll(['product1' => $id]);
        $res = array();
        foreach ($product as $v) $res[$v->id] = $v->id;
        return $res;
    }

    public static function getProductsBySql($Product){

        $sop_tovary = self::findAll(['product1' => $Product->id]);
        $in = [];

        foreach ($sop_tovary as $v){
            if($v->product2 != null){
                $in[] = $v->product2;
            }
        }

        if($in){
            $sql = 'products.id in ('.implode(',', $in).')';
            $model =  Products::getByQuery($sql);
        }else{
            $model =  null;
        }


        return $model;
    }

    public function createNew($product1, $product2){

        $this->product1 = $product1;
        $this->product2 = $product2;
        return $this->save();
    }

}
