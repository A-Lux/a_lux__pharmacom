<?php
namespace common\models;

use Yii;
/**
 * This is the model class for table "Banner".
 *
 * @property int $id
 * @property string $image
 * @property string $url
 * @property int $sort
 */

class Banner extends \yii\db\ActiveRecord
{

    public $path = 'images/banner/';

    public static function tableName()
    {
        return 'banner';
    }

    public function rules()
    {
        return [

            [['url'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }



    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'url' => 'Ссылка',
            'sort' => 'Сортировка'
        ];
    }


    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public function getImageUrl(){
        return '@backend/web/'.$this->path;
    }

    public static function getAll(){
        return Banner::find()->orderBy(new \yii\db\Expression('rand()'))->all();
    }
}
