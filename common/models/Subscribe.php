<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscribe".
 *
 * @property int $id
 * @property string $email
 */
class Subscribe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscribe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'string', 'max' => 255],

            [['email'], 'trim'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass'=>'common\models\Subscribe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Эл. адрес',
        ];
    }

    public function sendEmail()
    {
        $emailforrequest = Emailforrequest::findOne($this->id = 1);
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => 'Фармаком'])
            ->setTo($emailforrequest->email)
            ->setSubject('Пользователь подписался на рассылку')
            ->setHtmlBody("<p>Email: $this->email</p>
                             ");

        return $emailSend->send();

    }
}
