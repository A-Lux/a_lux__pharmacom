<?php
namespace common\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "Filial".
 *
 * @property int $id
 * @property string $address
 * @property string $longitude
 * @property string $latitude
 * @property string $telephone
 * @property int $from_time
 * @property int $to_time
 * @property int $city_id
 * @property int $status
 * @property string $name
 */


class Filial extends \yii\db\ActiveRecord
{

    const IS_CLOSED = 1;
    const IS_OPENED = 2;

    public static function tableName()
    {
        return 'filial';
    }

    public function rules()
    {
        return [
            [['name', 'address', 'from_time', 'to_time', 'telephone', 'longitude', 'latitude', 'city_id'], 'required'],
            [['address'], 'string'],
            [['from_time', 'to_time'], 'safe'],
            [['city_id', 'status'], 'integer'],
            [['telephone', 'longitude', 'latitude', 'name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'address' => 'Адрес',
            'from_time' => 'От',
            'to_time' => 'До',
            'telephone' => 'Телефон',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'city_id' => 'Город',
            'status' => 'Статус'
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Filial::find()->all(),'id','address');
    }

    public function getCurrentStatus(){

        date_default_timezone_set('Asia/Almaty');

        try {
            $from_time = new DateTime($this->from_time);
            $to_time = new DateTime($this->to_time);
        } catch (\Exception $e) {}


        if($this->status != null && $this->status == self::IS_OPENED){
            $status = 'Открыто';
        }elseif ($this->status != null && $this->status == self::IS_CLOSED){
            $status = 'Закрыто';
        }else {
            $date = date('H', time()) * 3600 + date('i', time()) * 60;
            if ($from_time->format('H') < $to_time->format('H')) {
                $from_time = $from_time->format('H') * 3600 + $from_time->format('i') * 60;
                $to_time = $to_time->format('H') * 3600 + $to_time->format('i') * 60;
            } else {
                $from_time = $from_time->format('H') * 3600 + $from_time->format('i') * 60;
                $to_time = $to_time->format('H') * 3600 + $to_time->format('i') * 60 + 24 * 3600;
            }

            if ($date > $from_time && $date < $to_time) {
                $status = 'Открыто';
            } else {
                $status = 'Закрыто';
            }
        }

        return $status;
    }

    public static function getAllByCity(){
        return Filial::find()
            ->where(['city_id' => Yii::$app->session["city_id"]])
            ->orderBy('SUBSTR(name FROM 1 FOR 6), CAST(SUBSTR(name FROM 7) AS UNSIGNED)')
            ->all();
    }

}
