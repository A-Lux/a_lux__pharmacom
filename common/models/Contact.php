<?php
namespace common\models;

use Yii;

class Contact extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'contact';
    }

    public function rules()
    {
        return [
            [['address', 'email', 'telephone', 'working'], 'required'],
            [['address', 'email', 'telephone','phone', 'vk', 'facebook', 'instagram'], 'string', 'max' => 255],
            [['working'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'email' => 'Почта',
            'telephone' => 'Телефон 1',
            'phone' => 'Телефон 2',
            'working' => 'Время работы',
            'vk' => 'Ссылка на Вконтакте',
            'facebook' => 'Ссылка на Facebook',
            'instagram' => 'Ссылка на Instagram',
        ];
    }

    public static function getContent(){
        return Contact::find()->one();
    }
}
