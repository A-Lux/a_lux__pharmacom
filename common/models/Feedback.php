<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedbacks".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $time
 * @property string $comment
 * @property string $isRead
 * @property string $created_at
 * @property string $updated_at
 */
class Feedback extends \yii\db\ActiveRecord
{

    const isRead = 1;
    const isNotRead  = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedbacks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['isRead'], 'integer'],
            [['name', 'phone', 'time'], 'string', 'max' => 255],
            [['comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'time' => 'Время',
            'comment' => 'Комментарии',
            'created_at' => 'Дата отправления',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function isReaded(){
        $this->isRead = self::isRead;
        return $this->save(false);
    }
}
