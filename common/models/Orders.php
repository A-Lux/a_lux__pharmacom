<?php
namespace common\models;

use Yii;
use yii\data\Pagination;


/**
 * This is the model class for table "Orders".
 *
 * @property int $id
 * @property int $user_id
 * @property int $message_id
 * @property int $payment_id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property int $statusPay
 * @property int $statusProgress
 * @property int $sum
 * @property int $used_bonus
 * @property int $used_discount_for_senior
 * @property int $used_discount_for_regular
 * @property string $access_token
 * @property int $response
 * @property int $is_active
 * @property int $paymentMethod
 * @property string $created_at
 * @property string $updated_at
 */

class Orders extends \yii\db\ActiveRecord
{



    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
            [['statusPay', 'sum', 'statusProgress', 'paymentMethod', 'used_bonus'], 'required'],
            [['fio','telephone','email', 'access_token'], 'string', 'max' => 255],
            [['user_id', 'statusPay','statusProgress', 'sum','used_bonus',
                'used_discount_for_senior','used_discount_for_regular', 'response', 'paymentMethod'], 'integer'],
            [['created_at','updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'user_id' => 'ФИО',
            'fio' => 'ФИО',
            'telephone' => 'Телефон',
            'email' => 'E-mail',
            'statusProgress' => 'Статус подготовки',
            'statusPay' => 'Статус оплату',
            'sum' => 'Сумма заказа',
            'used_discount_for_senior' => 'Использованная скидка для пенсионеров',
            'used_discount_for_regular' => 'Использованная скидка для постоянных покупателей',
            'used_bonus' => 'Использованный бонус',
            'created_at' => 'Дата заказа',
            'updated_at' => 'Дата редактирования',
            'message_id' => 'Идентификатор сообщения',
            'payment_id' => 'Идентификатор оплаты',
            'is_active' => 'is_active',
            'paymentMethod' => 'Тип заказа'
        ];
    }

    public static function getAllByUser($pageSize=6)
    {
        $query =  Orders::find()->where(['user_id'=>Yii::$app->user->id])->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }


    public static function findByToken(){

        if(isset($_SESSION["payment_token"])){
            return Orders::findOne(['access_token' => $_SESSION["payment_token"]]);
        }else{
            return null;
        }
    }

    public function deleteAccessToken(){

        $this->access_token = null;
        $this->save(false);
        unset($_SESSION["payment_token"]);
    }


    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getProfile(){
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    public function getEmail(){
        return $this->user ? $this->user->email : "Не указано";
    }

    public function getPhone(){
        return $this->user ? $this->user->username : "Не указано";
    }

    public function getFio(){
        return $this->profile ? $this->profile->fio : "Не указано";
    }

    public function getUsedDiscountForSenior(){
        return $this->email ? $this->email : "Не указано";
    }

    public function getProducts(){
        return $this->hasMany(Orderedproduct::className(), ['order_id' => 'id'])->where(['orderedproduct.isGift' => 0]);
    }

    public function getGift(){
        return $this->hasMany(Orderedproduct::className(), ['order_id' => 'id'])->andOnCondition([ 'isGift' => 1]);
    }

    public function getUserCount()
    {
        $query =  Orders::find()->where(['user_id'=>Yii::$app->user->id]);
        $count = $query->count();
        return $count;
    }

    public function generateAuthKey()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
        $this->createAccessTokenSession();
    }

    public function createAccessTokenSession(){
        $_SESSION["payment_token"] =  $this->access_token;
    }


}
