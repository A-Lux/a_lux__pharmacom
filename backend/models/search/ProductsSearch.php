<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

class  ProductsSearch extends Products
{
    public function rules()
    {
        return [
            [['id', 'category_id', 'sort', 'status', 'isHit', 'isNew', 'sort_hit', 'sort_top_month'], 'integer'],
            [[ 'description', 'name', 'url', 'images', 'created_at', 'text'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $status = null)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'sort' => $this->sort,
            'status' => $this->status,
            'isHit' => $this->isHit,
            'isNew' => $this->isNew,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url]);

        if($this->text != null){
            if($this->text == 1){
                $query->andWhere(['not', ['text' => ""]]);
            }else{
                $query->andWhere(['text' => ""]);
            }
        }

        if($this->images != null){
            if($this->images == 1){
                $query->andWhere(['not', ['images' => ""]]);
            }else{
                $query->andWhere(['images' => null]);
            }
        }

        if ($status == 'isHit') {
            $query->andFilterWhere(['isHit' => 1])
                ->orderBy([new \yii\db\Expression('sort_hit IS NULL ASC, sort_hit ASC')]);
        }
        elseif($status == 'top_month'){
            $query->andFilterWhere(['top_month' => 1])
                ->orderBy([new \yii\db\Expression('sort_top_month IS NULL ASC, sort_top_month ASC')]);
        }else{
            $query->orderBy('sort ASC');
        }

        return $dataProvider;
    }
}
