<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Admission;

class AdmissionSearch extends Admission
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'menu', 'banner', 'client', 'user_bonus', 'clientele', 'filial', 'catalogs', 'products', 'country', 'gift', 'discounts', 'discount_clientele', 'orders', 'sms_notification_lists', 'aboutus', 'about_content', 'about_story', 'about_services', 'trigger_birthday', 'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'text', 'message', 'contact', 'emailforrequest', 'vacancy', 'faq', 'reviews', 'subscribe', 'logo'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Admission::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'menu' => $this->menu,
            'banner' => $this->banner,
            'client' => $this->client,
            'user_bonus' => $this->user_bonus,
            'clientele' => $this->clientele,
            'filial' => $this->filial,
            'catalogs' => $this->catalogs,
            'products' => $this->products,
            'country' => $this->country,
            'gift' => $this->gift,
            'discounts' => $this->discounts,
            'discount_clientele' => $this->discount_clientele,
            'orders' => $this->orders,
            'sms_notification_lists' => $this->sms_notification_lists,
            'aboutus' => $this->aboutus,
            'about_content' => $this->about_content,
            'about_story' => $this->about_story,
            'about_services' => $this->about_services,
            'trigger_birthday' => $this->trigger_birthday,
            'trigger_holiday' => $this->trigger_holiday,
            'trigger_tovar_month' => $this->trigger_tovar_month,
            'trigger_remind_basket' => $this->trigger_remind_basket,
            'text' => $this->text,
            'message' => $this->message,
            'contact' => $this->contact,
            'emailforrequest' => $this->emailforrequest,
            'vacancy' => $this->vacancy,
            'faq' => $this->faq,
            'reviews' => $this->reviews,
            'subscribe' => $this->subscribe,
            'logo' => $this->logo,
        ]);

        return $dataProvider;
    }
}
