<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 22.07.2018
 * Time: 2:02
 */

namespace backend\controllers;

use common\models\Admission;
use common\models\Feedback;
use Yii;
use yii\web\Controller;

class BackendController extends  Controller
{
    public function beforeAction($action){
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/admin/site/login')->send();
        }elseif(Yii::$app->user->identity->role == null){
            Yii::$app->user->logout();
            return $this->redirect('/admin/site/login')->send();
        }

        Yii::$app->view->params['admission'] = Admission::findOne(["user_id" => Yii::$app->user->id]);

        Yii::$app->view->params['feedback'] = Feedback::findAll(['isRead' => 0]);


        return parent::beforeAction($action);
		
    }
}
