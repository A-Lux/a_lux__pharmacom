<?php

namespace backend\controllers;

use common\models\Admission;
use common\models\UserAddress;
use common\models\UserProfile;
use Yii;
use common\models\User;
use backend\models\search\UserSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class ClientController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    public function afterAction($action, $result)
    {
        if (!Yii::$app->view->params['admission']->client){
            throw new NotFoundHttpException();
        }else {
            return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
        }
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', compact('model'));


    }


    public function rules()
    {
        $rules = parent::rules();
        unset($rules['password']);
        return $rules;
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = User::client;

        $profile = UserProfile::findOne(['user_id' => $id]);
        $address = UserAddress::findOne(['user_id' => $id]);

        if($profile && $address) {

            if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())
                && $address->load(Yii::$app->request->post())) {

                if($model->password != null){
                    $model->setPassword($model->password);
                }

                if ($model->save(false) && $profile->save() && $address->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->render('update', [
                'model' => $model,
                'profile' => $profile,
                'address' => $address
            ]);

        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleteProfile();
        $model->deleteBonus();
        $model->deleteAddresses();
        $model->deleteProducts();
        $model->deleteFavorites();
        $model->deleteGift();
        $model->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
