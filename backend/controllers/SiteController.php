<?php
namespace backend\controllers;

use common\models\Admission;
use common\models\Feedback;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('/admin/admin-profile');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) { return $this->goHome();}

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



    public function beforeAction($action) {

        $exception = Yii::$app->getErrorHandler()->exception;

        if(parent::beforeAction($action)) {
            $hasError = $action->id == 'error' && $exception !== NULL;

            if($hasError) {
                if(Yii::$app->user->isGuest || (!Yii::$app->user->isGuest && Yii::$app->user->identity->role == null)){
                    return Yii::$app->getResponse()->redirect('/admin/site/login')->send();
                }else{
                    Yii::$app->view->params['admission'] = Admission::findOne(["user_id" => Yii::$app->user->id]);

                    Yii::$app->view->params['feedback'] = Feedback::findAll(['isRead' => 0]);
                }
            }
        }
        return true;
    }
}
