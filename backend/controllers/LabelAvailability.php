<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.05.2020
 * Time: 13:55
 */

namespace backend\controllers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelAvailability
{
    public static function statusList()
    {
        return [
            1 => 'Имеет',
            0 => 'Не имеет',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}
