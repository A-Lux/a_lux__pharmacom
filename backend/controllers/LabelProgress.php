<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.03.2019
 * Time: 20:29
 */

namespace backend\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelProgress
{
    public static function statusList()
    {
        return [
            1 => 'Получено',
            0 => 'Не получено',
            2 => 'Отказано',
            3 => 'Принято в работу'
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            case 2:
                $class = 'label label-danger';
                break;
            case 3:
                $class = 'label label-warning';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


    public static function statusLabelValue($status)
    {
        $class = "";
        switch ($status) {
            case 0:
                $class = 'Не получено';
                break;
            case 1:
                $class = 'Получено';
                break;
            case 2:
                $class = 'Отказано';
                break;
            case 3:
                $class = 'Принято в работу';
                break;
        }

        return $class;
    }
}
