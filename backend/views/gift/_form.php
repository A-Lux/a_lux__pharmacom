<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

$my_products = $model->getAvailableProductsAsArray();
?>

<?php if(Yii::$app->session->getFlash('products_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('products_error'); ?>
    </div>
<?php endif;?>

<?php if(Yii::$app->session->getFlash('price_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('price_error'); ?>
    </div>
<?php endif;?>
<div class="gift-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?>

    <div class="form-group">
        <label>Продукт</label>
        <select class="form-control" id="gift_product"  data-placeholder="Выберите продукта"
                style="width: 100%;" name="product_id">
            <? if($model != null):?>
                <option value="<?=$model->product_id;?>" selected><?=$model->productName;?></option>
            <? endif;?>
        </select>
    </div>

    <?= $form->field($model, 'from_price')->textInput() ?>

    <?= $form->field($model, 'to_price')->textInput() ?>

    <div class="form-group">
        <label>Подарки</label>
        <select class="form-control" id="gift" multiple="multiple" data-placeholder="Выберите продукта"
                style="width: 100%;" name="products[]">
                <? if($products != 0):?>
                    <? foreach ($products as $v):?>
                        <option value="<?=$v->id;?>" selected><?=$v->name;?></option>
                    <? endforeach;?>
                <? endif;?>
        </select>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<script>

    if($('#gift-type').val() == 0){
        $('.field-gift-from_price').hide();
        $('.field-gift-to_price').hide();
        $('#gift_product').closest('div').show();
    }else{
        $('.field-gift-from_price').show();
        $('.field-gift-to_price').show();
        $('#gift_product').closest('div').hide();
    }



    $('#gift-type').change(function(){
        if($(this).val() == 0){
            $('.field-gift-from_price').hide();
            $('.field-gift-to_price').hide();
            $('#gift_product').closest('div').show();
        }else{
            $('.field-gift-from_price').show();
            $('.field-gift-to_price').show();
            $('#gift_product').closest('div').hide();
        }
    });


</script>
