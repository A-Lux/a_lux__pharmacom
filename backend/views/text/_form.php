<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="text-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea();?>

    <?= $form->field($model, 'type_id')->dropDownList(\common\models\TextType::getList(), ['disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
