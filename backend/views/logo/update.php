<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Логотип и Копирайт', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="logo-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
