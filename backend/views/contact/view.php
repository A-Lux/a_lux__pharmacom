<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = "Контакты";
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'address',
                'value' => $model->address,
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'telephone',
                'value' => $model->telephone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'working',
                'value' => $model->working,
                'format' => 'raw',
            ],
            [
                'attribute' => 'vk',
                'value' => $model->vk,
                'format' => 'raw',
            ],
            [
                'attribute' => 'facebook',
                'value' => $model->facebook,
                'format' => 'raw',
            ],
            [
                'attribute' => 'instagram',
                'value' => $model->instagram,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
