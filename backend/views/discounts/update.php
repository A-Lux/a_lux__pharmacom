<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Discounts */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => " Для пенсионеров", 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="discounts-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
