<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Discounts */

$this->title = " Для пенсионеров";
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="discounts-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'raw',
                'attribute' => 'discount',
                'value' => function($data){
                    return $data->year.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'year',
                'value' => function($data){
                    return $data->year.' лет';
                }
            ],
        ],
    ]) ?>

</div>
