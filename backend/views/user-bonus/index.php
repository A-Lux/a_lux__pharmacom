<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserBonusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бонусы пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-bonus-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            ['attribute' => 'user_id', 'value' => 'userName', 'filter' => \common\models\UserProfile::getList()],
            'bonus',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
