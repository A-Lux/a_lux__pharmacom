<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserBonus */

$this->title = 'Редактирование: ' . $model->getUserName();
$this->params['breadcrumbs'][] = ['label' => 'Бонусы пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-bonus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
