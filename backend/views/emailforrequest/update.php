<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Emailforrequest */

$this->title = 'Редактирование эл. почту';
$this->params['breadcrumbs'][] = ['label' => ' Эл. почта для связи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="emailforrequest-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
