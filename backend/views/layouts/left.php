<aside class="main-sidebar">

    <div class="user-panel">
        <div class="pull-left image">
            <img src="/backend/web/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?=Yii::$app->user->identity->username;?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
    </ul>

    <?php
        $user = null;$menu = null;$banner = null;
        $clients = null;$client = null;$user_bonus = null;$clientele = null;
        $productions = null;$cities = null;$filial = null;$catalogs = null;$products = null;$country = null;$gift = null;$gifts = null;
        $discounts = null;$discount_clientele = null;
        $orders = null;$order = null;$sms_notification_lists = null;
        $abouts = null; $aboutus = null; $about_content = null; $about_story = null; $about_services = null;
        $triggers = null;$birthday = null;$holiday = null;$tovar_month = null;$remind_basket = null;
        $texts = null;$text= null;$message = null;
        $contacts = null;$contact = null;$emailforrequest = null;
        $vacancy = null;$faq = null;$reviews = null;$subscribe = null;$logo = null;

        if (Yii::$app->user->identity->role == 1) {
            $user = ['label' => 'Доступы', 'icon' => 'fa fa-user', 'url' => ['/user'],'active' => $this->context->id == 'user'];}
        if (Yii::$app->view->params['admission']->menu) {
            $menu = ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/menu'],'active' => $this->context->id == 'menu'];}
        if (Yii::$app->view->params['admission']->banner) {
            $banner = ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/banner'],'active' => $this->context->id == 'banner'];}


        $check = false;
        if (Yii::$app->view->params['admission']->client) {
            $check = true;
            $client = ['label' => 'Список пользователей', 'icon' => 'fa fa-user', 'url' => ['/client'], 'active' => $this->context->id == 'client'];}
        if (Yii::$app->view->params['admission']->user_bonus) {
            $check = true;
            $user_bonus =  ['label' => 'Бонусы', 'icon' => 'fa fa-user', 'url' => ['/user-bonus'], 'active' => $this->context->id == 'user-bonus'];}
        if (Yii::$app->view->params['admission']->clientele) {
            $check = true;
            $clientele = ['label' => 'Список пост. покупателей', 'icon' => 'fa fa-user', 'url' => ['/clientele'], 'active' => $this->context->id == 'clientele'];}
        if ($check) {
            $clients = ['label' => 'Пользователей', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$client,$user_bonus,$clientele]];}



        $check = false;
        if (Yii::$app->view->params['admission']->discounts) {
            $check = true;
            $discounts = ['label' => 'Для пенсионеров', 'icon' => 'fa fa-user', 'url' => ['/discounts'], 'active' => $this->context->id == 'discounts'];}
        if (Yii::$app->view->params['admission']->discount_clientele) {
            $check = true;
            $discount_clientele =  ['label' => 'Для пост. покупателей', 'icon' => 'fa fa-user', 'url' => ['/discount-clientele'], 'active' => $this->context->id == 'discount-clientele'];}
        if ($check) {
            $gifts = ['label' => 'Скидки', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$discounts,$discount_clientele]];}


        $cities = ['label' => 'Города', 'icon' => 'fa fa-user', 'url' => ['/city'], 'active' => $this->context->id == 'city'];
        if (Yii::$app->view->params['admission']->filial) {
            $check = true;
            $filial = ['label' => 'Филиалы', 'icon' => 'fa fa-user', 'url' => ['/filial'], 'active' => $this->context->id == 'filial'];}
        if (Yii::$app->view->params['admission']->catalogs) {
            $check = true;
            $catalogs =  ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/catalog-products'], 'active' => $this->context->id == 'catalog-products'];}
        if (Yii::$app->view->params['admission']->products) {
            $check = true;
            $products = ['label' => 'Продукция', 'icon' => 'fa fa-user', 'url' => ['/products'], 'active' => $this->context->id == 'products'];}
        if (Yii::$app->view->params['admission']->country) {
            $check = true;
            $country = ['label' => 'Страны', 'icon' => 'fa fa-user', 'url' => ['/country'], 'active' => $this->context->id == 'country'];}
        if (Yii::$app->view->params['admission']->gift) {
            $check = true;
            $gift = ['label' => 'Подарки', 'icon' => 'fa fa-user', 'url' => ['/gift'], 'active' => $this->context->id == 'gift'];}
        if ($check) {
            $productions = ['label' => 'Продукция', 'icon' => 'fa fa-home', 'url' => '#', 'items' =>
                [$cities, $filial,$catalogs,$products, $country,$gift, $gifts]];}


        $check = false;
        if (Yii::$app->view->params['admission']->orders) {
            $check = true;
            $order = ['label' => 'Заказы', 'icon' => 'fa fa-user', 'url' => ['/orders'], 'active' => $this->context->id == 'orders'];}
        if (Yii::$app->view->params['admission']->sms_notification_lists) {
            $check = true;
            $discount_clientele =  ['label' => 'Список рассылки', 'icon' => 'fa fa-user', 'url' => ['/sms-notification-lists'], 'active' => $this->context->id == 'sms-notification-lists'];}
        if ($check) {
            $orders = ['label' => 'Заказы', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$order,$discount_clientele]];}



        $check = false;
        if (Yii::$app->view->params['admission']->aboutus) {
            $check = true;
            $aboutus = ['label' => 'О компании', 'icon' => 'fa fa-user', 'url' => ['/aboutus'], 'active' => $this->context->id == 'aboutus'];}
        if (Yii::$app->view->params['admission']->about_content) {
            $check = true;
            $about_content =  ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['/about-content'], 'active' => $this->context->id == 'about-content'];}
        if (Yii::$app->view->params['admission']->about_story) {
            $check = true;
            $about_story =  ['label' => 'Наша История', 'icon' => 'fa fa-user', 'url' => ['/about-story'], 'active' => $this->context->id == 'about-story'];}
        if (Yii::$app->view->params['admission']->about_services) {
            $check = true;
            $about_services =  ['label' => 'Наши услуги', 'icon' => 'fa fa-user', 'url' => ['/about-services'], 'active' => $this->context->id == 'about-services'];}
        if ($check) {
            $abouts = ['label' => 'О компании', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$aboutus,$about_content, $about_story, $about_services]];}




        $check = false;
        if (Yii::$app->view->params['admission']->trigger_birthday) {
            $check = true;
            $birthday = ['label' => 'В дни рождение ', 'icon' => 'fa fa-user', 'url' => ['/trigger-birthday'],'active' => $this->context->id == 'trigger-birthday'];}
        if (Yii::$app->view->params['admission']->trigger_holiday) {
            $check = true;
            $holiday = ['label' => 'В празднике ', 'icon' => 'fa fa-user', 'url' => ['/trigger-holiday'],'active' => $this->context->id == 'trigger-holiday'];}
        if (Yii::$app->view->params['admission']->trigger_tovar_month) {
            $check = true;
            $tovar_month= ['label' => 'При обновлении Товара Месяца', 'icon' => 'fa fa-user', 'url' => ['/trigger-tovar-month'],'active' => $this->context->id == 'trigger-tovar-month'];}
        if (Yii::$app->view->params['admission']->trigger_remind_basket) {
            $check = true;
            $remind_basket = ['label' => 'При не осущестивление покупку', 'icon' => 'fa fa-user', 'url' => ['/trigger-remind-basket'],'active' => $this->context->id == 'trigger-remind-basket'];}
        if ($check) {
            $triggers = ['label' => 'Триггерные письма', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$birthday,$holiday,$tovar_month,$remind_basket]];}



        $check = false;
        if (Yii::$app->view->params['admission']->text) {
            $check = true;
            $text = ['label' => 'По сайту', 'icon' => 'fa fa-user', 'url' => ['/text'], 'active' => $this->context->id == 'text'];}
        if (Yii::$app->view->params['admission']->message) {
            $check = true;
            $message =  ['label' => 'Сообщения', 'icon' => 'fa fa-user', 'url' => ['/message'], 'active' => $this->context->id == 'message'];}
        if ($check) {
            $texts = ['label' => 'Тексты', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$text,$message]];}


        $check = false;
        if (Yii::$app->view->params['admission']->contact) {
            $check = true;
            $contact = ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/contact'], 'active' => $this->context->id == 'contact'];}
        if (Yii::$app->view->params['admission']->emailforrequest) {
            $check = true;
            $emailforrequest =  ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/emailforrequest'], 'active' => $this->context->id == 'emailforrequest'];}
        if ($check) {
            $contacts = ['label' => 'Контакты', 'icon' => 'fa fa-home', 'url' => '#', 'items' => [$contact,$emailforrequest]];}


        if (Yii::$app->view->params['admission']->vacancy) {
            $vacancy =  ['label' => 'Вакансии', 'icon' => 'fa fa-user', 'url' => ['/vacancy'], 'active' => $this->context->id == 'vacancy'];}
        if (Yii::$app->view->params['admission']->faq) {
            $faq = ['label' => 'FAQ', 'icon' => 'fa fa-user', 'url' => ['/faq'], 'active' => $this->context->id == 'faq'];}
        if (Yii::$app->view->params['admission']->reviews) {
            $reviews = ['label' => 'Отзывы', 'icon' => 'fa fa-user', 'url' => ['/reviews'], 'active' => $this->context->id == 'reviews'];}
        if (Yii::$app->view->params['admission']->subscribe) {
            $subscribe = ['label' => 'Подписчики на рассылку', 'icon' => 'fa fa-user', 'url' => ['/subscribe'], 'active' => $this->context->id == 'subscribe'];}
        if (Yii::$app->view->params['admission']->logo) {
            $logo = ['label' => 'Логотип и Копирайт', 'icon' => 'fa fa-user', 'url' => ['/logo'], 'active' => $this->context->id == 'logo'];}

?>



    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    $user,
                    $menu,
                    $banner,
                    $clients,
                    $productions,
                    $orders,
                    $abouts,
                    $triggers,
                    $texts,
                    $contacts,
                    $vacancy,
                    $faq,
                    $reviews,
                    $subscribe,
                    $logo,
                ],
            ]
        ) ?>
    </section>

</aside>
