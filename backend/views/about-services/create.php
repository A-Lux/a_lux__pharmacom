<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutServices */

$this->title = 'Добавить услугу:';
$this->params['breadcrumbs'][] = ['label' => 'Наши услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-services-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
