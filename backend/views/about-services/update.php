<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutServices */

$this->title = 'Редактировать услугу: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Наши услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="about-services-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
