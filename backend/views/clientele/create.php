<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Clientele */

$this->title = 'Добавить пост. покупателя';
$this->params['breadcrumbs'][] = ['label' => 'Постоянные покупатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientele-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
