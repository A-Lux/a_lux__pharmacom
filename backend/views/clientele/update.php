<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Clientele */

$this->title = 'Редактировать: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Постоянные покупатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="clientele-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
