<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Филиалы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filial-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'address',
                'value' => function ($data) {
                    return $data->address;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' =>  [ 1 => 'Закрыто', 2 => 'Открыто'],
                'value' => function ($model) {
                    return \backend\controllers\LabelFilial::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'from_time',
            'to_time',
//            [
//                'attribute' => 'telephone',
//                'value' => function ($data) {
//                    return $data->telephone;
//                },
//                'format' => 'raw',
//            ],
//            [
//                'attribute' => 'city_id',
//                'filter' => \common\models\City::getList(),
//                'value' => function ($model) {
//                     return $model->city->name;},
//                'format' => 'raw',
//            ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
