<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountClientele */

$this->title = "Скидка для постоянных покупателей";
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="discount-clientele-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
        <?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
           // 'class' => 'btn btn-danger',
           // 'data' => [
            //    'confirm' => 'Are you sure you want to delete this item?',
            //    'method' => 'post',
            //],
        //]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'format' => 'raw',
                'attribute' => 'discount',
                'value' => function($data){
                    return $data->discount.' %';
                }
            ],
        ],
    ]) ?>

</div>
