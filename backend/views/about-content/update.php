<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutContent */

$this->title = 'Редактировать: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'О компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="about-content-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
