<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutStory */

$this->title = 'Добавить историю';
$this->params['breadcrumbs'][] = ['label' => 'Наша история', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-story-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
