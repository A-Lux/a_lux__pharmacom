<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutStory */

$this->title = 'Редактировать историю: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Наша история', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="about-story-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
