<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Администратор', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'username',
                'value' => $model->username,
                'format' => 'raw',
            ],

        ],
    ]) ?>



    <h3 >Доступы: </h3></br>

    <div class="form-group field-admission-menu">
        <h4> <span class="label <?=$admission->menu == 1 ? "label-success" : "label-default";?>">Меню</span></h4>
    </div>


    <div class="form-group field-admission-menu">
        <h4> <span class="label <?=$admission->banner == 1 ? "label-success" : "label-default";?>">Баннер</span></h4>
    </div>


    <label class="control-label" for="user-username">Пользователей</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-client">
            <h4> <span class="label <?=$admission->client == 1 ? "label-success" : "label-default";?>"> - Список пользователей</span></h4>
        </div>


        <div class="form-group field-admission-user_bonus">
            <h4> <span class="label <?=$admission->user_bonus == 1 ? "label-success" : "label-default";?>"> - Бонусы</span></h4>
        </div>

        <div class="form-group field-admission-clientele">
            <h4> <span class="label <?=$admission->clientele == 1 ? "label-success" : "label-default";?>"> - Постоянные покупатели</span></h4>
        </div>
    </div>


    <label class="control-label" for="user-username">Продукция</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-filial">
            <h4> <span class="label <?=$admission->filial == 1 ? "label-success" : "label-default";?>"> - Филиалы</span></h4>
        </div>

        <div class="form-group field-admission-catalog">
            <h4> <span class="label <?=$admission->catalogs == 1 ? "label-success" : "label-default";?>"> - Категории</span></h4>
        </div>

        <div class="form-group field-admission-products">
            <h4> <span class="label <?=$admission->products == 1 ? "label-success" : "label-default";?>"> - Продукция</span></h4>
        </div>

        <div class="form-group field-admission-country">
            <h4> <span class="label <?=$admission->country == 1 ? "label-success" : "label-default";?>"> - Страны</span></h4>
        </div>

        <div class="form-group field-admission-gift">
            <h4> <span class="label <?=$admission->gift == 1 ? "label-success" : "label-default";?>"> - Подарки</span></h4>
        </div>


        <label class="control-label" for="user-username">Скидки</label>
        <div style="margin-left: 20px;">
            <div class="form-group field-admission-discounts">
                <h4> <span class="label <?=$admission->discounts == 1 ? "label-success" : "label-default";?>"> - Для пенсионеров</span></h4>
            </div>


            <div class="form-group field-admission-discount_clientele">
                <h4> <span class="label <?=$admission->discount_clientele == 1 ? "label-success" : "label-default";?>"> -  Для постоянных покупателей</span></h4>
            </div>
        </div>
    </div>



    <label class="control-label" for="user-username">Заказы</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-orders">
            <h4> <span class="label <?=$admission->orders == 1 ? "label-success" : "label-default";?>"> Заказы</span></h4>
        </div>


        <div class="form-group field-admission-trigger_remind_basket">
            <h4> <span class="label <?=$admission->sms_notification_lists == 1 ? "label-success" : "label-default";?>"> -  Список рассылки</span></h4>
        </div>
    </div>


    <label class="control-label" for="user-username">О компании</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-aboutus">
            <h4> <span class="label <?=$admission->aboutus == 1 ? "label-success" : "label-default";?>"> - О компании</span></h4>
        </div>


        <div class="form-group field-admission-about_content">
            <h4> <span class="label <?=$admission->about_content == 1 ? "label-success" : "label-default";?>"> -  Контент</span></h4>
        </div>

        <div class="form-group field-admission-about_story">
            <h4> <span class="label <?=$admission->about_story == 1 ? "label-success" : "label-default";?>"> - Наша история</span></h4>
        </div>

        <div class="form-group field-admission-about_services">
            <h4> <span class="label <?=$admission->about_services == 1 ? "label-success" : "label-default";?>"> -  Наши услуги</span></h4>
        </div>
    </div>



    <label class="control-label" for="user-username">Триггерные письма</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-trigger_birthday">
            <h4> <span class="label <?=$admission->trigger_birthday == 1 ? "label-success" : "label-default";?>"> - В дни рождение</span></h4>
        </div>


        <div class="form-group field-admission-trigger_holiday">
            <h4> <span class="label <?=$admission->trigger_holiday == 1 ? "label-success" : "label-default";?>"> -  В празднике</span></h4>
        </div>

        <div class="form-group field-admission-trigger_tovar_month">
            <h4> <span class="label <?=$admission->trigger_tovar_month == 1 ? "label-success" : "label-default";?>"> - При обновлении Товар Месяца</span></h4>
        </div>

        <div class="form-group field-admission-trigger_remind_basket">
            <h4> <span class="label <?=$admission->trigger_remind_basket == 1 ? "label-success" : "label-default";?>"> -  При не осущестивление покупку</span></h4>
        </div>
    </div>



    <label class="control-label" for="user-username">Тексты</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-text">
            <h4> <span class="label <?=$admission->text == 1 ? "label-success" : "label-default";?>"> - По сайту</span></h4>
        </div>

        <div class="form-group field-admission-message">
            <h4> <span class="label <?=$admission->message == 1 ? "label-success" : "label-default";?>"> -  Сообщения</span></h4>
        </div>
    </div>

    <label class="control-label" for="user-username">Контакты</label>
    <div style="margin-left: 20px;">

        <div class="form-group field-admission-text">
            <h4> <span class="label <?=$admission->contact == 1 ? "label-success" : "label-default";?>"> Контакты</span></h4>
        </div>

        <div class="form-group field-admission-emailforrequest">
            <h4> <span class="label <?=$admission->emailforrequest == 1 ? "label-success" : "label-default";?>"> Эл. почта для связи</span></h4>
        </div>
    </div>


    <div class="form-group field-admission-vacancy">
        <h4> <span class="label <?=$admission->vacancy == 1 ? "label-success" : "label-default";?>">  Вакансии</span></h4>
    </div>


    <div class="form-group field-admission-faq">
        <h4> <span class="label <?=$admission->faq == 1 ? "label-success" : "label-default";?>"> FAQ</span></h4>
    </div>


    <div class="form-group field-admission-reviews">
        <h4> <span class="label <?=$admission->reviews == 1 ? "label-success" : "label-default";?>">  Отзывы</span></h4>
    </div>

    <div class="form-group field-admission-feedback">
        <h4> <span class="label <?=$admission->feedback == 1 ? "label-success" : "label-default";?>">  Обратная связь</span></h4>
    </div>


    <div class="form-group field-admission-subscribe">
        <h4> <span class="label <?=$admission->subscribe == 1 ? "label-success" : "label-default";?>">  Подписчики на рассылку</span></h4>
    </div>

    <div class="form-group field-admission-logo">
        <h4> <span class="label <?=$admission->logo == 1 ? "label-success" : "label-default";?>">  Логотип и Копирайт</span></h4>
    </div>

</div>
