<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="user-form" style="padding-bottom: 2000px;">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Доступы</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">



                <div class="form-group field-admission-menu">
                    <input type="hidden" name="Admission[menu]" value="0">
                    <label><input type="checkbox" id="user-menu" name="Admission[menu]" value="1" <?=$admission->client == 1 ? "checked":"";?>> Меню</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-banner">
                    <input type="hidden" name="Admission[banner]" value="0">
                    <label><input type="checkbox" id="user-banner" name="Admission[banner]" value="1" <?=$admission->banner == 1 ? "checked":"";?>> Баннер</label>
                    <div class="help-block"></div>
                </div>



                <label class="control-label" for="user-username">Пользователей</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-client">
                        <input type="hidden" name="Admission[client]" value="0">
                        <label><input type="checkbox" id="user-client" name="Admission[client]" value="1" <?=$admission->client == 1 ? "checked":"";?> > -  Список пользователей</label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-user_bonus">
                        <input type="hidden" name="Admission[user_bonus]" value="0">
                        <label><input type="checkbox" id="user-user_bonus" name="Admission[user_bonus]" value="1" <?=$admission->user_bonus == 1 ? "checked":"";?> > -  Бонусы</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-clientele">
                        <input type="hidden" name="Admission[clientele]" value="0">
                        <label><input type="checkbox" id="user-clientele" name="Admission[clientele]" value="1" <?=$admission->clientele == 1 ? "checked":"";?> > - Постоянные покупатели</label>
                        <div class="help-block"></div>
                    </div>
                </div>





                <label class="control-label" for="user-username">Продукция</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-filial">
                        <input type="hidden" name="Admission[filial]" value="0">
                        <label><input type="checkbox" id="user-filial" name="Admission[filial]" value="1" <?=$admission->filial == 1 ? "checked":"";?> > - Филиалы</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-catalogs">
                        <input type="hidden" name="Admission[catalogs]" value="0">
                        <label><input type="checkbox" id="user-catalogs" name="Admission[catalogs]" value="1" <?=$admission->catalogs == 1 ? "checked":"";?> > - Категории</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-products">
                        <input type="hidden" name="Admission[products]" value="0">
                        <label><input type="checkbox" id="user-products" name="Admission[products]" value="1" <?=$admission->products == 1 ? "checked":"";?> > - Продукция</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-country">
                        <input type="hidden" name="Admission[country]" value="0">
                        <label><input type="checkbox" id="user-country" name="Admission[country]" value="1" <?=$admission->country == 1 ? "checked":"";?> > - Страны </label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-gift">
                        <input type="hidden" name="Admission[gift]" value="0">
                        <label><input type="checkbox" id="user-gift" name="Admission[gift]" value="1" <?=$admission->gift == 1 ? "checked":"";?> > -  Подарки</label>
                        <div class="help-block"></div>
                    </div>


                    <label class="control-label" for="user-username">Скидки</label>
                    <div style="margin-left: 20px;">

                        <div class="form-group field-admission-discounts">
                            <input type="hidden" name="Admission[discounts]" value="0">
                            <label><input type="checkbox" id="user-discounts" name="Admission[discounts]" value="1" <?=$admission->discounts == 1 ? "checked":"";?> > -   Для пенсионеров</label>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group field-admission-discount_clientele">
                            <input type="hidden" name="Admission[discount_clientele]" value="0">
                            <label><input type="checkbox" id="user-discount_clientele" name="Admission[discount_clientele]" value="1" <?=$admission->discount_clientele == 1 ? "checked":"";?> > -  Для постоянных покупателей </label>
                            <div class="help-block"></div>
                        </div>

                    </div>

                </div>





                <label class="control-label" for="user-username">Заказы</label>
                <div style="margin-left: 20px;">

                    <div class="form-group field-admission-orders">
                        <input type="hidden" name="Admission[orders]" value="0">
                        <label><input type="checkbox" id="user-orders" name="Admission[orders]" value="1" <?=$admission->orders == 1 ? "checked":"";?> > -   Заказы</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-sms_notification_lists">
                        <input type="hidden" name="Admission[sms_notification_lists]" value="0">
                        <label><input type="checkbox" id="user-sms_notification_lists" name="Admission[sms_notification_lists]" value="1" <?=$admission->sms_notification_lists == 1 ? "checked":"";?> > -   Список рассылки </label>
                        <div class="help-block"></div>
                    </div>

                </div>



                <label class="control-label" for="user-username">О компании</label>
                <div style="margin-left: 20px;">

                    <div class="form-group field-admission-aboutus">
                        <input type="hidden" name="Admission[aboutus]" value="0">
                        <label><input type="checkbox" id="user-aboutus" name="Admission[aboutus]" value="1" <?=$admission->aboutus == 1 ? "checked":"";?> > -   О компании</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-about_content">
                        <input type="hidden" name="Admission[about_content]" value="0">
                        <label><input type="checkbox" id="user-about_content" name="Admission[about_content]" value="1" <?=$admission->about_content == 1 ? "checked":"";?> > -    Контент </label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-about_story">
                        <input type="hidden" name="Admission[about_story]" value="0">
                        <label><input type="checkbox" id="user-about_story" name="Admission[about_story]" value="1" <?=$admission->about_story == 1 ? "checked":"";?> > -   Наша история </label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-about_services">
                        <input type="hidden" name="Admission[about_services]" value="0">
                        <label><input type="checkbox" id="user-about_services" name="Admission[about_services]" value="1" <?=$admission->about_services == 1 ? "checked":"";?> > -   Наши услуги </label>
                        <div class="help-block"></div>
                    </div>

                </div>




                <label class="control-label" for="user-username">Триггерные письма</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-trigger_birthday">
                        <input type="hidden" name="Admission[trigger_birthday]" value="0">
                        <label><input type="checkbox" id="user-trigger_birthday" name="Admission[trigger_birthday]" value="1" <?=$admission->trigger_birthday == 1 ? "checked":"";?> > -   В дни рождение </label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-trigger_holiday">
                        <input type="hidden" name="Admission[trigger_holiday]" value="0">
                        <label><input type="checkbox" id="user-trigger_holiday" name="Admission[trigger_holiday]" value="1" <?=$admission->trigger_holiday == 1 ? "checked":"";?> > -    В празднике</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-trigger_tovar_month">
                        <input type="hidden" name="Admission[trigger_tovar_month]" value="0">
                        <label><input type="checkbox" id="user-trigger_tovar_month" name="Admission[trigger_tovar_month]" value="1" <?=$admission->trigger_tovar_month == 1 ? "checked":"";?>>  -  При обновлении Товар Месяца</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-trigger_remind_basket">
                        <input type="hidden" name="Admission[trigger_remind_basket]" value="0">
                        <label><input type="checkbox" id="user-trigger_remind_basket" name="Admission[trigger_remind_basket]" value="1" <?=$admission->trigger_remind_basket == 1 ? "checked":"";?> >  -  При не осущестивление покупку</label>
                        <div class="help-block"></div>
                    </div>
                </div>





                <label class="control-label" for="user-username">Тексты</label>
                <div style="margin-left: 20px;">

                    <div class="form-group field-admission-text">
                        <input type="hidden" name="Admission[text]" value="0">
                        <label><input type="checkbox" id="user-text" name="Admission[text]" value="1" <?=$admission->text == 1 ? "checked":"";?> > -  По сайту</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-message">
                        <input type="hidden" name="Admission[message]" value="0">
                        <label><input type="checkbox" id="user-message" name="Admission[message]" value="1" <?=$admission->message == 1 ? "checked":"";?> > -   Сообщения </label>
                        <div class="help-block"></div>
                    </div>

                </div>



                <label class="control-label" for="user-username">Контакты</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-contact">
                        <input type="hidden" name="Admission[contact]" value="0">
                        <label><input type="checkbox" id="user-contact" name="Admission[contact]" value="1" <?=$admission->contact == 1 ? "checked":"";?> > -  Контакты</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-emailforrequest">
                        <input type="hidden" name="Admission[emailforrequest]" value="0">
                        <label><input type="checkbox" id="user-emailforrequest" name="Admission[emailforrequest]" value="1" <?=$admission->emailforrequest == 1 ? "checked":"";?> > -   Эл. почта для связи </label>
                        <div class="help-block"></div>
                    </div>
                </div>



                <div class="form-group field-admission-vacancy">
                    <input type="hidden" name="Admission[vacancy]" value="0">
                    <label><input type="checkbox" id="user-vacancy" name="Admission[vacancy]" value="1" <?=$admission->contact == 1 ? "checked":"";?> >   Вакансии</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-faq">
                    <input type="hidden" name="Admission[faq]" value="0">
                    <label><input type="checkbox" id="user-faq" name="Admission[faq]" value="1" <?=$admission->faq == 1 ? "checked":"";?> >   FAQ</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-reviews">
                    <input type="hidden" name="Admission[reviews]" value="0">
                    <label><input type="checkbox" id="user-reviews" name="Admission[reviews]" value="1" <?=$admission->reviews == 1 ? "checked":"";?> >   Отзывы</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-feedback">
                    <input type="hidden" name="Admission[feedback]" value="0">
                    <label><input type="checkbox" id="user-feedback" name="Admission[feedback]" value="1" <?=$admission->feedback == 1 ? "checked":"";?> >   Обратная связь</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-email">
                    <input type="hidden" name="Admission[subscribe]" value="0">
                    <label><input type="checkbox" id="user-subscribe" name="Admission[subscribe]" value="1" <?=$admission->subscribe == 1 ? "checked":"";?> >   Подписчики на рассылку</label>
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-logo">
                    <input type="hidden" name="Admission[logo]" value="0">
                    <label><input type="checkbox" id="user-logo" name="Admission[logo]" value="1" <?=$admission->logo == 1 ? "checked":"";?> >   Логотип и Копирайт</label>
                    <div class="help-block"></div>
                </div>


            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <? if($from != "update"):?>

                    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

                <? endif;?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
