<?php

/* @var $this yii\web\View */

$this->title = 'Административная панель';


?>

<h1 style="text-align: center">
    Административная панель
</h1>


<?php if(Yii::$app->session->getFlash('profile_success')):?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('profile_success'); ?>
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('profile_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('profile_error'); ?>
    </div>
<?php endif;?>
