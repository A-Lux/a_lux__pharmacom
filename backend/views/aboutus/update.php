<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Aboutus */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'О компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="aboutus-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
