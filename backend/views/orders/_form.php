<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'statusProgress')->dropDownList(\backend\controllers\LabelProgress::statusList()) ?>

    <?= $form->field($model, 'statusPay')->dropDownList([0 => 'Не оплачено', 1 => 'Оплачено']) ?>

    <?= $form->field($model, 'sum')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_discount_for_senior')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_discount_for_regular')->textInput(['disabled'=>'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

