<?php

use yii\helpers\Html;

$this->title = 'Редактирование заказа: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="orders-update" style="padding-bottom: 600px;">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form',compact('model','products','gift')) ?>
</div>
