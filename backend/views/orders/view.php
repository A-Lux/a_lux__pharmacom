<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Заказ: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="orders-view" style="padding-bottom: 600px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::button('Копировать', ['class' => 'btn btn-success', 'data-clipboard-target' => '#data' ,'id' => 'copy']) ?>
    </p>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'id',

            [
                'attribute'=>'user_id',
                'value'=>function($model) {
                    if ($model->user_id) return $model->getFio();
                    else return $model->fio;
                }
            ],

            [
                'attribute'=>'Заказчик',
                'value'=>function($model) {
                    if ($model->user_id) return 'Зарегистрирован';
                    else return 'Не зарегистрирован';
                }
            ],

            [
                'attribute'=>'Телефон',
                'value'=>function($model){
                    if ($model->user_id) return $model->getPhone();
                    else return $model->telephone;
                }
            ],

            [
                'attribute'=> "Почта",
                'value'=>function($model){
                    if ($model->user_id) return $model->getEmail();
                     else return $model->email;
                }
            ],

            [
                'attribute' => 'paymentMethod',
                'filter' => \backend\controllers\LabelType::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelType::statusLabel($model->paymentMethod);
                },
                'format' => 'raw',
            ],



            [
                'attribute' => 'statusPay',
                'filter' => \backend\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusProgress',
                'filter' => \backend\controllers\LabelProgress::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelProgress::statusLabel($model->statusProgress);
                },
                'format' => 'raw',
            ],




            [
                'format' => 'raw',
                'attribute' => 'used_discount_for_senior',
                'value' => function($data){
                    return $data->used_discount_for_senior.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_discount_for_regular',
                'value' => function($data){
                    return $data->used_discount_for_regular	.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_bonus',
                'value' => function($data){
                    return $data->used_bonus.' тг';
                }
            ],


            [
                'format' => 'raw',
                'attribute' => 'sum',
                'value' => function($data){
                    return $data->sum.' тг';
                }
            ],
            'message_id',
            'payment_id',

            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return $model->updated_at;
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>



    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Продукты заказа:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Количества</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                <? if($products != null):?>
                    <? foreach ($products as $v):?>
                        <? if($v->product):?>
                            <tr>
                                <td>
                                    <a href="/admin/products/view?id=<?=$v->product->id;?>">
                                        <img src="<?=$v->product->getImage();?>" width="50">  <?=$v->product->name;?>
                                    </a>
                                </td>
                                <td><?=$v->count;?></td>
                                <td><?=$v->product->calculatePrice * $v->count;?></td>
                            </tr>
                        <? else:?>
                            <tr>
                                <td>Не найдено, ID:<?=$v->product_id;?></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <? endif;?>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
    </div>


    <? if($gift != null):?>
    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Подарки:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Количества</th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($gift as $v):?>

                    <tr>
                        <td><a href="/admin/product/view?id=<?=$v->product->id;?>"><img src="<?=$v->product->getImage();?>" width="50">  <?=$v->product->name;?></a></td>
                        <td><?=$v->count;?></td>
                    </tr>
                <? endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <? endif;?>

</div>


<textarea id="data" style="height:0;position:absolute;z-index: -1;">
Заказчик: <?=$model->user_id ? "Зарегистрирован\n" : "Не зарегистрирован\n";?>
ФИО: <?=$model->getFio()."\n";?>
Телефон: <?=$model->getPhone()."\n";?>
Email: <?=$model->getEmail()."\n";?>
Тип заказа: <?=backend\controllers\LabelType::statusLabelValue($model->paymentMethod)."\n";?>
Статус оплаты: <?=\backend\controllers\LabelPay::statusLabelValue($model->statusPay)."\n";?>
Сумма заказа: <?=$model->sum." тг\n";?>
Дата заказа: <?=$model->created_at."\n";?>

Продукты заказа:<?="\n"?> <? $m = 0;?>
<? foreach ($products as $v):?><? $m++;?>
№<?=$m?> <?=$v->product->name;?>  X  <?=$v->count."\n";?>
<? endforeach;?>
</textarea>



<script>
    var clipboard = new ClipboardJS('#copy');

    clipboard.on('success', function(e) {
        showSuccessAlert("Скопировано")
    });

</script>


