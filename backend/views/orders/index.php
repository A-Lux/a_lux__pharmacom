<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Экспорт в Excel', ['export-file'], ['class' => 'btn btn-success']) ?>
    </p>
    <?echo date_default_timezone_get();;?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'user_id',
                'value'=>function($model) {
                    if ($model->user_id) return $model->getFio();
                    else return $model->fio;
                }
            ],
            [
                'attribute' => 'paymentMethod',
                'filter' => \backend\controllers\LabelType::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelType::statusLabel($model->paymentMethod);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPay',
                'filter' => \backend\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusProgress',
                'filter' => \backend\controllers\LabelProgress::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelProgress::statusLabel($model->statusProgress);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
