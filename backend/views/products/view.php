<?php

use backend\controllers\LabelNew;
use backend\controllers\LabelPopular;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    if($model->category != null){
                        return $model->category->name;
                    }else{
                        return null;
                    }

                },
                'format' => 'raw'
            ],

            [
                'attribute' => 'description',
                'value' => $model->description,
                'format' => 'raw',
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => LabelPopular::statusList(),
                'value' => function ($model) {
                    return LabelPopular::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isNew',
                'filter' => LabelNew::statusList(),
                'value' => function ($model) {
                    return LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            'price',
            'bonus',
            'discount',
            [
                'attribute' => 'url',
                'value' => $model->url,
                'format' => 'raw',
            ],
            [
                'attribute' => 'images',
                'value' => function ($model) {
                    $status = 0;
                    if($model->images){
                        $files = unserialize($model->images);
                        if(file_exists($model->path . $files[0])){
                             $status = 1;
                        }
                    }

                    if($status){
                        return Html::img($model->getImage(), ['width'=>200]);
                    }elseif($model->images == ''){
                        return "(не задано)";
                    }else{
                        return "Картинка не найдено";
                    }
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'text',
                'value' => function ($model) {
                    return $model->text;
                },
                'format' => 'raw',
            ],
            'created_at',

        ],
    ]) ?>

    <table id="w1" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Сопутствующие товары</th>
                <td>
                <? foreach ($sop_tovary as $v):?>
                    <? if($v->product != null):?>
                    <?=$v->product->name.'<br>';?>
                    <? endif;?>
                <? endforeach;?>
                </td>
            </tr>
            <tr>
                <th>Аналоги лекарственных средств</th>
                <td>
                    <? foreach ($analogy as $v):?>
                        <? if($v->product != null):?>
                        <?=$v->product->name.'<br>';?>
                        <? endif;?>
                    <? endforeach;?>
                </td>
            </tr>
        </tbody>
    </table>




    <br><br>
    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Остатки</h3>
        </div>
        <? if($remainder != null):?>
        <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Аптека</th>
                            <th>Количества</th>
                        </tr>
                    </thead>
                    <tbody id="nearlyPharmaciesData">
                    <? foreach ($remainder as $v):?>
                        <tr>
                            <td><?= $v->filialNumber; ?></td>
                            <td><?= $v->filialName; ?></td>
                            <td>
                                <? if($v->amount > 0):?>
                                    <?= $v->amount; ?>
                                <? else:?>
                                    Нет в наличии
                                <? endif;?>
                            </td>
                        </tr>
                    <? endforeach;?>
                    </tbody>
                </table>
            </div>
        <? else:?>
            <div class="box-body">
                Нет
            </div>
        <? endif;?>

    </div>
</div>
