<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

?>

<link rel="stylesheet" src="/backend/web/private/bower_components/select2/dist/css/select2.min.css">

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false
        ]),
    ]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <? if($model->images):?>
        <? $files = unserialize($model->images);?>
        <? if(file_exists($model->path . $files[0])):?>

            <div id="images">
                <div class="row">
                    <div class="" style="text-align: center">
                        <div style="margin-top:20px;">
                            <img src="<?=$model->getImage();?>" alt="" style="margin-left:15px;width:400px;height:400px;float:left;">
                        </div>
                    </div>
                </div>
                <button type="button" style="margin-bottom: 30px;margin-top: 10px;" onclick="deleteImage(<?=$model->id;?>)"
                        class="btn btn-danger">Удалить картинку</button>
            </div>
        <? endif;?>
    <? endif;?>


    <?= $form->field($model, 'files[]')->label(false)->widget(FileInput::className(), [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'showUpload' => false
        ],
    ]) ?>
    <div class="form-group field-products-category_id required has-error">
        <label class="control-label" for="products-category_id">Category ID</label>

        <select name="Products[category_id]" class="form-control">
            <option></option>
        <?
            $catalog = \common\models\CatalogProducts::find()
                ->where("level = 1")->all();
            $array = [];
            foreach ($catalog as $v){
                echo '<option value="'.$v->id.'" disabled="disabled">'.$v->name.'</option>';
                $array[$v->id] = $v->name;
                if(isset($v->childs)){
                    foreach($v->childs as $child1) {
                        if(!empty($child1->childs))
                            echo '<option value="'.$child1->id.'" disabled="disabled">--'.$child1->name.'</option>';
                        else{
                            $selected = '';
                            if($child1->id == $model->category_id)
                                $selected = 'selected="selected"';
                                echo '<option '.$selected.' value="'.$child1->id.'">--'.$child1->name.'</option>';
                        }
                        $array[$child1->id] = '--'.$child1->name;
                        if(isset($child1->childs)){
                            foreach($child1->childs as $child2){
                                $selected = '';
                                if($child2->id == $model->category_id)
                                    $selected = 'selected="selected"';
                                echo '<option '.$selected.' value="'.$child2->id.'">----'.$child2->name.'</option>';
                                $array[$child2->id] = '----'.$child2->name;
                            }
                        }
                    }
                }
            }
        ?>
        </select>

    </div>
    <?//= $form->field($model, 'category_id')->dropDownList($array ,['prompt' => '']) ?>
    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::getAll(), 'id' ,'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'top_month')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isNew')->dropDownList([1 => 'Новые',0 => 'Старые']) ?>

    <?= $form->field($model, 'isHit')->dropDownList([0 => 'Нет',1 => 'Да'])?>

    <?= $form->field($model, 'bonus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_products')->dropDownList(['дефицит' => 'дефицит', null => null]) ?>



    <div class="form-group">
        <label> Сопутствующие товары</label>
        <select class="form-control" id="sop_tovary" multiple="multiple" data-placeholder="Выберите продукта"
                style="width: 100%;" name="sop_tovary[]">
            <? if(count($sop_tovary) > 0):?>
                <? foreach ($sop_tovary as $v):?>
                    <? if($v->product != null):?>
                    <option value="<?=$v->product->id;?>" selected><?=$v->product->name;?></option>
                    <? endif;?>
                <? endforeach;?>
            <? endif;?>
        </select>
    </div>



    <div class="form-group">
        <label> Аналоги лекарственных средств</label>
        <select class="form-control" id="analogy" multiple="multiple" data-placeholder="Выберите продукта"
                style="width: 100%;" name="analogy[]">
            <? if(count($analogy) > 0):?>
                <? foreach ($analogy as $v):?>
                    <? if($v->product != null):?>
                    <option value="<?=$v->product->id;?>" selected><?=$v->product->name;?></option>
                    <? endif;?>
                <? endforeach;?>
            <? endif;?>
        </select>
    </div>



    <div class="form-group" style="padding-top: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <br><br><br><br><br><br>
</div>


<script>



    function deleteImage(id) {
        $.ajax({
            type: 'GET',
            url: '/admin/products/delete-image',
            data: {id:id},
            success: function (response) {
                if (response == 1) {
                    $('#images').hide();
                }
            },
            error: function () {
                alert('Что-то пошло не так.');
            }
        });
    }



</script>
